import convertCase from './convert-case'
export const Capitalize = (string) => string.replace(/\b\w/g, (l) => l.toUpperCase())
export { convertCase }
