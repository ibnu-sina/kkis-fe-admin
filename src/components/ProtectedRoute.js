import React, { useEffect } from 'react'
import { Route, useHistory, useLocation } from 'react-router-dom'
import { useSelector } from 'react-redux'
import localStorage from 'store'

function ProtectedRoute({ ...rest }) {
  const history = useHistory()
  const location = useLocation()
  const { isLoggedIn } = useSelector((state) => state.userLogin)

  useEffect(() => {
    if (!isLoggedIn) {
      localStorage.remove('rt')
      history.push(`/?from=${location.pathname}`)
    }
  }, [location, history, isLoggedIn])

  return <Route {...rest} />
}

export default ProtectedRoute
