import React, { useEffect } from 'react'
import { Autocomplete } from '@material-ui/lab'
import { Controller } from 'react-hook-form'
import { TextField } from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux'
import { getAvailableRoomList } from 'redux/actions/roomActions'

function RoomDropdown({ errors, control, currentRoom, required }) {
  const dispatch = useDispatch()

  const { availableRooms } = useSelector((state) => state.roomList)
  const rules = {
    validate: (value) => {
      if (!value) return false
      return Boolean(value.id)
    },
  }

  useEffect(() => {
    dispatch(getAvailableRoomList())
  }, [dispatch])

  return (
    <Controller
      as={
        <Autocomplete
          options={availableRooms}
          getOptionSelected={(option) => option.name}
          getOptionLabel={(option) => option.name}
          renderOption={(option) => {
            const { occupied, capacity, name } = option
            const available = capacity - occupied

            return `${name} (${available})`
          }}
          renderInput={(params) => (
            <TextField {...params} label='Room' variant='outlined' error={!!errors.room} />
          )}
        />
      }
      onChange={([, data]) => data}
      name='room'
      control={control}
      defaultValue={currentRoom}
      rules={required ? rules : {}}
    />
  )
}

export default RoomDropdown
