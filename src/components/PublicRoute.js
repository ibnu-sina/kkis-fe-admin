import React, { useEffect } from 'react'
import { Route, useHistory } from 'react-router-dom'
import { useSelector } from 'react-redux'

function ProtectedRoute({ ...rest }) {
  const history = useHistory()
  const { isLoggedIn } = useSelector((state) => state.userLogin)

  useEffect(() => {
    if (isLoggedIn) history.push(`/dashboard/students`)
  }, [history, isLoggedIn])

  return <Route {...rest} />
}

export default ProtectedRoute
