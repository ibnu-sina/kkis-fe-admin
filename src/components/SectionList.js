import React from 'react'

import { Divider, ListItem, ListItemText, makeStyles, Paper } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  button: {
    color: theme.palette.text.secondary,
    fontWeight: theme.typography.fontWeightMedium,
    justifyContent: 'flex-start',
    letterSpacing: 0,
    textTransform: 'none',
    width: '100%',
    paddingBottom: theme.spacing(2),
    paddingTop: theme.spacing(2),
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
  },
  title: {
    fontWeight: theme.typography.fontWeightRegular,
  },
  selected: {
    background: 'none!important',
    color: theme.palette.primary.main,
    '& $title': {
      fontWeight: theme.typography.fontWeightBold,
    },
  },
}))

function SectionList({ section, setSection, listItem }) {
  const classes = useStyles()
  return (
    <Paper>
      {listItem.map((item, index) => (
        <React.Fragment key={index}>
          <ListItem
            button
            className={classes.button}
            classes={{ selected: classes.selected }}
            selected={section === item.section}
            onClick={() => setSection(item.section)}
          >
            <ListItemText
              primaryTypographyProps={{ className: classes.title }}
              primary={item.label}
            />
          </ListItem>
          {index !== listItem.length && <Divider />}
        </React.Fragment>
      ))}
    </Paper>
  )
}

export default SectionList
