import React, { useState } from 'react'

import Alert from '@material-ui/lab/Alert'
import Snackbar from '@material-ui/core/Snackbar'

function CustomSnackbar({
  children,
  vertical = 'bottom',
  horizontal = 'left',
  variant = 'standard',
  severity = 'info',
  duration = 6000,
}) {
  const [open, setOpen] = useState(true)
  return (
    <Snackbar
      open={open}
      autoHideDuration={duration}
      anchorOrigin={{ vertical, horizontal }}
      onClose={() => setOpen(false)}
    >
      <Alert variant={variant} severity={severity}>
        {children}
      </Alert>
    </Snackbar>
  )
}

export default CustomSnackbar
