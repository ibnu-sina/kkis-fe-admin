import React from 'react'
import hexToRgba from 'hex-to-rgba'
import { makeStyles } from '@material-ui/core'

function StatusBadge({ text, severity = 'info' }) {
  const useStyles = makeStyles((theme) => ({
    bar: {
      paddingTop: theme.spacing(0.5),
      paddingBottom: theme.spacing(0.5),
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
      fontWeight: '500',
      fontSize: '0.75rem',
      borderRadius: '2px',
      textTransform: 'uppercase',
      letterSpacing: '0.5px',
      backgroundColor: hexToRgba(theme.palette[severity].main, '0.08'),
      color: theme.palette[severity].main,
    },
  }))

  const classes = useStyles()
  return <span className={classes.bar}>{text}</span>
}

export default StatusBadge
