import axios from 'axios'
import createAuthRefreshInterceptor from 'axios-auth-refresh'
import store from 'redux/store'
import { attemptRefreshToken } from 'redux/actions/userActions'

axios.defaults.baseURL = process.env.REACT_APP_API_URL

axios.interceptors.request.use((req) => {
  // Add auth header if available
  const { token } = store.getState().userLogin
  if (token) req.headers.Authorization = `Bearer ${token}`

  // Add user_type to every request with data
  if (req.data) req.data.user_type = 'Admin'
  return req
})

export const refreshAuthLogic = async (failedRequest) => {
  const { token, refreshToken } = store.getState().userLogin
  await store.dispatch(attemptRefreshToken(refreshToken))
  failedRequest.response.config.headers['Authorization'] = `Bearer ${token}`

  return Promise.resolve()
}

export default createAuthRefreshInterceptor(axios, refreshAuthLogic)
