import React, { useState } from 'react'
import { Checkbox, TableCell } from '@material-ui/core'

function useSelect(data) {
  const [selectedIds, setSelectedIds] = useState([])
  const isSelected = (id) => selectedIds.indexOf(id) !== -1

  const handleSelectAll = (event) => {
    let newSelectedIds
    if (event.target.checked) {
      newSelectedIds = data.map((item) => item.id)
    } else {
      newSelectedIds = []
    }
    setSelectedIds(newSelectedIds)
  }

  const handleSelectOne = (event, id) => {
    const selectedIndex = selectedIds.indexOf(id)
    let newSelectedIds = []

    if (selectedIndex === -1) {
      newSelectedIds = newSelectedIds.concat(selectedIds, id)
    } else if (selectedIndex === 0) {
      newSelectedIds = newSelectedIds.concat(selectedIds.slice(1))
    } else if (selectedIndex === selectedIds.length - 1) {
      newSelectedIds = newSelectedIds.concat(selectedIds.slice(0, -1))
    } else if (selectedIndex > 0) {
      newSelectedIds = newSelectedIds.concat(
        selectedIds.slice(0, selectedIndex),
        selectedIds.slice(selectedIndex + 1)
      )
    }

    setSelectedIds(newSelectedIds)
  }

  function TopCheckBox() {
    return (
      <TableCell padding='checkbox'>
        <Checkbox
          checked={selectedIds.length === data.length}
          color='primary'
          indeterminate={selectedIds.length > 0 && selectedIds.length < data.length}
          onChange={handleSelectAll}
        />
      </TableCell>
    )
  }

  function ItemCheckBox({ id }) {
    return (
      <TableCell padding='checkbox'>
        <Checkbox
          checked={selectedIds.indexOf(id) !== -1}
          onChange={(event) => handleSelectOne(event, id)}
          value='true'
        />
      </TableCell>
    )
  }

  return { isSelected, TopCheckBox, ItemCheckBox }
}

export default useSelect
