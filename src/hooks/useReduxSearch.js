import React, { useEffect, useState } from 'react'
import { IconButton, TextField } from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux'
import { convertCase } from 'helpers'
import { Search as SearchIcon, X as XIcon } from 'react-feather'
import { _CLEAR_SEARCH, _ON_CHANGE_SEARCH } from 'redux/types/serachTypes'

function useReduxSearch(reducer, fetchData) {
  const dispatch = useDispatch()
  const REDUCER_NAME = convertCase.toKebab(reducer).split('-').join('_').toUpperCase()
  const reducerName = reducer + 'SearchQuery'

  function SearchField() {
    const searchQuery = useSelector((state) => state[reducerName])
    const [search, setSearch] = useState(searchQuery !== '')

    useEffect(() => {
      if (searchQuery === '') setSearch(false)
    }, [searchQuery])

    const clearSearch = () => {
      setSearch(false)
      dispatch({ type: REDUCER_NAME + _CLEAR_SEARCH })
      dispatch(fetchData())
    }

    const handleChange = (e) => {
      setSearch(true)
      dispatch({ type: REDUCER_NAME + _ON_CHANGE_SEARCH, payload: e.target.value })
    }

    const onSubmit = (e) => {
      e.preventDefault()
      dispatch(fetchData())
    }

    return (
      <form onSubmit={onSubmit}>
        <TextField
          value={searchQuery}
          fullWidth
          InputProps={{
            startAdornment: (
              <IconButton fontSize='small' type='submit'>
                <SearchIcon />
              </IconButton>
            ),
            endAdornment: search && (
              <IconButton fontSize='small' onClick={clearSearch}>
                <XIcon />
              </IconButton>
            ),
          }}
          placeholder='Search'
          variant='outlined'
          onChange={handleChange}
        />
      </form>
    )
  }
  return { SearchField }
}

export default useReduxSearch
