import React, { useState } from 'react'
import { TablePagination } from '@material-ui/core'

function usePagination(currentLimit = 10, currentPage = 1) {
  const [limit, setLimit] = useState(currentLimit)
  const [page, setPage] = useState(currentPage)

  function Pagination({ data, total }) {
    const handleLimitChange = (event) => {
      setLimit(event.target.value)
      setPage(page)
    }
    const handlePageChange = (event, newPage) => {
      setPage(newPage + 1)
    }
    const count = total ? total : data ? data.length : 0
    return (
      <TablePagination
        component='div'
        count={count}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page - 1}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    )
  }
  return { page, limit, Pagination }
}

export default usePagination
