import React, { useState } from 'react'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  TextField,
  Typography,
} from '@material-ui/core'
import { useForm } from 'react-hook-form'
import { useDispatch } from 'react-redux'
import { attachSerialNo } from 'redux/actions/studentApplianceActions'

function useSerialNo({ from, appliance_id, student_id }) {
  const [open, setDialogOpen] = useState(false)
  const dispatch = useDispatch()

  function AddSerialNoDialog() {
    const { handleSubmit, register, errors } = useForm()
    const onClose = () => setDialogOpen(false)

    const onSubmit = (body) => {
      dispatch(attachSerialNo({ from, body, appliance_id, student_id }))
      onClose()
    }

    return (
      <Dialog open={open} onClose={onClose} maxWidth='xs' fullWidth>
        <form onSubmit={handleSubmit(onSubmit)}>
          <DialogTitle disableTypography>
            <Typography variant='h5' component='h5'>
              Add Serial Number
            </Typography>
          </DialogTitle>
          <DialogContent>
            <Grid container spacing={1}>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  type='text'
                  label='Serial No.'
                  name='serial_no'
                  variant='outlined'
                  inputRef={register({ required: true })}
                  error={!!errors.serial_no}
                  helperText={!!errors.serial_no && 'Required field'}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button color='primary' onClick={onClose}>
              Cancel
            </Button>
            <Button type='submit' color='primary' variant='contained'>
              Confirm
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    )
  }

  return { setDialogOpen, AddSerialNoDialog }
}

export default useSerialNo
