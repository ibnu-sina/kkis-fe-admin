import React from 'react'
import { TablePagination } from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux'
import { _CHANGE_LIMIT, _CHANGE_PAGE } from 'redux/types/paginationTypes'
import { convertCase } from 'helpers'

function useReduxPagination(reducer) {
  const actionName = convertCase.toKebab(reducer).split('-').join('_').toUpperCase()
  const reducerName = reducer + 'Pagination'
  const dispatch = useDispatch()
  const { total, page, perPage } = useSelector((state) => state[reducerName])

  function Pagination() {
    const handleLimitChange = (event) => {
      // Update limit
      dispatch({ type: actionName + _CHANGE_LIMIT, payload: event.target.value })
      // Reset page to 1
      dispatch({ type: actionName + _CHANGE_PAGE, payload: 1 })
    }

    const handlePageChange = (event, newPage) => {
      dispatch({ type: actionName + _CHANGE_PAGE, payload: newPage + 1 })
    }

    return (
      <TablePagination
        component='div'
        count={total}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page - 1}
        rowsPerPage={perPage}
        rowsPerPageOptions={[5, 10, 25]}
      />
    )
  }
  return { Pagination }
}

export default useReduxPagination
