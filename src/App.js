import React from 'react'

import { BrowserRouter as Router, Switch } from 'react-router-dom'

import theme from './theme'
import { ThemeProvider } from '@material-ui/core'
import { ConfirmProvider } from 'material-ui-confirm'

import MainLayout from './layout/MainLayout'
import DashboardLayout from './layout/DashboardLayout'
import PublicRoute from './components/PublicRoute'
import ProtectedRoute from './components/ProtectedRoute'

function App() {
  return (
    <>
      <ThemeProvider theme={theme}>
        <ConfirmProvider>
          <Router>
            <Switch>
              <ProtectedRoute path='/dashboard' component={DashboardLayout} />
              <PublicRoute path='/' component={MainLayout} />
            </Switch>
          </Router>
        </ConfirmProvider>
      </ThemeProvider>
    </>
  )
}

export default App
