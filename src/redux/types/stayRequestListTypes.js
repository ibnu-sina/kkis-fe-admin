export const GET_STAY_REQUEST_LIST_REQUEST = 'GET_STAY_REQUEST_LIST_REQUEST'
export const GET_STAY_REQUEST_LIST_FAIL = 'GET_STAY_REQUEST_LIST_FAIL'
export const GET_STAY_REQUEST_LIST_SUCCESS = 'GET_STAY_REQUEST_LIST_SUCCESS'

export const GET_STAY_REQUEST_REQUEST = 'GET_STAY_REQUEST_REQUEST'
export const GET_STAY_REQUEST_FAIL = 'GET_STAY_REQUEST_FAIL'
export const GET_STAY_REQUEST_SUCCESS = 'GET_STAY_REQUEST_SUCCESS'

export const APPROVE_STAY_REQUEST_REQUEST = 'APPROVE_STAY_REQUEST_REQUEST'
export const APPROVE_STAY_REQUEST_FAIL = 'APPROVE_STAY_REQUEST_FAIL'
export const APPROVE_STAY_REQUEST_SUCCESS = 'APPROVE_STAY_REQUEST_SUCCESS'

export const STAY_REQUEST_LIST_SET_PAGINATION = 'STAY_REQUEST_LIST_SET_PAGINATION'
