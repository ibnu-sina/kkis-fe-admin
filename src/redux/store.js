import { createStore, combineReducers, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import localstorage from 'store'

import {
  inventoryStorageReducer,
  inventoryStorageListReducer,
} from './reducers/inventoryStorageReducers'
import { applianceReducer, applianceListReducer } from './reducers/applianceListReducers'
import { stayRequestListReducer, stayRequestReducer } from './reducers/stayRequestListReducers'
import { userLoginReducer, userProfileReducer } from './reducers/userReducers'
import { courseListReducer, courseReducer } from './reducers/courseListReducers'
import { activityListReducer } from './reducers/activityListReducers'
import { meritListReducer } from './reducers/meritListReducers'
import { adminListReducer } from './reducers/adminListReducers'
import { studentListReducer } from './reducers/studentListReducers'
import { studentProfileReducer, studentRoomReducer } from './reducers/studentProfileReducers'
import { inviteListReducer } from './reducers/inviteListReducers'
import { registerReducer } from './reducers/registerReducers'
import { profileReducer } from './reducers/profileReducers'
import {
  studenApplianceListReducer,
  studentApplianceReducer,
} from './reducers/studentApplianceReducers'
import {
  studentInviteListReducer,
  uploadStudentInviteListReducer,
} from './reducers/studentInviteListReducers'
import { roomListReducer, roomReducer } from './reducers/roomReducers'

import { USER_LOGOUT_SUCCESS } from './types/userTypes'
import { createPaginationReducer } from './reducers/paginationReducer'
import { createSearchReducer } from './reducers/searchReducer'

const middleware = [thunk]
const appReducer = combineReducers({
  activityList: activityListReducer,
  activityListPagination: createPaginationReducer('activityList'),
  adminList: adminListReducer,
  adminListPagination: createPaginationReducer('adminList'),
  adminListSearchQuery: createSearchReducer('adminList'),
  appliance: applianceReducer,
  applianceList: applianceListReducer,
  applianceListPagination: createPaginationReducer('applianceList'),
  course: courseReducer,
  courseList: courseListReducer,
  courseListPagination: createPaginationReducer('courseList'),
  courseListSearchQuery: createSearchReducer('courseList'),
  inventoryStorage: inventoryStorageReducer,
  inventoryStorageList: inventoryStorageListReducer,
  inventoryStorageListPagination: createPaginationReducer('inventoryStorageList'),
  inviteList: inviteListReducer,
  inviteListPagination: createPaginationReducer('inviteList'),
  inviteListSearchQuery: createSearchReducer('inviteList'),
  meritList: meritListReducer,
  meritListPagination: createPaginationReducer('meritList'),
  meritListSearchQuery: createSearchReducer('meritList'),
  profile: profileReducer,
  register: registerReducer,
  room: roomReducer,
  roomList: roomListReducer,
  roomListPagination: createPaginationReducer('roomList'),
  stayRequest: stayRequestReducer,
  stayRequestList: stayRequestListReducer,
  stayRequestListPagination: createPaginationReducer('stayRequestList'),
  studentAppliance: studentApplianceReducer,
  studentApplianceList: studenApplianceListReducer,
  studentApplianceListPagination: createPaginationReducer('studentApplianceList'),
  studentApplianceListSearchQuery: createSearchReducer('studentApplianceList'),
  studentInviteList: studentInviteListReducer,
  studentInviteListPagination: createPaginationReducer('studentInviteList'),
  studentInviteListSearchQuery: createSearchReducer('studentInviteList'),
  studentList: studentListReducer,
  studentListPagination: createPaginationReducer('studentList'),
  studentListSearchQuery: createSearchReducer('studentList'),
  studentProfile: studentProfileReducer,
  studentRoom: studentRoomReducer,
  uploadStudentInviteList: uploadStudentInviteListReducer,
  userLogin: userLoginReducer,
  userProfile: userProfileReducer,
})

const reducer = (state, action) => {
  if (action.type === USER_LOGOUT_SUCCESS) {
    state = undefined
  }

  return appReducer(state, action)
}

const refreshToken = localstorage.get('rt') ? localstorage.get('rt') : ''

const initialState = {
  userLogin: {
    isLoggedIn: refreshToken ? true : false,
    refreshToken: refreshToken ? refreshToken : '',
  },
}

const store = createStore(
  reducer,
  initialState,
  composeWithDevTools(applyMiddleware(...middleware))
)

export default store
