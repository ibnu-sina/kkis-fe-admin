import {
  GET_MERIT_LIST_REQUEST,
  GET_MERIT_LIST_SUCCESS,
  GET_MERIT_LIST_FAIL,
  TOGGLE_MERIT_APPROVAL_REQUEST,
  TOGGLE_MERIT_APPROVAL_SUCCESS,
  TOGGLE_MERIT_APPROVAL_FAIL,
} from '../types/meritListTypes'

const initialMeritListState = {
  meritList: [],
  status: 'idle',
  errorMsg: null,
  successMsg: null,
}

export function meritListReducer(state = initialMeritListState, action) {
  switch (action.type) {
    case GET_MERIT_LIST_REQUEST:
      return {
        ...state,
        status: 'loading',
      }
    case TOGGLE_MERIT_APPROVAL_REQUEST:
      return {
        ...state,
        status: 'toggle',
      }
    case GET_MERIT_LIST_SUCCESS:
      return {
        ...state,
        status: 'loaded',
        meritList: action.payload,
      }
    case GET_MERIT_LIST_FAIL:
    case TOGGLE_MERIT_APPROVAL_FAIL:
      return {
        ...state,
        status: 'error',
        errorMsg: action.payload,
      }
    case TOGGLE_MERIT_APPROVAL_SUCCESS:
      const updated = state.meritList.map((i) => {
        if (i.id === action.payload.data.id) {
          i.is_approved = action.payload.data.is_approved
        }
        return i
      })
      return {
        ...state,
        status: 'updated',
        successMsg: action.payload.message,
        meritList: updated,
      }
    default:
      return state
  }
}
