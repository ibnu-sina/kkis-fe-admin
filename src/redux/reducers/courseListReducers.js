import {
  GET_COURSE_LIST_REQUEST,
  GET_COURSE_LIST_SUCCESS,
  GET_COURSE_LIST_FAIL,
  ADD_COURSE_REQUEST,
  ADD_COURSE_SUCCESS,
  ADD_COURSE_FAIL,
  GET_COURSE_REQUEST,
  GET_COURSE_SUCCESS,
  GET_COURSE_FAIL,
  EDIT_COURSE_REQUEST,
  EDIT_COURSE_SUCCESS,
  EDIT_COURSE_FAIL,
  TOGGLE_COURSE_SUCCESS,
  TOGGLE_COURSE_FAIL,
  DELETE_COURSE_REQUEST,
  DELETE_COURSE_SUCCESS,
  DELETE_COURSE_FAIL,
} from '../types/courseListTypes'

const initialCourseListState = {
  courseList: [],
  status: 'idle',
  errorMsg: null,
  successMsg: null,
}

export function courseListReducer(state = initialCourseListState, action) {
  switch (action.type) {
    case GET_COURSE_LIST_REQUEST:
    case ADD_COURSE_REQUEST:
      return {
        ...state,
        status: 'loading',
      }
    case GET_COURSE_LIST_SUCCESS:
      return {
        ...state,
        status: 'loaded',
        courseList: action.payload,
      }
    case GET_COURSE_LIST_FAIL:
    case ADD_COURSE_FAIL:
    case TOGGLE_COURSE_FAIL:
      return {
        ...state,
        status: 'error',
        errorMsg: action.payload,
      }
    case ADD_COURSE_SUCCESS:
      return {
        ...state,
        status: 'created',
        successMsg: action.payload.message,
        courseList: [...state.courseList, action.payload.data],
      }
    case TOGGLE_COURSE_SUCCESS:
      const filter = state.courseList.map((i) => {
        if (i.id === action.payload.data.id) {
          i.is_checkin = action.payload.data.is_checkin
          i.is_checkout = action.payload.data.is_checkout
        }
        return i
      })
      return {
        ...state,
        status: 'updated',
        successMsg: action.payload.message,
        courseList: filter,
      }
    case DELETE_COURSE_SUCCESS:
      return {
        ...state,
        status: 'deleted',
        successMsg: action.payload.message,
        courseList: state.courseList.filter((i) => i.id !== action.payload.data),
      }
    default:
      return state
  }
}

const initialCourseState = {
  course: {},
  status: 'idle',
  errorMsg: null,
  successMsg: null,
}

export function courseReducer(state = initialCourseState, action) {
  switch (action.type) {
    case GET_COURSE_REQUEST:
    case EDIT_COURSE_REQUEST:
    case DELETE_COURSE_REQUEST:
      return {
        ...state,
        status: 'loading',
      }
    case GET_COURSE_SUCCESS:
      return {
        ...state,
        status: 'loaded',
        course: action.payload.data,
      }
    case GET_COURSE_FAIL:
    case EDIT_COURSE_FAIL:
    case DELETE_COURSE_FAIL:
      return {
        ...state,
        status: 'error',
        errorMsg: action.payload,
      }
    case EDIT_COURSE_SUCCESS:
      return {
        ...state,
        status: 'updated',
        course: action.payload.data,
      }
    case DELETE_COURSE_SUCCESS:
      return {
        ...state,
        status: 'deleted',
        course: {},
      }
    default:
      return state
  }
}
