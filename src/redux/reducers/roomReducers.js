import {
  GET_ROOM_LIST_FAIL,
  GET_ROOM_LIST_REQUEST,
  GET_ROOM_LIST_SUCCESS,
  ADD_ROOM_FAIL,
  ADD_ROOM_REQUEST,
  ADD_ROOM_SUCCESS,
  EDIT_ROOM_FAIL,
  EDIT_ROOM_REQUEST,
  EDIT_ROOM_SUCCESS,
  RESET_ROOM,
  DELETE_ROOM_REQUEST,
  DELETE_ROOM_FAIL,
  DELETE_ROOM_SUCCESS,
  GET_AVAILABLE_ROOM_LIST_REQUEST,
  GET_AVAILABLE_ROOM_LIST_SUCCESS,
  GET_AVAILABLE_ROOM_LIST_FAIL,
  UPDATE_ROOM_STATUS_FILTER,
} from '../types/roomTypes'

const initialRoomListState = {
  filter: { is_available: '' },
  roomList: [],
  status: 'idle',
  errorMsg: null,
  successMsg: null,
  availableRooms: [],
}

export function roomListReducer(state = initialRoomListState, action) {
  switch (action.type) {
    case GET_ROOM_LIST_REQUEST:
    case ADD_ROOM_REQUEST:
    case DELETE_ROOM_REQUEST:
    case GET_AVAILABLE_ROOM_LIST_REQUEST:
      return {
        ...state,
        status: 'loading',
        successMsg: null,
        errorMsg: null,
      }
    case GET_ROOM_LIST_SUCCESS:
      return {
        ...state,
        status: 'loaded',
        roomList: action.payload,
      }
    case ADD_ROOM_SUCCESS:
      return {
        ...state,
        status: 'loaded',
        successMsg: action.payload.message,
        roomList: [...state.roomList, action.payload.data],
      }
    case DELETE_ROOM_SUCCESS:
      return {
        ...state,
        status: 'loaded',
        successMsg: action.payload.message,
        roomList: state.roomList.filter((i) => i.id !== action.payload.data),
      }
    case GET_AVAILABLE_ROOM_LIST_SUCCESS:
      return {
        ...state,
        status: 'success',
        availableRooms: action.payload.data,
      }
    case GET_ROOM_LIST_FAIL:
    case ADD_ROOM_FAIL:
    case DELETE_ROOM_FAIL:
    case GET_AVAILABLE_ROOM_LIST_FAIL:
      return {
        ...state,
        status: 'error',
        errorMsg: action.payload,
      }
    case UPDATE_ROOM_STATUS_FILTER:
      const previousFilter = state.filter
      return {
        ...state,
        filter: {
          ...previousFilter,
          is_available: action.payload,
        },
      }
    default:
      return state
  }
}

const initialRoomState = {
  status: 'idle',
  errorMsg: null,
  successMsg: null,
}

export function roomReducer(state = initialRoomState, action) {
  switch (action.type) {
    case EDIT_ROOM_REQUEST:
      return {
        ...state,
        status: 'loading',
        successMsg: null,
      }
    case EDIT_ROOM_SUCCESS:
      return {
        ...state,
        status: 'updated',
        successMsg: action.payload.message,
      }
    case EDIT_ROOM_FAIL:
      return {
        ...state,
        status: 'error',
        errorMsg: action.payload,
      }
    case RESET_ROOM:
      return initialRoomState
    default:
      return state
  }
}
