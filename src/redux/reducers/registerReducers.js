import {
  VERIFY_REGISTER_TOKEN_REQUEST,
  VERIFY_REGISTER_TOKEN_FAIL,
  VERIFY_REGISTER_TOKEN_SUCCESS,
} from '../types/registerTypes'

const initialInviteListState = {
  status: 'idle',
  errorMsg: null,
}

export function registerReducer(state = initialInviteListState, action) {
  switch (action.type) {
    case VERIFY_REGISTER_TOKEN_REQUEST:
      return {
        ...state,
        errorMsg: null,
        status: 'loading',
      }
    case VERIFY_REGISTER_TOKEN_SUCCESS:
      return {
        ...state,
        status: 'success',
      }
    case VERIFY_REGISTER_TOKEN_FAIL:
      return {
        ...state,
        status: 'error',
        errorMsg: action.payload,
      }
    default:
      return state
  }
}
