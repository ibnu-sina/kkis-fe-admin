import {
  GET_STAY_REQUEST_LIST_FAIL,
  GET_STAY_REQUEST_LIST_REQUEST,
  GET_STAY_REQUEST_LIST_SUCCESS,
  GET_STAY_REQUEST_FAIL,
  GET_STAY_REQUEST_REQUEST,
  GET_STAY_REQUEST_SUCCESS,
  APPROVE_STAY_REQUEST_FAIL,
  APPROVE_STAY_REQUEST_REQUEST,
  APPROVE_STAY_REQUEST_SUCCESS,
} from '../types/stayRequestListTypes'

const initialStayRequestListState = {
  stayRequestList: [],
  status: 'idle',
  errorMsg: null,
  successMsg: null,
}

export function stayRequestListReducer(state = initialStayRequestListState, action) {
  switch (action.type) {
    case GET_STAY_REQUEST_LIST_REQUEST:
      return {
        ...state,
        status: 'loading',
      }
    case GET_STAY_REQUEST_LIST_SUCCESS:
      return {
        ...state,
        status: 'loaded',
        stayRequestList: action.payload,
      }
    case GET_STAY_REQUEST_LIST_FAIL:
      return {
        ...state,
        status: 'error',
        errorMsg: action.payload,
      }
    default:
      return state
  }
}

const initialStayRequestState = {
  stayRequest: {},
  status: 'idle',
  errorMsg: null,
}

export function stayRequestReducer(state = initialStayRequestState, action) {
  switch (action.type) {
    case GET_STAY_REQUEST_REQUEST:
    case APPROVE_STAY_REQUEST_REQUEST:
      return {
        ...state,
        errorMsg: null,
        status: 'loading',
      }
    case GET_STAY_REQUEST_SUCCESS:
    case APPROVE_STAY_REQUEST_SUCCESS:
      return {
        ...state,
        status: 'loaded',
        stayRequest: action.payload.data,
      }
    case GET_STAY_REQUEST_FAIL:
    case APPROVE_STAY_REQUEST_FAIL:
      return {
        ...state,
        status: 'error',
        errorMsg: action.payload,
      }
    default:
      return state
  }
}
