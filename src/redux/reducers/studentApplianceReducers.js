import {
  ATTACH_SERIAL_NO_FAIL,
  ATTACH_SERIAL_NO_REQUEST,
  ATTACH_SERIAL_NO_SUCCESS,
  DELETE_STUDENT_APPLIANCE_FAIL,
  DELETE_STUDENT_APPLIANCE_REQUEST,
  DELETE_STUDENT_APPLIANCE_SUCCESS,
  GET_STUDENT_APPLIANCE_LIST_FAIL,
  GET_STUDENT_APPLIANCE_LIST_REQUEST,
  GET_STUDENT_APPLIANCE_LIST_SUCCESS,
  RESET_STUDENT_APPLIANCE,
} from 'redux/types/studentApplianceTypes'

const initialStudentApplianceListState = {
  applianceList: [],
  status: 'idle',
  errorMsg: null,
  successMsg: null,
}

export function studenApplianceListReducer(state = initialStudentApplianceListState, action) {
  switch (action.type) {
    case GET_STUDENT_APPLIANCE_LIST_REQUEST:
      return {
        ...state,
        status: 'loading',
        successMsg: null,
        errorMsg: null,
      }
    case GET_STUDENT_APPLIANCE_LIST_SUCCESS:
      return {
        ...state,
        status: 'loaded',
        applianceList: action.payload,
      }
    case GET_STUDENT_APPLIANCE_LIST_FAIL:
      return {
        ...state,
        status: 'error',
        errorMsg: action.payload,
      }
    default:
      return state
  }
}

const initialStudentApplianceState = {
  status: 'idle',
  errorMsg: null,
  successMsg: null,
}

export function studentApplianceReducer(state = initialStudentApplianceState, action) {
  switch (action.type) {
    case ATTACH_SERIAL_NO_REQUEST:
    case DELETE_STUDENT_APPLIANCE_REQUEST:
      return {
        ...state,
        status: 'loading',
        successMsg: null,
      }
    case ATTACH_SERIAL_NO_SUCCESS:
      return {
        ...state,
        status: 'updated',
        successMsg: action.payload.message,
      }
    case DELETE_STUDENT_APPLIANCE_SUCCESS:
      return {
        ...state,
        status: 'deleted',
        successMsg: action.payload.message,
      }
    case ATTACH_SERIAL_NO_FAIL:
    case DELETE_STUDENT_APPLIANCE_FAIL:
      return {
        ...state,
        status: 'error',
        errorMsg: action.payload,
      }
    case RESET_STUDENT_APPLIANCE:
      return initialStudentApplianceState
    default:
      return state
  }
}
