import {
  GET_APPLIANCE_LIST_FAIL,
  GET_APPLIANCE_LIST_REQUEST,
  GET_APPLIANCE_LIST_SUCCESS,
  ADD_APPLIANCE_FAIL,
  ADD_APPLIANCE_REQUEST,
  ADD_APPLIANCE_SUCCESS,
  EDIT_APPLIANCE_FAIL,
  EDIT_APPLIANCE_REQUEST,
  EDIT_APPLIANCE_SUCCESS,
  RESET_APPLIANCE,
  DELETE_APPLIANCE_FAIL,
  DELETE_APPLIANCE_REQUEST,
  DELETE_APPLIANCE_SUCCESS,
} from '../types/applianceListTypes'

const initialApplianceListState = {
  applianceList: [],
  status: 'idle',
  errorMsg: null,
  successMsg: null,
}

export function applianceListReducer(state = initialApplianceListState, action) {
  switch (action.type) {
    case GET_APPLIANCE_LIST_REQUEST:
    case ADD_APPLIANCE_REQUEST:
    case DELETE_APPLIANCE_REQUEST:
      return {
        ...state,
        status: 'loading',
        successMsg: null,
        errorMsg: null,
      }
    case GET_APPLIANCE_LIST_SUCCESS:
      return {
        ...state,
        status: 'loaded',
        applianceList: action.payload,
      }
    case ADD_APPLIANCE_SUCCESS:
      return {
        ...state,
        status: 'loaded',
        successMsg: action.payload.message,
        applianceList: [...state.applianceList, action.payload.data],
      }
    case DELETE_APPLIANCE_SUCCESS:
      return {
        ...state,
        status: 'loaded',
        successMsg: action.payload.message,
        applianceList: state.applianceList.filter((i) => i.id !== action.payload.data),
      }
    case GET_APPLIANCE_LIST_FAIL:
    case ADD_APPLIANCE_FAIL:
    case DELETE_APPLIANCE_FAIL:
      return {
        ...state,
        status: 'error',
        errorMsg: action.payload,
      }
    default:
      return state
  }
}

const initialApplianceState = {
  status: 'idle',
  errorMsg: null,
  successMsg: null,
}

export function applianceReducer(state = initialApplianceState, action) {
  switch (action.type) {
    case EDIT_APPLIANCE_REQUEST:
      return {
        ...state,
        status: 'loading',
        successMsg: null,
      }
    case EDIT_APPLIANCE_SUCCESS:
      return {
        ...state,
        status: 'updated',
        successMsg: action.payload.message,
      }
    case EDIT_APPLIANCE_FAIL:
      return {
        ...state,
        status: 'error',
        errorMsg: action.payload,
      }
    case RESET_APPLIANCE:
      return {
        status: 'idle',
        errorMsg: null,
        successMsg: null,
      }
    default:
      return state
  }
}
