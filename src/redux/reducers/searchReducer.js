import { convertCase } from 'helpers'
import { _CLEAR_SEARCH, _ON_CHANGE_SEARCH } from 'redux/types/serachTypes'

export function createSearchReducer(name = '') {
  const ACTION_NAME = convertCase.toKebab(name).split('-').join('_').toUpperCase()
  return function search(state = '', action) {
    switch (action.type) {
      case ACTION_NAME + _ON_CHANGE_SEARCH:
        return action.payload
      case ACTION_NAME + _CLEAR_SEARCH:
        return ''
      default:
        return state
    }
  }
}
