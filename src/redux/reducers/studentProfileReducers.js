import {
  GET_STUDENT_PROFILE_FAIL,
  GET_STUDENT_PROFILE_REQUEST,
  GET_STUDENT_PROFILE_SUCCESS,
  REASSIGN_STUDENT_ROOM_FAIL,
  REASSIGN_STUDENT_ROOM_REQUEST,
  REASSIGN_STUDENT_ROOM_SUCCESS,
  REMOVE_STUDENT_ROOM_FAIL,
  REMOVE_STUDENT_ROOM_REQUEST,
  REMOVE_STUDENT_ROOM_SUCCESS,
  RESET_REASSIGN_STUDENT_ROOM,
} from '../types/studentProfileTypes'

const initialStudentProfileState = {
  status: 'idle',
  errorMsg: null,
  studentProfile: {},
}

export function studentProfileReducer(state = initialStudentProfileState, action) {
  switch (action.type) {
    case GET_STUDENT_PROFILE_REQUEST:
      return {
        ...state,
        errorMsg: null,
        status: 'loading',
      }
    case GET_STUDENT_PROFILE_SUCCESS:
      return {
        ...state,
        status: 'loaded',
        studentProfile: action.payload.data,
      }
    case GET_STUDENT_PROFILE_FAIL:
      return {
        ...state,
        status: 'error',
        errorMsg: action.payload,
      }
    default:
      return state
  }
}

const initialStudentRoomState = {
  status: 'idle',
  successMsg: null,
  errorMsg: null,
  studentRoom: {},
}

export function studentRoomReducer(state = initialStudentRoomState, action) {
  switch (action.type) {
    case REASSIGN_STUDENT_ROOM_REQUEST:
    case REMOVE_STUDENT_ROOM_REQUEST:
      return {
        ...state,
        status: 'loading',
      }
    case REASSIGN_STUDENT_ROOM_SUCCESS:
    case REMOVE_STUDENT_ROOM_SUCCESS:
      return {
        ...state,
        status: 'room_success',
        successMsg: action.payload.message,
        studentRoom: action.payload.data,
      }
    case REASSIGN_STUDENT_ROOM_FAIL:
    case REMOVE_STUDENT_ROOM_FAIL:
      return {
        ...state,
        status: 'room_error',
        errorMsg: action.payload,
      }
    case RESET_REASSIGN_STUDENT_ROOM:
      return initialStudentRoomState
    default:
      return state
  }
}
