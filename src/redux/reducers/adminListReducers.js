import {
  CLEAR_SEARCH_ADMIN,
  GET_ADMIN_LIST_FAIL,
  GET_ADMIN_LIST_REQUEST,
  GET_ADMIN_LIST_SUCCESS,
  ON_SEARCH_CHANGE_ADMIN,
} from '../types/adminListTypes'

const initialAdminListState = {
  searchQuery: '',
  adminList: [],
  status: 'idle',
  errorMsg: null,
  successMsg: null,
}

export function adminListReducer(state = initialAdminListState, action) {
  switch (action.type) {
    case GET_ADMIN_LIST_REQUEST:
      return {
        ...state,
        status: 'loading',
      }
    case GET_ADMIN_LIST_SUCCESS:
      return {
        ...state,
        status: 'loaded',
        adminList: action.payload,
      }
    case GET_ADMIN_LIST_FAIL:
      return {
        ...state,
        status: 'error',
        errorMsg: action.payload,
      }
    case ON_SEARCH_CHANGE_ADMIN:
      return {
        ...state,
        searchQuery: action.payload,
      }
    case CLEAR_SEARCH_ADMIN:
      return {
        ...state,
        searchQuery: '',
      }
    default:
      return state
  }
}
