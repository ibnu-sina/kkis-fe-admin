import {
  UPDATE_STUDENT_COURSE_FILTER,
  UPDATE_STUDENT_ROOM_FILTER,
} from 'redux/types/studentListTypes'
import {
  GET_STUDENT_INVITE_LIST_REQUEST,
  GET_STUDENT_INVITE_LIST_FAIL,
  GET_STUDENT_INVITE_LIST_SUCCESS,
  ADD_STUDENT_INVITE_LIST_REQUEST,
  ADD_STUDENT_INVITE_LIST_FAIL,
  ADD_STUDENT_INVITE_LIST_SUCCESS,
  DELETE_STUDENT_INVITE_LIST_REQUEST,
  DELETE_STUDENT_INVITE_LIST_FAIL,
  DELETE_STUDENT_INVITE_LIST_SUCCESS,
  RENEW_STUDENT_INVITE_LIST_REQUEST,
  RENEW_STUDENT_INVITE_LIST_SUCCESS,
  RENEW_STUDENT_INVITE_LIST_FAIL,
  UPLOAD_STUDENT_INVITE_LIST_REQUEST,
  UPLOAD_STUDENT_INVITE_LIST_SUCCESS,
  UPLOAD_STUDENT_INVITE_LIST_FAIL,
  ADD_MULTIPLE_STUDENT_INVITE_LIST_FAIL,
  ADD_MULTIPLE_STUDENT_INVITE_LIST_REQUEST,
  ADD_MULTIPLE_STUDENT_INVITE_LIST_SUCCESS,
  REMOVE_STUDENT_INVITE_LIST_ITEM,
  UPDATE_STUDENT_INVITE_LIST_ITEM,
} from '../types/studentInviteListTypes'

const initialStudentInviteListState = {
  filter: { roomStatus: '', roomId: '', courseId: '' },
  studentInviteList: [],
  status: 'idle',
  errorMsg: null,
  successMsg: null,
}

export function studentInviteListReducer(state = initialStudentInviteListState, action) {
  switch (action.type) {
    case GET_STUDENT_INVITE_LIST_REQUEST:
    case ADD_STUDENT_INVITE_LIST_REQUEST:
    case DELETE_STUDENT_INVITE_LIST_REQUEST:
    case RENEW_STUDENT_INVITE_LIST_REQUEST:
      return {
        ...state,
        errorMsg: null,
        successMsg: null,
        status: 'loading',
      }
    case GET_STUDENT_INVITE_LIST_SUCCESS:
      return {
        ...state,
        status: 'loaded',
        studentInviteList: action.payload,
      }
    case GET_STUDENT_INVITE_LIST_FAIL:
    case DELETE_STUDENT_INVITE_LIST_FAIL:
    case RENEW_STUDENT_INVITE_LIST_FAIL:
      return {
        ...state,
        status: 'error',
        errorMsg: action.payload,
      }
    case ADD_STUDENT_INVITE_LIST_SUCCESS:
      return {
        ...state,
        status: 'added',
        successMsg: action.payload.message,
        studentInviteList: [...state.studentInviteList, action.payload.data],
      }
    case ADD_STUDENT_INVITE_LIST_FAIL:
      return {
        ...state,
        status: 'invite_error',
        errorMsg: action.payload,
      }
    case DELETE_STUDENT_INVITE_LIST_SUCCESS:
      return {
        ...state,
        status: 'deleted',
        studentInviteList: state.studentInviteList.filter((i) => i.id !== action.payload.data),
        successMsg: action.payload.message,
      }
    case RENEW_STUDENT_INVITE_LIST_SUCCESS:
      const filter = state.studentInviteList.map((i) => {
        if (i.id === action.payload.data.id) {
          return {
            ...i,
            expired_at: action.payload.data.expired_at,
          }
        }
        return i
      })
      return {
        ...state,
        status: 'updated',
        successMsg: action.payload.message,
        studentInviteList: filter,
      }
    case UPDATE_STUDENT_ROOM_FILTER: {
      const previousFilter = state.filter
      return {
        ...state,
        filter: {
          ...previousFilter,
          roomId: action.payload,
        },
      }
    }
    case UPDATE_STUDENT_COURSE_FILTER: {
      const previousFilter = state.filter
      return {
        ...state,
        filter: {
          ...previousFilter,
          courseId: action.payload,
        },
      }
    }
    default:
      return state
  }
}

const initialUploadStudentInviteList = {
  studentInviteList: [],
  status: 'idle',
  errorMsg: null,
  successMsg: null,
}

export function uploadStudentInviteListReducer(state = initialUploadStudentInviteList, action) {
  switch (action.type) {
    case UPLOAD_STUDENT_INVITE_LIST_REQUEST:
    case ADD_MULTIPLE_STUDENT_INVITE_LIST_REQUEST:
      return {
        ...state,
        errorMsg: null,
        successMsg: null,
        status: 'loading',
      }
    case UPLOAD_STUDENT_INVITE_LIST_SUCCESS:
      return {
        ...state,
        status: 'uploaded',
        studentInviteList: action.payload.data,
      }
    case UPLOAD_STUDENT_INVITE_LIST_FAIL:
      return {
        ...state,
        status: 'error',
        errorMsg: action.payload,
      }
    case ADD_MULTIPLE_STUDENT_INVITE_LIST_SUCCESS:
      return {
        ...state,
        status: 'created',
        successMsg: action.payload.message,
        studentInviteList: [],
      }
    case ADD_MULTIPLE_STUDENT_INVITE_LIST_FAIL:
      return {
        ...state,
        status: 'error',
        errorMsg: action.payload.message,
        studentInviteList: action.payload.data,
      }
    case REMOVE_STUDENT_INVITE_LIST_ITEM:
      const isLastItem = state.studentInviteList.length === 1
      if (isLastItem) return initialUploadStudentInviteList

      return {
        ...state,
        studentInviteList: action.payload,
      }

    case UPDATE_STUDENT_INVITE_LIST_ITEM:
      return {
        ...state,
        studentInviteList: action.payload,
      }
    default:
      return state
  }
}
