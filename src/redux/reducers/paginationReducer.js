import { convertCase } from 'helpers'
import { _CHANGE_PAGE, _CHANGE_LIMIT, _SET_PAGINATION } from 'redux/types/paginationTypes'

const initialPagination = {
  total: 1,
  page: 1,
  perPage: 10,
  lastPage: null,
}

export function createPaginationReducer(name = '') {
  const ACTION_NAME = convertCase.toKebab(name).split('-').join('_').toUpperCase()
  return function pagination(state = initialPagination, action) {
    switch (action.type) {
      case ACTION_NAME + _CHANGE_PAGE:
        return {
          ...state,
          page: action.payload,
        }
      case ACTION_NAME + _CHANGE_LIMIT:
        return {
          ...state,
          perPage: action.payload,
        }
      case ACTION_NAME + _SET_PAGINATION:
        return {
          ...action.payload,
        }
      default:
        return state
    }
  }
}
