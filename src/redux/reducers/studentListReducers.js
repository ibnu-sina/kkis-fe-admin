import {
  GET_STUDENT_LIST_FAIL,
  GET_STUDENT_LIST_REQUEST,
  GET_STUDENT_LIST_SUCCESS,
  UPDATE_STUDENT_COURSE_FILTER,
  UPDATE_STUDENT_ROOM_FILTER,
  UPDATE_STUDENT_ROOM_STATUS_FILTER,
} from '../types/studentListTypes'

const initialStudentListState = {
  filter: { roomStatus: '', roomId: '', courseId: '' },
  studentList: [],
  status: 'idle',
  errorMsg: null,
  successMsg: null,
}

export function studentListReducer(state = initialStudentListState, action) {
  switch (action.type) {
    case GET_STUDENT_LIST_REQUEST:
      return {
        ...state,
        status: 'loading',
      }
    case GET_STUDENT_LIST_SUCCESS:
      return {
        ...state,
        status: 'loaded',
        studentList: action.payload,
      }
    case GET_STUDENT_LIST_FAIL:
      return {
        ...state,
        status: 'error',
        errorMsg: action.payload,
      }
    case UPDATE_STUDENT_ROOM_STATUS_FILTER: {
      const previousFilter = state.filter
      return {
        ...state,
        filter: {
          ...previousFilter,
          roomStatus: action.payload,
        },
      }
    }
    case UPDATE_STUDENT_ROOM_FILTER: {
      const previousFilter = state.filter
      return {
        ...state,
        filter: {
          ...previousFilter,
          roomId: action.payload,
        },
      }
    }
    case UPDATE_STUDENT_COURSE_FILTER: {
      const previousFilter = state.filter
      return {
        ...state,
        filter: {
          ...previousFilter,
          courseId: action.payload,
        },
      }
    }
    default:
      return state
  }
}
