import {
  GET_ACTIVITY_LIST_REQUEST,
  GET_ACTIVITY_LIST_SUCCESS,
  GET_ACTIVITY_LIST_FAIL,
  UPDATE_ACTIVITY_REQUEST,
  UPDATE_ACTIVITY_SUCCESS,
  UPDATE_ACTIVITY_FAIL,
} from '../types/activityListTypes'

const initialActivityListState = {
  activityList: [],
  status: 'idle',
  errorMsg: null,
  successMsg: null,
}

export function activityListReducer(state = initialActivityListState, action) {
  switch (action.type) {
    case GET_ACTIVITY_LIST_REQUEST:
    case UPDATE_ACTIVITY_REQUEST:
      return {
        ...state,
        status: 'loading',
      }
    case GET_ACTIVITY_LIST_SUCCESS:
      return {
        ...state,
        status: 'loaded',
        activityList: action.payload,
      }
    case GET_ACTIVITY_LIST_FAIL:
    case UPDATE_ACTIVITY_FAIL:
      return {
        ...state,
        status: 'error',
        errorMsg: action.payload,
      }
    case UPDATE_ACTIVITY_SUCCESS:
      const updated = state.activityList.map((i) => {
        if (i.id === action.payload.data.id) {
          return action.payload.data
        }
        return i
      })

      return {
        ...state,
        status: 'updated',
        successMsg: action.payload.message,
        activityList: updated,
      }
    default:
      return state
  }
}
