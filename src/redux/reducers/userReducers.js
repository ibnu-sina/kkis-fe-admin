import jwt_decode from 'jwt-decode'
import { DELETE_PROFILE_SUCCESS } from '../types/profileTypes'
import {
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAIL,
  USER_REFRESH_TOKEN_REQUEST,
  USER_REFRESH_TOKEN_SUCCESS,
  USER_REFRESH_TOKEN_FAIL,
  USER_REGISTER_REQUEST,
  USER_REGISTER_SUCCESS,
  USER_REGISTER_FAIL,
  USER_LOGOUT_FAIL,
  USER_LOGOUT_REQUEST,
  USER_UPDATE_PASSWORD_FAIL,
  USER_UPDATE_PASSWORD_REQUEST,
  USER_UPDATE_PASSWORD_SUCCESS,
  GET_MY_PROFILE_REQUEST,
  GET_MY_PROFILE_SUCCESS,
  GET_MY_PROFILE_FAIL,
} from '../types/userTypes'

const initialLoginState = {
  isLoggedIn: false,
  uid: '',
  token: '',
  refreshToken: '',
  loading: false,
  error: null,
  success: null,
}

export function userLoginReducer(state = initialLoginState, action) {
  switch (action.type) {
    case USER_LOGIN_REQUEST:
    case USER_REFRESH_TOKEN_REQUEST:
    case USER_REGISTER_REQUEST:
    case USER_LOGOUT_REQUEST:
    case USER_UPDATE_PASSWORD_REQUEST:
      return {
        ...state,
        error: null,
        success: null,
        loading: true,
      }
    case USER_LOGIN_SUCCESS:
    case USER_REFRESH_TOKEN_SUCCESS:
    case USER_REGISTER_SUCCESS:
      const { uid } = jwt_decode(action.payload.data.token)
      return {
        ...state,
        loading: false,
        isLoggedIn: true,
        token: action.payload.data.token,
        refreshToken: action.payload.data.refreshToken,
        uid,
      }
    case USER_LOGIN_FAIL:
    case USER_REFRESH_TOKEN_FAIL:
    case USER_REGISTER_FAIL:
    case USER_LOGOUT_FAIL:
    case USER_UPDATE_PASSWORD_FAIL:
      return {
        ...state,
        loading: false,
        isLoggedIn: false,
        error: action.payload,
      }
    case DELETE_PROFILE_SUCCESS:
      return {
        ...state,
        isLoggedIn: false,
        uid: '',
        token: '',
        refreshToken: '',
        loading: false,
        error: null,
      }
    case USER_UPDATE_PASSWORD_SUCCESS:
      return {
        ...state,
        loading: false,
        success: action.payload.message,
      }
    default:
      return state
  }
}

const initialProfileState = {
  profile: {},
  status: 'idle',
  error: null,
}

export function userProfileReducer(state = initialProfileState, action) {
  switch (action.type) {
    case GET_MY_PROFILE_REQUEST:
      return {
        ...state,
        status: 'loading',
      }
    case GET_MY_PROFILE_SUCCESS:
      return {
        ...state,
        status: 'loaded',
        profile: action.payload.data,
      }
    case GET_MY_PROFILE_FAIL:
      return {
        ...state,
        status: 'error',
        error: action.payload,
      }
    default:
      return state
  }
}
