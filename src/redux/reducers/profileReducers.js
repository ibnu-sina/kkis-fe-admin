import {
  GET_PROFILE_FAIL,
  GET_PROFILE_REQUEST,
  GET_PROFILE_SUCCESS,
  EDIT_PROFILE_FAIL,
  EDIT_PROFILE_REQUEST,
  EDIT_PROFILE_SUCCESS,
  DELETE_PROFILE_FAIL,
  DELETE_PROFILE_REQUEST,
  DELETE_PROFILE_SUCCESS,
  EDIT_PASSWORD_FAIL,
  EDIT_PASSWORD_REQUEST,
  EDIT_PASSWORD_SUCCESS,
} from '../types/profileTypes'

const initialProfileState = {
  status: 'idle',
  errorMsg: null,
  successMsg: null,
  adminProfile: {},
}

export function profileReducer(state = initialProfileState, action) {
  switch (action.type) {
    case GET_PROFILE_REQUEST:
    case EDIT_PROFILE_REQUEST:
    case DELETE_PROFILE_REQUEST:
    case EDIT_PASSWORD_REQUEST:
      return {
        ...state,
        errorMsg: null,
        successMsg: null,
        status: 'loading',
      }
    case GET_PROFILE_SUCCESS:
    case EDIT_PROFILE_SUCCESS:
      return {
        ...state,
        status: 'loaded',
        successMsg: action.payload.message,
        adminProfile: action.payload.data,
      }
    case GET_PROFILE_FAIL:
    case EDIT_PROFILE_FAIL:
    case DELETE_PROFILE_FAIL:
    case EDIT_PASSWORD_FAIL:
      return {
        ...state,
        status: 'error',
        errorMsg: action.payload,
      }
    case DELETE_PROFILE_SUCCESS:
      return {
        ...state,
        status: 'idle',
        errorMsg: null,
        successMsg: null,
        adminProfile: {},
      }
    case EDIT_PASSWORD_SUCCESS:
      return {
        ...state,
        status: 'password_success',
        successMsg: action.payload.message,
      }
    default:
      return state
  }
}
