import {
  CLAIM_INVENTORY_STORAGE_FAIL,
  CLAIM_INVENTORY_STORAGE_REQUEST,
  CLAIM_INVENTORY_STORAGE_SUCCESS,
  GET_INVENTORY_STORAGE_LIST_FAIL,
  GET_INVENTORY_STORAGE_LIST_REQUEST,
  GET_INVENTORY_STORAGE_LIST_SUCCESS,
  RESET_INVENTORY_STORAGE,
  RESOLVE_INVENTORY_STORAGE_FAIL,
  RESOLVE_INVENTORY_STORAGE_REQUEST,
  RESOLVE_INVENTORY_STORAGE_SUCCESS,
  UPDATE_INVENTORY_STORAGE_STATUS_FILTER,
} from '../types/inventoryStorageTypes'

const initialInventoryStorageListState = {
  filter: { status: '' },
  inventoryStorageList: [],
  status: 'idle',
  errorMsg: null,
  successMsg: null,
}

export function inventoryStorageListReducer(state = initialInventoryStorageListState, action) {
  switch (action.type) {
    case GET_INVENTORY_STORAGE_LIST_REQUEST:
      return {
        ...state,
        status: 'loading',
      }
    case GET_INVENTORY_STORAGE_LIST_SUCCESS:
      return {
        ...state,
        status: 'loaded',
        inventoryStorageList: action.payload,
      }
    case GET_INVENTORY_STORAGE_LIST_FAIL:
      return {
        ...state,
        status: 'error',
        errorMsg: action.payload,
      }
    case UPDATE_INVENTORY_STORAGE_STATUS_FILTER:
      const previousFilter = state.filter
      return {
        ...state,
        filter: {
          ...previousFilter,
          status: action.payload,
        },
      }
    default:
      return state
  }
}

const initialInventoryStorageState = {
  inventoryStorage: {},
  status: 'idle',
  errorMsg: null,
  successMsg: null,
}

export function inventoryStorageReducer(state = initialInventoryStorageState, action) {
  switch (action.type) {
    case RESOLVE_INVENTORY_STORAGE_REQUEST:
    case CLAIM_INVENTORY_STORAGE_REQUEST:
      return {
        ...state,
        status: 'loading',
      }
    case RESOLVE_INVENTORY_STORAGE_SUCCESS:
    case CLAIM_INVENTORY_STORAGE_SUCCESS:
      return {
        ...state,
        status: 'updated',
        successMsg: action.payload.message,
        inventoryStorage: action.payload.data,
      }
    case RESOLVE_INVENTORY_STORAGE_FAIL:
    case CLAIM_INVENTORY_STORAGE_FAIL:
      return {
        ...state,
        status: 'error',
        errorMsg: action.payload,
      }
    case RESET_INVENTORY_STORAGE:
      return initialInventoryStorageState
    default:
      return state
  }
}
