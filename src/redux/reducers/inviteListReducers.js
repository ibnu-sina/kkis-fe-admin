import {
  GET_INVITE_LIST_REQUEST,
  GET_INVITE_LIST_FAIL,
  GET_INVITE_LIST_SUCCESS,
  ADD_INVITE_LIST_REQUEST,
  ADD_INVITE_LIST_SUCCESS,
  ADD_INVITE_LIST_FAIL,
  DELETE_INVITE_LIST_REQUEST,
  DELETE_INVITE_LIST_SUCCESS,
  DELETE_INVITE_LIST_FAIL,
  RENEW_INVITE_LIST_REQUEST,
  RENEW_INVITE_LIST_SUCCESS,
  RENEW_INVITE_LIST_FAIL,
} from '../types/inviteListTypes'

const initialInviteListState = {
  inviteList: [],
  status: 'idle',
  errorMsg: null,
  successMsg: null,
}

export function inviteListReducer(state = initialInviteListState, action) {
  switch (action.type) {
    case GET_INVITE_LIST_REQUEST:
    case ADD_INVITE_LIST_REQUEST:
    case DELETE_INVITE_LIST_REQUEST:
    case RENEW_INVITE_LIST_REQUEST:
      return {
        ...state,
        errorMsg: null,
        successMsg: null,
        status: 'loading',
      }
    case GET_INVITE_LIST_SUCCESS:
      return {
        ...state,
        status: 'loaded',
        inviteList: action.payload,
      }
    case GET_INVITE_LIST_FAIL:
    case DELETE_INVITE_LIST_FAIL:
    case RENEW_INVITE_LIST_FAIL:
      return {
        ...state,
        status: 'error',
        errorMsg: action.payload,
      }

    case ADD_INVITE_LIST_SUCCESS:
      return {
        ...state,
        status: 'loaded',
        successMsg: action.payload.message,
        inviteList: [...state.inviteList, action.payload.data],
      }
    case ADD_INVITE_LIST_FAIL:
      return {
        ...state,
        status: 'invite_error',
        errorMsg: action.payload,
      }
    case DELETE_INVITE_LIST_SUCCESS:
      return {
        ...state,
        status: 'loaded',
        inviteList: state.inviteList.filter((i) => i.id !== action.payload.data),
        successMsg: action.payload.message,
      }
    case RENEW_INVITE_LIST_SUCCESS:
      const filter = state.inviteList.map((i) => {
        if (i.id === action.payload.data.id) {
          return {
            ...i,
            expired_at: action.payload.data.expired_at,
          }
        }
        return i
      })
      return {
        ...state,
        status: 'updated',
        successMsg: action.payload.message,
        inviteList: filter,
      }
    default:
      return state
  }
}
