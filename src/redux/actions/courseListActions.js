import axios from 'axios'
import queryString from 'query-string'
import {
  GET_COURSE_LIST_FAIL,
  GET_COURSE_LIST_REQUEST,
  GET_COURSE_LIST_SUCCESS,
  ADD_COURSE_REQUEST,
  ADD_COURSE_SUCCESS,
  ADD_COURSE_FAIL,
  GET_COURSE_REQUEST,
  GET_COURSE_SUCCESS,
  GET_COURSE_FAIL,
  EDIT_COURSE_REQUEST,
  EDIT_COURSE_SUCCESS,
  EDIT_COURSE_FAIL,
  TOGGLE_COURSE_REQUEST,
  TOGGLE_COURSE_FAIL,
  TOGGLE_COURSE_SUCCESS,
  DELETE_COURSE_REQUEST,
  DELETE_COURSE_FAIL,
  DELETE_COURSE_SUCCESS,
  COURSE_LIST_SET_PAGINATION,
} from '../types/courseListTypes'

export const getCourseList = () => async (dispatch, getState) => {
  const { page, perPage } = getState().courseListPagination
  const searchQuery = getState().courseListSearchQuery
  const apiQuery = { page, limit: perPage, q: searchQuery }
  const formattedApiQuery = queryString.stringify(apiQuery, { skipEmptyString: true })
  try {
    dispatch({ type: GET_COURSE_LIST_REQUEST })
    const response = await axios.get(`/courses?${formattedApiQuery}`)

    const { data, ...pagination } = response.data
    dispatch({ type: COURSE_LIST_SET_PAGINATION, payload: pagination })
    dispatch({ type: GET_COURSE_LIST_SUCCESS, payload: data })
  } catch (error) {
    dispatch({
      type: GET_COURSE_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const addCourse = (course) => async (dispatch) => {
  try {
    dispatch({ type: ADD_COURSE_REQUEST })
    const { data } = await axios.post('/courses', course)
    dispatch({
      type: ADD_COURSE_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: ADD_COURSE_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const deleteCourse = (id) => async (dispatch) => {
  try {
    dispatch({ type: DELETE_COURSE_REQUEST })
    const { data } = await axios.delete(`/courses/${id}`)
    dispatch({
      type: DELETE_COURSE_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: DELETE_COURSE_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const editCourse = (course, id) => async (dispatch) => {
  try {
    dispatch({ type: EDIT_COURSE_REQUEST })
    const { data } = await axios.put(`/courses/${id}`, course)
    dispatch({
      type: EDIT_COURSE_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: EDIT_COURSE_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const toggleCourse = (action, id) => async (dispatch) => {
  try {
    dispatch({ type: TOGGLE_COURSE_REQUEST })
    const { data } = await axios.put(`/courses/${id}/${action}`)
    dispatch({
      type: TOGGLE_COURSE_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: TOGGLE_COURSE_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const getCourse = (id) => async (dispatch) => {
  try {
    dispatch({ type: GET_COURSE_REQUEST })
    const { data } = await axios.get(`/courses/${id}?students=true`)
    dispatch({
      type: GET_COURSE_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: GET_COURSE_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}
