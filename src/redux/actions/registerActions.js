import axios from 'axios'
import {
  VERIFY_REGISTER_TOKEN_REQUEST,
  VERIFY_REGISTER_TOKEN_FAIL,
  VERIFY_REGISTER_TOKEN_SUCCESS,
} from '../types/registerTypes'

export const verifyToken = (token) => async (dispatch) => {
  try {
    dispatch({ type: VERIFY_REGISTER_TOKEN_REQUEST })
    await axios.get(`/invite/${token}`)
    dispatch({
      type: VERIFY_REGISTER_TOKEN_SUCCESS,
    })
  } catch (error) {
    dispatch({
      type: VERIFY_REGISTER_TOKEN_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}
