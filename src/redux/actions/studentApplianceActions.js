import axios from 'axios'
import queryString from 'query-string'
import {
  ATTACH_SERIAL_NO_FAIL,
  ATTACH_SERIAL_NO_REQUEST,
  ATTACH_SERIAL_NO_SUCCESS,
  DELETE_STUDENT_APPLIANCE_FAIL,
  DELETE_STUDENT_APPLIANCE_REQUEST,
  DELETE_STUDENT_APPLIANCE_SUCCESS,
  GET_STUDENT_APPLIANCE_LIST_FAIL,
  GET_STUDENT_APPLIANCE_LIST_REQUEST,
  GET_STUDENT_APPLIANCE_LIST_SUCCESS,
  STUDENT_APPLIANCE_LIST_SET_PAGINATION,
} from 'redux/types/studentApplianceTypes'
import { getStudentProfile } from './studentProfileActions'

export const getStudentApplianceList = () => async (dispatch, getState) => {
  const { page, perPage } = getState().studentApplianceListPagination
  const searchQuery = getState().studentApplianceListSearchQuery
  const apiQuery = { page, limit: perPage, q: searchQuery }
  const formattedApiQuery = queryString.stringify(apiQuery, { skipEmptyString: true })
  try {
    dispatch({ type: GET_STUDENT_APPLIANCE_LIST_REQUEST })
    const response = await axios.get(`/appliances/pivot?${formattedApiQuery}`)

    const { data, ...pagination } = response.data

    dispatch({ type: STUDENT_APPLIANCE_LIST_SET_PAGINATION, payload: pagination })
    dispatch({ type: GET_STUDENT_APPLIANCE_LIST_SUCCESS, payload: data })
  } catch (error) {
    dispatch({
      type: GET_STUDENT_APPLIANCE_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const attachSerialNo = (args) => async (dispatch, getState) => {
  const { from, body, student_id, appliance_id } = args
  const { pagination } = getState().studentApplianceList

  try {
    dispatch({ type: ATTACH_SERIAL_NO_REQUEST })
    const { data } = await axios.put(`/appliances/${appliance_id}/${student_id}`, body)
    dispatch({ type: ATTACH_SERIAL_NO_SUCCESS, payload: data })

    if (from === 'student_profile_page') dispatch(getStudentProfile(student_id))
    if (from === 'student_appliance_page') {
      const paginationArgs = {
        page: pagination.page,
        limit: pagination.perPage,
      }
      dispatch(getStudentApplianceList(paginationArgs))
    }
  } catch (error) {
    dispatch({
      type: ATTACH_SERIAL_NO_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const deleteStudentAppliance = ({ from, student_id, appliance_id }) => async (dispatch) => {
  try {
    dispatch({ type: DELETE_STUDENT_APPLIANCE_REQUEST })
    const { data } = await axios.delete(`/appliances/${appliance_id}/${student_id}`)
    dispatch({ type: DELETE_STUDENT_APPLIANCE_SUCCESS, payload: data })

    if (from === 'student_profile_page') dispatch(getStudentProfile(student_id))
    if (from === 'student_appliance_page') dispatch(getStudentApplianceList())
  } catch (error) {
    dispatch({
      type: DELETE_STUDENT_APPLIANCE_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}
