import axios from 'axios'
import {
  GET_STUDENT_PROFILE_FAIL,
  GET_STUDENT_PROFILE_REQUEST,
  GET_STUDENT_PROFILE_SUCCESS,
  REASSIGN_STUDENT_ROOM_FAIL,
  REASSIGN_STUDENT_ROOM_REQUEST,
  REASSIGN_STUDENT_ROOM_SUCCESS,
  REMOVE_STUDENT_ROOM_FAIL,
  REMOVE_STUDENT_ROOM_REQUEST,
  REMOVE_STUDENT_ROOM_SUCCESS,
} from '../types/studentProfileTypes'

export const getStudentProfile = (id) => async (dispatch) => {
  try {
    dispatch({ type: GET_STUDENT_PROFILE_REQUEST })
    const { data } = await axios.get(`/students/${id}?attach=course&attach=appliances&attach=room`)
    dispatch({
      type: GET_STUDENT_PROFILE_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: GET_STUDENT_PROFILE_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const reassignRoom = ({ student_id, room_id }) => async (dispatch) => {
  try {
    dispatch({ type: REASSIGN_STUDENT_ROOM_REQUEST })
    const { data } = await axios.put(`/students/${student_id}/room`, { room_id })
    dispatch({
      type: REASSIGN_STUDENT_ROOM_SUCCESS,
      payload: data,
    })
    dispatch(getStudentProfile(student_id))
  } catch (error) {
    dispatch({
      type: REASSIGN_STUDENT_ROOM_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const removeStudentRoom = (student_id) => async (dispatch) => {
  try {
    dispatch({ type: REMOVE_STUDENT_ROOM_REQUEST })
    const { data } = await axios.delete(`/students/${student_id}/room`)
    dispatch({
      type: REMOVE_STUDENT_ROOM_SUCCESS,
      payload: data,
    })
    dispatch(getStudentProfile(student_id))
  } catch (error) {
    dispatch({
      type: REMOVE_STUDENT_ROOM_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}
