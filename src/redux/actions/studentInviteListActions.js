import axios from 'axios'
import queryString from 'query-string'
import {
  GET_STUDENT_INVITE_LIST_REQUEST,
  GET_STUDENT_INVITE_LIST_SUCCESS,
  GET_STUDENT_INVITE_LIST_FAIL,
  ADD_STUDENT_INVITE_LIST_REQUEST,
  ADD_STUDENT_INVITE_LIST_FAIL,
  ADD_STUDENT_INVITE_LIST_SUCCESS,
  DELETE_STUDENT_INVITE_LIST_REQUEST,
  DELETE_STUDENT_INVITE_LIST_FAIL,
  DELETE_STUDENT_INVITE_LIST_SUCCESS,
  RENEW_STUDENT_INVITE_LIST_REQUEST,
  RENEW_STUDENT_INVITE_LIST_SUCCESS,
  RENEW_STUDENT_INVITE_LIST_FAIL,
  UPLOAD_STUDENT_INVITE_LIST_REQUEST,
  UPLOAD_STUDENT_INVITE_LIST_FAIL,
  UPLOAD_STUDENT_INVITE_LIST_SUCCESS,
  ADD_MULTIPLE_STUDENT_INVITE_LIST_REQUEST,
  ADD_MULTIPLE_STUDENT_INVITE_LIST_SUCCESS,
  ADD_MULTIPLE_STUDENT_INVITE_LIST_FAIL,
  REMOVE_STUDENT_INVITE_LIST_ITEM,
  UPDATE_STUDENT_INVITE_LIST_ITEM,
  STUDENT_INVITE_LIST_SET_PAGINATION,
} from '../types/studentInviteListTypes'

export const getStudentInviteList = () => async (dispatch, getState) => {
  const { filter } = getState().studentInviteList
  const { page, perPage } = getState().studentInviteListPagination
  const searchQuery = getState().studentInviteListSearchQuery
  const apiQuery = {
    q: searchQuery,
    attach: ['course', 'room'],
    page,
    limit: perPage,
    room_id: filter.roomId,
    course_id: filter.courseId,
  }

  const formattedApiQuery = queryString.stringify(apiQuery, { skipEmptyString: true })

  try {
    dispatch({ type: GET_STUDENT_INVITE_LIST_REQUEST })
    const response = await axios.get(`/student-invites?${formattedApiQuery}`)

    // Separate data and pagination
    const { data, ...pagination } = response.data

    // Setup pagination state and list state
    dispatch({ type: STUDENT_INVITE_LIST_SET_PAGINATION, payload: pagination })
    dispatch({ type: GET_STUDENT_INVITE_LIST_SUCCESS, payload: data })
  } catch (error) {
    dispatch({
      type: GET_STUDENT_INVITE_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const addStudentInviteList = (body) => async (dispatch) => {
  try {
    dispatch({ type: ADD_STUDENT_INVITE_LIST_REQUEST })
    const { data } = await axios.post('/student-invites', body)
    dispatch({
      type: ADD_STUDENT_INVITE_LIST_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: ADD_STUDENT_INVITE_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const renewStudentInviteList = (id) => async (dispatch) => {
  try {
    dispatch({ type: RENEW_STUDENT_INVITE_LIST_REQUEST })
    const { data } = await axios.put(`/student-invites/${id}/renew`)
    dispatch({
      type: RENEW_STUDENT_INVITE_LIST_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: RENEW_STUDENT_INVITE_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const deleteStudentInviteList = (id) => async (dispatch) => {
  try {
    dispatch({ type: DELETE_STUDENT_INVITE_LIST_REQUEST })
    const { data } = await axios.delete(`/student-invites/${id}`)

    dispatch({
      type: DELETE_STUDENT_INVITE_LIST_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: DELETE_STUDENT_INVITE_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const parseFileUpload = (csv) => async (dispatch) => {
  const file = new FormData()
  file.append('file', csv)

  try {
    dispatch({ type: UPLOAD_STUDENT_INVITE_LIST_REQUEST })
    const { data } = await axios.post('/student-invites/parse', file)
    dispatch({
      type: UPLOAD_STUDENT_INVITE_LIST_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: UPLOAD_STUDENT_INVITE_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const inviteMultipleStudents = (students) => async (dispatch) => {
  try {
    dispatch({ type: ADD_MULTIPLE_STUDENT_INVITE_LIST_REQUEST })
    const { data } = await axios.post('/student-invites/multiple', { students })
    dispatch({
      type: ADD_MULTIPLE_STUDENT_INVITE_LIST_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: ADD_MULTIPLE_STUDENT_INVITE_LIST_FAIL,
      payload: error.response.data,
    })
  }
}

export const updateInviteListItem = (id, newStudentIndo) => async (dispatch, getState) => {
  const students = getState().uploadStudentInviteList.studentInviteList
  const newStudentInviteList = students.map((i) => (i.id !== id ? i : newStudentIndo))
  dispatch({ type: UPDATE_STUDENT_INVITE_LIST_ITEM, payload: newStudentInviteList })
}

export const removeInviteListItem = (id) => async (dispatch, getState) => {
  const students = getState().uploadStudentInviteList.studentInviteList
  const newStudentInviteList = students.filter((i) => i.id !== id)
  dispatch({ type: REMOVE_STUDENT_INVITE_LIST_ITEM, payload: newStudentInviteList })
}
