import axios from 'axios'
import queryString from 'query-string'
import {
  GET_INVITE_LIST_REQUEST,
  GET_INVITE_LIST_SUCCESS,
  GET_INVITE_LIST_FAIL,
  ADD_INVITE_LIST_FAIL,
  ADD_INVITE_LIST_REQUEST,
  ADD_INVITE_LIST_SUCCESS,
  DELETE_INVITE_LIST_FAIL,
  DELETE_INVITE_LIST_REQUEST,
  DELETE_INVITE_LIST_SUCCESS,
  RENEW_INVITE_LIST_REQUEST,
  RENEW_INVITE_LIST_SUCCESS,
  RENEW_INVITE_LIST_FAIL,
  INVITE_LIST_SET_PAGINATION,
} from '../types/inviteListTypes'

export const getInviteList = () => async (dispatch, getState) => {
  const { page, perPage } = getState().inviteListPagination
  const searchQuery = getState().inviteListSearchQuery
  const apiQuery = { page, limit: perPage, q: searchQuery }
  const formattedApiQuery = queryString.stringify(apiQuery, { skipEmptyString: true })

  try {
    dispatch({ type: GET_INVITE_LIST_REQUEST })
    const response = await axios.get(`/invite?${formattedApiQuery}`)

    const { data, ...pagination } = response.data

    dispatch({ type: INVITE_LIST_SET_PAGINATION, payload: pagination })
    dispatch({ type: GET_INVITE_LIST_SUCCESS, payload: data })
  } catch (error) {
    dispatch({
      type: GET_INVITE_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const addInviteList = (email, role) => async (dispatch) => {
  try {
    dispatch({ type: ADD_INVITE_LIST_REQUEST })
    const { data } = await axios.post('/invite', { email, role })
    dispatch({
      type: ADD_INVITE_LIST_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: ADD_INVITE_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const renewInviteList = (id) => async (dispatch) => {
  try {
    dispatch({ type: RENEW_INVITE_LIST_REQUEST })
    const { data } = await axios.put(`/invite/${id}/renew`)
    dispatch({
      type: RENEW_INVITE_LIST_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: RENEW_INVITE_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const deleteInviteList = (id) => async (dispatch) => {
  try {
    dispatch({ type: DELETE_INVITE_LIST_REQUEST })
    const { data } = await axios.delete(`/invite/${id}`)

    dispatch({
      type: DELETE_INVITE_LIST_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: DELETE_INVITE_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}
