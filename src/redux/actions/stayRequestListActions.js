import axios from 'axios'
import queryString from 'query-string'
import {
  GET_STAY_REQUEST_LIST_FAIL,
  GET_STAY_REQUEST_LIST_REQUEST,
  GET_STAY_REQUEST_LIST_SUCCESS,
  GET_STAY_REQUEST_FAIL,
  GET_STAY_REQUEST_REQUEST,
  GET_STAY_REQUEST_SUCCESS,
  APPROVE_STAY_REQUEST_FAIL,
  APPROVE_STAY_REQUEST_REQUEST,
  APPROVE_STAY_REQUEST_SUCCESS,
  STAY_REQUEST_LIST_SET_PAGINATION,
} from '../types/stayRequestListTypes'

export const getStayRequestList = (args = {}) => async (dispatch, getState) => {
  const { status } = args
  const { page, perPage } = getState().stayRequestListPagination
  const apiQuery = { page, limit: perPage, status }
  const formattedApiQuery = queryString.stringify(apiQuery, { skipEmptyString: true })
  try {
    dispatch({ type: GET_STAY_REQUEST_LIST_REQUEST })
    const response = await axios.get(`/stay-requests?${formattedApiQuery}`)

    const { data, ...pagination } = response.data
    dispatch({ type: STAY_REQUEST_LIST_SET_PAGINATION, payload: pagination })
    dispatch({ type: GET_STAY_REQUEST_LIST_SUCCESS, payload: data })
  } catch (error) {
    dispatch({
      type: GET_STAY_REQUEST_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const getStayRequest = (id) => async (dispatch) => {
  try {
    dispatch({ type: GET_STAY_REQUEST_REQUEST })
    const { data } = await axios.get(`/stay-requests/${id}`)
    dispatch({
      type: GET_STAY_REQUEST_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: GET_STAY_REQUEST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const updateStatus = (id, action, body) => async (dispatch) => {
  try {
    dispatch({ type: APPROVE_STAY_REQUEST_REQUEST })
    const { data } = await axios.put(`/stay-requests/${id}/${action}`, body)
    dispatch({
      type: APPROVE_STAY_REQUEST_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: APPROVE_STAY_REQUEST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}
