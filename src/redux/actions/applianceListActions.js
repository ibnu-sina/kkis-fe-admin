import axios from 'axios'
import queryString from 'query-string'
import {
  GET_APPLIANCE_LIST_FAIL,
  GET_APPLIANCE_LIST_REQUEST,
  GET_APPLIANCE_LIST_SUCCESS,
  ADD_APPLIANCE_FAIL,
  ADD_APPLIANCE_REQUEST,
  ADD_APPLIANCE_SUCCESS,
  EDIT_APPLIANCE_FAIL,
  EDIT_APPLIANCE_REQUEST,
  EDIT_APPLIANCE_SUCCESS,
  DELETE_APPLIANCE_FAIL,
  DELETE_APPLIANCE_REQUEST,
  DELETE_APPLIANCE_SUCCESS,
  APPLIANCE_LIST_SET_PAGINATION,
} from '../types/applianceListTypes'

export const getApplianceList = () => async (dispatch, getState) => {
  const { page, perPage } = getState().applianceListPagination
  const apiQuery = { page, limit: perPage }
  const formattedApiQuery = queryString.stringify(apiQuery, { skipEmptyString: true })
  try {
    dispatch({ type: GET_APPLIANCE_LIST_REQUEST })
    const response = await axios.get(`/appliances?${formattedApiQuery}`)

    const { data, ...pagination } = response.data
    dispatch({ type: APPLIANCE_LIST_SET_PAGINATION, payload: pagination })
    dispatch({ type: GET_APPLIANCE_LIST_SUCCESS, payload: data })
  } catch (error) {
    dispatch({
      type: GET_APPLIANCE_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const addAppliance = (appliance) => async (dispatch) => {
  try {
    dispatch({ type: ADD_APPLIANCE_REQUEST })
    const { data } = await axios.post('/appliances', appliance)
    dispatch({
      type: ADD_APPLIANCE_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: ADD_APPLIANCE_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const editAppliance = (appliance, id) => async (dispatch) => {
  try {
    dispatch({ type: EDIT_APPLIANCE_REQUEST })
    const { data } = await axios.put(`/appliances/${id}`, appliance)
    dispatch({ type: EDIT_APPLIANCE_SUCCESS, payload: data })
    dispatch(getApplianceList())
  } catch (error) {
    dispatch({
      type: EDIT_APPLIANCE_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const deleteAppliance = (id) => async (dispatch) => {
  try {
    dispatch({ type: DELETE_APPLIANCE_REQUEST })
    const { data } = await axios.delete(`/appliances/${id}`)
    dispatch({ type: DELETE_APPLIANCE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({
      type: DELETE_APPLIANCE_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}
