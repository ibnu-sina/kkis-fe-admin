import axios from 'axios'
import queryString from 'query-string'
import {
  GET_STUDENT_LIST_FAIL,
  GET_STUDENT_LIST_REQUEST,
  GET_STUDENT_LIST_SUCCESS,
  STUDENT_LIST_SET_PAGINATION,
} from '../types/studentListTypes'

export const getStudentList = () => async (dispatch, getState) => {
  const { filter } = getState().studentList
  const searchQuery = getState().studentListSearchQuery
  const { page, perPage } = getState().studentListPagination
  const apiQuery = {
    attach: ['course', 'room'],
    q: searchQuery,
    page,
    limit: perPage,
    room_status: filter.roomStatus,
    room_id: filter.roomId,
    course_id: filter.courseId,
  }
  const formattedApiQuery = queryString.stringify(apiQuery, { skipEmptyString: true })
  try {
    dispatch({ type: GET_STUDENT_LIST_REQUEST })
    const response = await axios.get(`/students?${formattedApiQuery}`)

    // Separate data and pagination
    const { data, ...pagination } = response.data

    // Setup pagination state and list state
    dispatch({ type: STUDENT_LIST_SET_PAGINATION, payload: pagination })
    dispatch({ type: GET_STUDENT_LIST_SUCCESS, payload: data })
  } catch (error) {
    dispatch({
      type: GET_STUDENT_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}
