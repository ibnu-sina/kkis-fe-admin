import axios from 'axios'
import queryString from 'query-string'
import { MERIT_LIST_SET_PAGINATION } from 'redux/types/meritListTypes'
import {
  GET_ACTIVITY_LIST_FAIL,
  GET_ACTIVITY_LIST_REQUEST,
  GET_ACTIVITY_LIST_SUCCESS,
  UPDATE_ACTIVITY_REQUEST,
  UPDATE_ACTIVITY_SUCCESS,
  UPDATE_ACTIVITY_FAIL,
} from '../types/activityListTypes'

export const getActivityList = (args = {}) => async (dispatch, getState) => {
  const { action, status } = args
  const { page, perPage } = getState().activityListPagination
  const apiQuery = { page, limit: perPage, status }
  const formattedApiQuery = queryString.stringify(apiQuery)
  try {
    dispatch({ type: GET_ACTIVITY_LIST_REQUEST })
    const response = await axios.get(`/student-activities/${action}?${formattedApiQuery}`)

    const { data, ...pagination } = response.data

    dispatch({ type: MERIT_LIST_SET_PAGINATION, payload: pagination })
    dispatch({ type: GET_ACTIVITY_LIST_SUCCESS, payload: data })
  } catch (error) {
    dispatch({
      type: GET_ACTIVITY_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const resolveActivity = (id, action) => async (dispatch) => {
  try {
    dispatch({ type: UPDATE_ACTIVITY_REQUEST })
    const { data } = await axios.put(`/student-activities/${id}/${action}`)

    dispatch({
      type: UPDATE_ACTIVITY_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: UPDATE_ACTIVITY_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}
