import axios from 'axios'
import {
  GET_PROFILE_FAIL,
  GET_PROFILE_REQUEST,
  GET_PROFILE_SUCCESS,
  EDIT_PROFILE_FAIL,
  EDIT_PROFILE_SUCCESS,
  EDIT_PROFILE_REQUEST,
  DELETE_PROFILE_FAIL,
  DELETE_PROFILE_REQUEST,
  DELETE_PROFILE_SUCCESS,
  EDIT_PASSWORD_FAIL,
  EDIT_PASSWORD_REQUEST,
  EDIT_PASSWORD_SUCCESS,
} from '../types/profileTypes'

export const getProfile = (id) => async (dispatch) => {
  try {
    dispatch({ type: GET_PROFILE_REQUEST })
    const { data } = await axios.get(`/admins/${id}`)
    dispatch({
      type: GET_PROFILE_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: GET_PROFILE_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const editMyProfile = ({ name, phone_no, gender }) => async (dispatch) => {
  try {
    dispatch({ type: EDIT_PROFILE_REQUEST })
    const { data } = await axios.put('/admins', { name, phone_no, gender })
    dispatch({
      type: EDIT_PROFILE_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: EDIT_PROFILE_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const deleteAccount = (id) => async (dispatch) => {
  try {
    dispatch({ type: DELETE_PROFILE_REQUEST })
    await axios.delete(`/admins/${id}`)
    dispatch({
      type: DELETE_PROFILE_SUCCESS,
    })
  } catch (error) {
    dispatch({
      type: DELETE_PROFILE_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const updatePassword = (old_password, password) => async (dispatch) => {
  try {
    dispatch({ type: EDIT_PASSWORD_REQUEST })
    const { data } = await axios.post('/auth/change-password', { old_password, password })
    dispatch({
      type: EDIT_PASSWORD_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: EDIT_PASSWORD_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}
