import axios from 'axios'
import queryString from 'query-string'
import {
  ADD_ROOM_FAIL,
  ADD_ROOM_REQUEST,
  ADD_ROOM_SUCCESS,
  DELETE_ROOM_FAIL,
  DELETE_ROOM_REQUEST,
  DELETE_ROOM_SUCCESS,
  EDIT_ROOM_FAIL,
  EDIT_ROOM_REQUEST,
  EDIT_ROOM_SUCCESS,
  GET_AVAILABLE_ROOM_LIST_FAIL,
  GET_AVAILABLE_ROOM_LIST_REQUEST,
  GET_AVAILABLE_ROOM_LIST_SUCCESS,
  GET_ROOM_LIST_FAIL,
  GET_ROOM_LIST_REQUEST,
  GET_ROOM_LIST_SUCCESS,
  ROOM_LIST_SET_PAGINATION,
} from 'redux/types/roomTypes'

export const getRoomList = () => async (dispatch, getState) => {
  const { filter } = getState().roomList
  const { page, perPage } = getState().roomListPagination
  const apiQuery = {
    page,
    limit: perPage,
    is_available: filter.is_available,
  }
  const formattedApiQuery = queryString.stringify(apiQuery, { skipEmptyString: true })
  try {
    dispatch({ type: GET_ROOM_LIST_REQUEST })
    const response = await axios.get(`/rooms?${formattedApiQuery}`)

    // Destructure data from pagination info
    const { data, ...pagination } = response.data

    // Set pagination and pass data to list reducer
    dispatch({ type: ROOM_LIST_SET_PAGINATION, payload: pagination })
    dispatch({ type: GET_ROOM_LIST_SUCCESS, payload: data })
  } catch (error) {
    dispatch({
      type: GET_ROOM_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const getAvailableRoomList = () => async (dispatch) => {
  try {
    dispatch({ type: GET_AVAILABLE_ROOM_LIST_REQUEST })
    const { data } = await axios.get('/rooms/available')
    dispatch({
      type: GET_AVAILABLE_ROOM_LIST_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: GET_AVAILABLE_ROOM_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const addRoom = (body) => async (dispatch) => {
  try {
    dispatch({ type: ADD_ROOM_REQUEST })
    const { data } = await axios.post('/rooms', body)
    dispatch({
      type: ADD_ROOM_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: ADD_ROOM_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const editRoom = (body, id) => async (dispatch) => {
  try {
    dispatch({ type: EDIT_ROOM_REQUEST })
    const { data } = await axios.put(`/rooms/${id}`, body)
    dispatch({ type: EDIT_ROOM_SUCCESS, payload: data })
    dispatch(getRoomList())
  } catch (error) {
    dispatch({
      type: EDIT_ROOM_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const deleteRoom = (id) => async (dispatch) => {
  try {
    dispatch({ type: DELETE_ROOM_REQUEST })
    const { data } = await axios.delete(`/rooms/${id}`)
    dispatch({ type: DELETE_ROOM_SUCCESS, payload: data })
  } catch (error) {
    dispatch({
      type: DELETE_ROOM_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}
