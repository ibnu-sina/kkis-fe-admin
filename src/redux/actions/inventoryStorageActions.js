import axios from 'axios'
import queryString from 'query-string'
import {
  CLAIM_INVENTORY_STORAGE_FAIL,
  CLAIM_INVENTORY_STORAGE_REQUEST,
  CLAIM_INVENTORY_STORAGE_SUCCESS,
  GET_INVENTORY_STORAGE_LIST_FAIL,
  GET_INVENTORY_STORAGE_LIST_REQUEST,
  GET_INVENTORY_STORAGE_LIST_SUCCESS,
  INVENTORY_STORAGE_LIST_SET_PAGINATION,
  RESOLVE_INVENTORY_STORAGE_FAIL,
  RESOLVE_INVENTORY_STORAGE_REQUEST,
  RESOLVE_INVENTORY_STORAGE_SUCCESS,
} from '../types/inventoryStorageTypes'

export const getInventoryStorageList = () => async (dispatch, getState) => {
  const { filter } = getState().inventoryStorageList
  const { page, perPage } = getState().inventoryStorageListPagination
  const apiQuery = { page, limit: perPage, status: filter.status }
  const formattedApiQuery = queryString.stringify(apiQuery, { skipEmptyString: true })
  try {
    dispatch({ type: GET_INVENTORY_STORAGE_LIST_REQUEST })
    const response = await axios.get(`/inventory-storages?${formattedApiQuery}`)

    const { data, ...pagination } = response.data

    dispatch({ type: INVENTORY_STORAGE_LIST_SET_PAGINATION, payload: pagination })
    dispatch({ type: GET_INVENTORY_STORAGE_LIST_SUCCESS, payload: data })
  } catch (error) {
    dispatch({
      type: GET_INVENTORY_STORAGE_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const resolveStorageRequest = ({
  id,
  request_box_no,
  request_description,
  request_storekeeper_id,
}) => async (dispatch) => {
  try {
    dispatch({ type: RESOLVE_INVENTORY_STORAGE_REQUEST })
    const body = { request_box_no, request_description, request_storekeeper_id }
    const { data } = await axios.post(`/inventory-storages/${id}/resolve`, body)
    dispatch({
      type: RESOLVE_INVENTORY_STORAGE_SUCCESS,
      payload: data,
    })
    dispatch(getInventoryStorageList())
  } catch (error) {
    dispatch({
      type: RESOLVE_INVENTORY_STORAGE_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const claimStorageRequest = ({
  id,
  claim_box_no,
  claim_description,
  claim_claimer_name,
}) => async (dispatch) => {
  try {
    dispatch({ type: CLAIM_INVENTORY_STORAGE_REQUEST })
    const body = { claim_box_no, claim_description, claim_claimer_name }
    const { data } = await axios.post(`/inventory-storages/${id}/claim`, body)
    dispatch({
      type: CLAIM_INVENTORY_STORAGE_SUCCESS,
      payload: data,
    })
    dispatch(getInventoryStorageList())
  } catch (error) {
    dispatch({
      type: CLAIM_INVENTORY_STORAGE_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}
