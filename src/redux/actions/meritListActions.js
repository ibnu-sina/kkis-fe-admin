import axios from 'axios'
import queryString from 'query-string'
import {
  GET_MERIT_LIST_FAIL,
  GET_MERIT_LIST_REQUEST,
  GET_MERIT_LIST_SUCCESS,
  TOGGLE_MERIT_APPROVAL_REQUEST,
  TOGGLE_MERIT_APPROVAL_SUCCESS,
  TOGGLE_MERIT_APPROVAL_FAIL,
  MERIT_LIST_SET_PAGINATION,
} from '../types/meritListTypes'

export const getMeritList = (args = {}) => async (dispatch, getState) => {
  const { status } = args

  const { page, perPage } = getState().meritListPagination
  const searchQuery = getState().meritListSearchQuery
  const apiQuery = { page, limit: perPage, status, q: searchQuery }

  const formattedApiQuery = queryString.stringify(apiQuery, { skipEmptyString: true })

  try {
    dispatch({ type: GET_MERIT_LIST_REQUEST })
    const response = await axios.get(`/merits?${formattedApiQuery}`)

    const { data, ...pagination } = response.data
    dispatch({ type: MERIT_LIST_SET_PAGINATION, payload: pagination })
    dispatch({ type: GET_MERIT_LIST_SUCCESS, payload: data })
  } catch (error) {
    dispatch({
      type: GET_MERIT_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const toggleMeritApproval = (id) => async (dispatch) => {
  try {
    dispatch({ type: TOGGLE_MERIT_APPROVAL_REQUEST })
    const { data } = await axios.put(`/merits/${id}/approve`)
    dispatch({
      type: TOGGLE_MERIT_APPROVAL_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: TOGGLE_MERIT_APPROVAL_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}
