import axios from 'axios'
import queryString from 'query-string'
import {
  ADMIN_LIST_SET_PAGINATION,
  GET_ADMIN_LIST_FAIL,
  GET_ADMIN_LIST_REQUEST,
  GET_ADMIN_LIST_SUCCESS,
} from '../types/adminListTypes'

export const getAdminList = () => async (dispatch, getState) => {
  const { page, perPage } = getState().adminListPagination
  const searchQuery = getState().adminListSearchQuery
  const apiQuery = { page, limit: perPage, q: searchQuery }
  const formattedApiQuery = queryString.stringify(apiQuery, { skipEmptyString: true })
  try {
    dispatch({ type: GET_ADMIN_LIST_REQUEST })
    const response = await axios.get(`/admins?${formattedApiQuery}`)

    const { data, ...pagination } = response.data
    dispatch({ type: ADMIN_LIST_SET_PAGINATION, payload: pagination })
    dispatch({ type: GET_ADMIN_LIST_SUCCESS, payload: data })
  } catch (error) {
    dispatch({
      type: GET_ADMIN_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}
