import axios from 'axios'
import localstorage from 'store'
import {
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAIL,
  USER_REFRESH_TOKEN_REQUEST,
  USER_REFRESH_TOKEN_SUCCESS,
  USER_REFRESH_TOKEN_FAIL,
  USER_REGISTER_REQUEST,
  USER_REGISTER_SUCCESS,
  USER_REGISTER_FAIL,
  USER_LOGOUT_FAIL,
  USER_LOGOUT_REQUEST,
  USER_LOGOUT_SUCCESS,
  USER_UPDATE_PASSWORD_FAIL,
  USER_UPDATE_PASSWORD_REQUEST,
  USER_UPDATE_PASSWORD_SUCCESS,
  GET_MY_PROFILE_REQUEST,
  GET_MY_PROFILE_SUCCESS,
  GET_MY_PROFILE_FAIL,
} from '../types/userTypes'

export const login = (email, password) => async (dispatch) => {
  try {
    dispatch({ type: USER_LOGIN_REQUEST })
    const { data } = await axios.post('/auth/login', { email, password })
    dispatch({
      type: USER_LOGIN_SUCCESS,
      payload: data,
    })

    localstorage.set('rt', data.data.refreshToken)
  } catch (error) {
    dispatch({
      type: USER_LOGIN_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const logout = () => async (dispatch, getState) => {
  try {
    dispatch({ type: USER_LOGOUT_REQUEST })
    const { userLogin } = getState()
    await axios.post('/auth/logout', { refreshToken: userLogin.refreshToken })
    dispatch({ type: USER_LOGOUT_SUCCESS })

    localstorage.remove('rt')
  } catch (error) {
    dispatch({
      type: USER_LOGOUT_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const attemptRefreshToken = (refreshToken) => async (dispatch) => {
  try {
    dispatch({ type: USER_REFRESH_TOKEN_REQUEST })

    const { data } = await axios.post('/auth/refresh-token', { refreshToken })

    dispatch({
      type: USER_REFRESH_TOKEN_SUCCESS,
      payload: data,
    })

    localstorage.set('rt', data.data.refreshToken)
  } catch (error) {
    dispatch({
      type: USER_REFRESH_TOKEN_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const registerAdmin = (name, password, confirm_password, token) => async (dispatch) => {
  try {
    dispatch({ type: USER_REGISTER_REQUEST })
    const { data } = await axios.post('/admins', { name, password, confirm_password, token })
    dispatch({
      type: USER_REGISTER_SUCCESS,
      payload: data,
    })

    localstorage.set('rt', data.data.refreshToken)
  } catch (error) {
    dispatch({
      type: USER_REGISTER_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const updatePasswordByToken = (password, confirm_password, token) => async (dispatch) => {
  try {
    dispatch({ type: USER_UPDATE_PASSWORD_REQUEST })
    const { data } = await axios.post('/auth/update-password', {
      password,
      confirm_password,
      token,
    })
    dispatch({
      type: USER_UPDATE_PASSWORD_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: USER_UPDATE_PASSWORD_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}

export const getUserProfile = () => async (dispatch) => {
  try {
    dispatch({ type: GET_MY_PROFILE_REQUEST })
    const { data } = await axios.get('/auth/me')
    dispatch({
      type: GET_MY_PROFILE_SUCCESS,
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: GET_MY_PROFILE_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}
