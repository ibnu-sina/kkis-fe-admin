import React, { useEffect } from 'react'
import moment from 'moment'

import { NavLink as RouterLink } from 'react-router-dom'
import PerfectScrollbar from 'react-perfect-scrollbar'
import Box from '@material-ui/core/Box'
import Card from '@material-ui/core/Card'
import IconButton from '@material-ui/core/IconButton'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Link from '@material-ui/core/Link'
import StatusBadge from 'components/StatusBadge'
import { ArrowRight } from 'react-feather'

import FilterDropdown from './components/FilterDropdown'
import { useDispatch, useSelector } from 'react-redux'
import { getStayRequestList } from 'redux/actions/stayRequestListActions'
import useReduxPagination from 'hooks/useReduxPagination'

function RequestTable() {
  const dispatch = useDispatch()
  const { stayRequestList } = useSelector((state) => state.stayRequestList)
  const { page, perPage } = useSelector((state) => state.stayRequestListPagination)

  const { Pagination } = useReduxPagination('stayRequestList')

  useEffect(() => {
    dispatch(getStayRequestList())
  }, [dispatch, page, perPage])

  const handleStatus = (status) => {
    let severity

    if (status === 'rejected') severity = 'error'
    if (status === 'pending') severity = 'warning'
    if (status === 'approved') severity = 'primary'
    if (status === 'paid') severity = 'success'

    return <StatusBadge severity={severity} text={status} />
  }

  const handleStudentStatus = (checkin, checkout) => {
    if (checkout) return <StatusBadge severity='error' text='Checked Out' />
    if (checkin) return <StatusBadge severity='success' text='Checked In' />
    return 'N/A'
  }

  const showPaymentMenthod = (payment_method) => {
    if (payment_method === 1) return 'Cash'
    if (payment_method === 2) return 'Potongan Yuran'
  }

  const filterByStatus = (status) => {
    dispatch(getStayRequestList({ status }))
  }

  return (
    <Card>
      <FilterDropdown filterByStatus={filterByStatus} />
      <PerfectScrollbar>
        <Box minWidth={1050}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Student Name</TableCell>
                <TableCell>Start Date</TableCell>
                <TableCell>End Date</TableCell>
                <TableCell>Reason</TableCell>
                <TableCell>Payment Method</TableCell>
                <TableCell>Status</TableCell>
                <TableCell>Student Status</TableCell>
                <TableCell>Total Day(s)</TableCell>
                <TableCell>Action</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {stayRequestList.map((i) => (
                <TableRow hover key={i.id}>
                  <TableCell>
                    <Link component={RouterLink} to={`/dashboard/students/${i.student_id}`}>
                      {i.student.name}
                    </Link>
                  </TableCell>
                  <TableCell>{moment(i.start_date).format('DD/MM/YYYY')}</TableCell>
                  <TableCell>{moment(i.end_date).format('DD/MM/YYYY')}</TableCell>
                  <TableCell>{i.reason}</TableCell>
                  <TableCell>
                    <StatusBadge severity='grey' text={showPaymentMenthod(i.payment_method)} />
                  </TableCell>
                  <TableCell>{handleStatus(i.status)}</TableCell>
                  <TableCell>{handleStudentStatus(i.checkin_date, i.checkout_date)}</TableCell>
                  <TableCell>{moment(i.end_date).diff(moment(i.start_date), 'days') + 1}</TableCell>
                  <TableCell>
                    <IconButton
                      component={RouterLink}
                      to={`/dashboard/forms/semester-break/${i.id}`}
                    >
                      <ArrowRight size='20' />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <Pagination />
    </Card>
  )
}

export default RequestTable
