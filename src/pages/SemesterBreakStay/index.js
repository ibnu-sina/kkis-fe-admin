import React from 'react'

import { Box, Container, makeStyles, Typography } from '@material-ui/core'
import RequestTable from './RequestTable'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100vh',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}))

const SemesterBreakStay = () => {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <Container maxWidth='xl'>
        <Typography variant='h2' component='h2'>
          Semester break stay
        </Typography>
        <Box mt={3}>
          <RequestTable />
        </Box>
      </Container>
    </div>
  )
}

export default SemesterBreakStay
