import React, { useState } from 'react'
import moment from 'moment'
import { updateStatus } from 'redux/actions/stayRequestListActions'
import { useDispatch } from 'react-redux'
import { useRouteMatch } from 'react-router-dom'

import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableRow from '@material-ui/core/TableRow'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core'
import { Check, X, DollarSign } from 'react-feather'

import StatusBadge from 'components/StatusBadge'
import ApproveDialog from './ApproveDialog'

function StayInfoCard({ data }) {
  const useStyles = makeStyles((theme) => ({
    text: { color: theme.palette.text.secondary },
    tableCell: { padding: theme.spacing(1), paddingLeft: theme.spacing(1.5) },
    line: {
      border: 'none',
      height: '1px',
      margin: 0,
      backgroundColor: '#000',
      opacity: 0.12,
    },
  }))

  const dispatch = useDispatch()
  const match = useRouteMatch()
  const classes = useStyles()

  const id = match.params.id
  const [approveModal, setApproveModal] = useState(false)

  const getStatus = (status) => {
    let severity

    if (status === 'rejected') severity = 'error'
    if (status === 'pending') severity = 'warning'
    if (status === 'approved') severity = 'primary'
    if (status === 'paid') severity = 'success'

    return <StatusBadge severity={severity} text={status} />
  }

  const getPaymentMethod = (payment_method) => {
    if (payment_method === 1) return 'Cash'
    if (payment_method === 2) return 'Potongan Yuran'
  }

  const getTotalPrice = (days, price) => {
    return !price ? 'N/A' : `${days} day(s) X RM${price.toFixed(2)} = RM${(days * price).toFixed(2)}`
  }

  const showActionButton = (status) => {
    if (status === 'pending') {
      return (
        <TableRow>
          <TableCell className={classes.tableCell} colSpan={2}>
            <Button
              color='primary'
              startIcon={<Check size='20' />}
              onClick={() => setApproveModal(true)}
            >
              Approve Request
            </Button>
            <br />
            <Button
              color='secondary'
              startIcon={<X size='20' />}
              onClick={() => handleUpdateStatus('rejected')}
            >
              Reject Request
            </Button>
          </TableCell>
        </TableRow>
      )
    }

    if (status === 'approved') {
      return (
        <TableRow>
          <TableCell className={classes.tableCell} colSpan={2}>
            <Button
              color='primary'
              startIcon={<DollarSign size='20' />}
              onClick={() => handleUpdateStatus('paid')}
            >
              Mark as paid
            </Button>
          </TableCell>
        </TableRow>
      )
    }
  }

  const handleUpdateStatus = (action, data = {}) => {
    dispatch(updateStatus(id, action, data))
  }

  const onSubmit = (data) => {
    setApproveModal(false)
    handleUpdateStatus('approved', data)
  }

  return (
    <Card>
      <CardHeader title='Stay Info' />
      <hr className={classes.line} />
      <ApproveDialog
        open={approveModal}
        onSubmit={onSubmit}
        onClose={() => setApproveModal(false)}
      />
      <Table>
        <TableBody>
          <TableRow>
            <TableCell width='40%'>
              <Typography variant='h6'>Status</Typography>
            </TableCell>
            <TableCell>{getStatus(data.status)}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography variant='h6'>Payment Method</Typography>
            </TableCell>
            <TableCell>
              <StatusBadge severity='success' text={getPaymentMethod(data.payment_method)} />
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography variant='h6'>Reason</Typography>
            </TableCell>
            <TableCell className={classes.text}>{data.reason}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography variant='h6'>Start Date</Typography>
            </TableCell>
            <TableCell className={classes.text}>
              {moment(data.start_date).format('DD/MM/YYYY | ddd')}
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography variant='h6'>End Date</Typography>
            </TableCell>
            <TableCell className={classes.text}>
              {moment(data.end_date).format('DD/MM/YYYY | ddd')}
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography variant='h6'>Total Price</Typography>
            </TableCell>
            <TableCell className={classes.text}>
              {getTotalPrice(data.total_days, data.price_per_day)}
            </TableCell>
          </TableRow>
          {showActionButton(data.status)}
        </TableBody>
      </Table>
    </Card>
  )
}

export default StayInfoCard
