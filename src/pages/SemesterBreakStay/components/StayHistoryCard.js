import React, { useEffect, useState } from 'react'
import moment from 'moment'

import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableRow from '@material-ui/core/TableRow'
import { makeStyles } from '@material-ui/core'

import StatusBadge from 'components/StatusBadge'

const useStyle = makeStyles(() => ({
  line: {
    border: 'none',
    height: '1px',
    margin: 0,
    backgroundColor: '#000',
    opacity: 0.12,
  },
}))

function StayHistoryCard({ data }) {
  const classes = useStyle()
  const [history, setHistory] = useState([])

  useEffect(() => {
    const history = [
      {
        action: 'created',
        date: data.created_at,
      },
      {
        action: data.status === 'rejected' ? 'rejected' : 'approved',
        date: data.respond_date,
        admin: data.respond_admin,
      },
      {
        action: 'paid',
        date: data.paid_date,
        admin: data.paid_admin,
      },
      {
        action: 'checkin',
        date: data.checkin_date,
      },
      {
        action: 'checkout',
        date: data.checkout_date,
      },
    ]

    const withDates = history.filter((i) => i.date)
    const sortedByDates = withDates.sort((a, b) => new Date(a.date) - new Date(b.date))
    setHistory(sortedByDates)
  }, [data])

  const getStatusBadge = (status) => {
    if (status === 'approved') return <StatusBadge severity='primary' text='approved' />
    if (status === 'rejected') return <StatusBadge severity='error' text='rejected' />
    if (status === 'checkin') return <StatusBadge severity='success' text='checked in' />
    if (status === 'checkout') return <StatusBadge severity='error' text='checked out' />
    if (status === 'created') return <StatusBadge severity='warning' text='created' />
    if (status === 'paid') return <StatusBadge severity='success' text='paid' />
  }

  return (
    <Card>
      <CardHeader title='Stay History' />
      <hr className={classes.line} />
      <Table>
        <TableBody>
          {history.map((i, idx) => {
            return (
              <TableRow key={idx}>
                <TableCell>{moment(i.date).format('DD/MM/YYYY | HH:mm')}</TableCell>
                <TableCell colSpan={!i.admin ? 2 : 'false'}>{getStatusBadge(i.action)}</TableCell>
                {i.admin && <TableCell>{i.admin.name}</TableCell>}
              </TableRow>
            )
          })}
        </TableBody>
      </Table>
    </Card>
  )
}

export default StayHistoryCard
