import React from 'react'

import { Link } from 'react-router-dom'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableRow from '@material-ui/core/TableRow'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core'

function StudentInfoCard({ data }) {
  const useStyles = makeStyles((theme) => ({
    text: { color: theme.palette.text.secondary },
    tableCell: { padding: theme.spacing(1) },
    line: {
      border: 'none',
      height: '1px',
      margin: 0,
      backgroundColor: '#000',
      opacity: 0.12,
    },
  }))
  const classes = useStyles()

  return (
    <Card>
      <CardHeader title='Student Info' />
      <hr className={classes.line} />
      <Table>
        <TableBody>
          <TableRow>
            <TableCell width='40%'>
              <Typography variant='h6'>Name</Typography>
            </TableCell>
            <TableCell className={classes.text}>{data.student.name}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography variant='h6'>Matric Number</Typography>
            </TableCell>
            <TableCell className={classes.text}>{data.student.matric_no}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography variant='h6'>Room</Typography>
            </TableCell>
            <TableCell className={classes.text}>{data.student.room.name}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography variant='h6'>Course</Typography>
            </TableCell>
            <TableCell className={classes.text}>{data.student.course.name}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.tableCell} colSpan={2}>
              <Box display='flex' justifyContent='flex-end'>
                <Button fullWidth component={Link} to={`/dashboard/students/${data.student.id}`}>
                  View student
                </Button>
              </Box>
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </Card>
  )
}

export default StudentInfoCard
