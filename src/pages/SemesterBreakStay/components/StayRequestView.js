import React, { useEffect, useState } from 'react'
import { getStayRequest } from 'redux/actions/stayRequestListActions'
import { useDispatch, useSelector } from 'react-redux'

import { Box, Button, Container, Grid, makeStyles, Typography } from '@material-ui/core'

import StudentInfoCard from './StudentInfoCard'
import StayHistoryCard from './StayHistoryCard'
import StayInfoCard from './StayInfoCard'
import Loader from 'components/Loader'
import CustomSnackbar from 'components/CustomSnackbar'
import SectionList from 'components/SectionList'
import { ChevronLeft } from 'react-feather'
import { Link } from 'react-router-dom'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100vh',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}))

const listItem = [
  { section: 'stay-info', label: 'Stay Info' },
  { section: 'stay-history', label: 'Stay History' },
  { section: 'student-info', label: 'Student Info' },
]

function StayRequestView({ match }) {
  const classes = useStyles()
  const dispatch = useDispatch()
  const [section, setSection] = useState('stay-info')

  const { errorMsg, status, stayRequest } = useSelector((state) => state.stayRequest)
  useEffect(() => {
    dispatch(getStayRequest(Number(match.params.id)))
  }, [dispatch, match])

  return (
    <div className={classes.root}>
      <Container maxWidth='lg'>
        <Button
          color='primary'
          startIcon={<ChevronLeft />}
          component={Link}
          to='/dashboard/forms/semester-break'
        >
          Back to request list
        </Button>
        <Box mt={3}>
          <Typography variant='h2' component='h2'>
            Stay Details
          </Typography>
        </Box>
        {errorMsg && (
          <CustomSnackbar vertical='top' horizontal='center' severity='error' variant='filled'>
            {errorMsg}
          </CustomSnackbar>
        )}
        {status === 'loading' && <Loader />}
        {status === 'loaded' && (
          <Box mt={3}>
            <Grid container spacing={3}>
              <Grid item xs={12} md={4} lg={3}>
                <SectionList section={section} setSection={setSection} listItem={listItem} />
              </Grid>
              <Grid item xs={12} md={8} lg={6}>
                {section === 'student-info' && <StudentInfoCard data={stayRequest} />}
                {section === 'stay-info' && <StayInfoCard data={stayRequest} />}
                {section === 'stay-history' && <StayHistoryCard data={stayRequest} />}
              </Grid>
            </Grid>
          </Box>
        )}
      </Container>
    </div>
  )
}

export default StayRequestView
