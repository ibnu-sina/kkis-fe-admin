import React, { useState } from 'react'

import Box from '@material-ui/core/Box'
import CardContent from '@material-ui/core/CardContent'
import TextField from '@material-ui/core/TextField'
import { makeStyles } from '@material-ui/core'

const useStyles = makeStyles({
  root: {
    width: 200,
  },
})

const FilterDropdown = ({ filterByStatus }) => {
  const classes = useStyles()
  const [value, setValue] = useState('')

  const handleChange = (e) => {
    setValue(e.target.value)
    filterByStatus(e.target.value)
  }
  return (
    <CardContent>
      <Box display='flex'>
        <TextField
          select
          label='Status'
          onChange={handleChange}
          value={value}
          SelectProps={{ native: true }}
          variant='outlined'
          className={classes.root}
          InputLabelProps={{ shrink: true }}
        >
          <option value=''>All</option>
          <option value='pending'>Pending</option>
          <option value='approved'>Approved</option>
          <option value='rejected'>Rejected</option>
          <option value='paid'>Paid</option>
        </TextField>
      </Box>
    </CardContent>
  )
}

export default FilterDropdown
