import React from 'react'
import { useForm } from 'react-hook-form'

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  TextField,
  Typography,
} from '@material-ui/core'

function ApproveDialog({ open, onClose, onSubmit, ...rest }) {
  const { handleSubmit, register, errors } = useForm()
  return (
    <Dialog open={open} onClose={onClose} maxWidth='xs' fullWidth {...rest}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <DialogTitle disableTypography>
          <Typography variant='h5' component='h5'>
            Please provide Price Per Day
          </Typography>
        </DialogTitle>
        <DialogContent>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <TextField
                fullWidth
                type='number'
                inputProps={{ min: 0, step: '.01' }}
                label='Price Per Day (RM)'
                name='price_per_day'
                variant='outlined'
                inputRef={register({ required: true })}
                error={!!errors.price_per_day}
                helperText={!!errors.price_per_day && 'Required field'}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button color='primary' onClick={onClose}>
            Cancel
          </Button>
          <Button type='submit' color='primary' variant='contained'>
            Approve
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  )
}

export default ApproveDialog
