import React from 'react'
import moment from 'moment'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { resolveActivity } from 'redux/actions/activityListActions'

import { makeStyles } from '@material-ui/core'
import PerfectScrollbar from 'react-perfect-scrollbar'
import Box from '@material-ui/core/Box'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TextLink from '@material-ui/core/Link'
import IconButton from '@material-ui/core/IconButton'
import { X, Check } from 'react-feather'

import StatusBadge from 'components/StatusBadge'

const useStyles = makeStyles((theme) => ({
  reject: { color: theme.palette.error.main },
  approve: { color: theme.palette.success.main },
}))

function ActivityTable({ status, Pagination }) {
  const { activityList } = useSelector((state) => state.activityList)
  const severityStatus = (status) => {
    switch (status.toLowerCase()) {
      case 'approved':
        return 'success'
      case 'rejected':
        return 'error'
      case 'pending':
        return 'warning'
      default:
        return 'info'
    }
  }

  const dispatch = useDispatch()

  const setActivityStatus = (id, action) => {
    dispatch(resolveActivity(id, action))
  }

  const classes = useStyles()

  return (
    <div role='tabpanel'>
      <PerfectScrollbar>
        <Box minWidth={1050}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Room</TableCell>
                <TableCell>Matric No.</TableCell>
                <TableCell>Student Name</TableCell>
                <TableCell>Course</TableCell>
                <TableCell>Request Date</TableCell>
                {status !== 'pending' && <TableCell>Response Date</TableCell>}
                {status !== 'pending' && <TableCell>Response By</TableCell>}
                <TableCell>Status</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {activityList.map((item) => (
                <TableRow hover key={item.id}>
                  <TableCell>{item.student.room.name}</TableCell>
                  <TableCell>{item.student.matric_no}</TableCell>
                  <TableCell>
                    <TextLink component={Link} to={`/dashboard/students/${item.student.id}`}>
                      {item.student.name}
                    </TextLink>
                  </TableCell>
                  <TableCell>
                    <TextLink component={Link} to={`/dashboard/courses/${item.student.course.id}`}>
                      {item.student.course.name}
                    </TextLink>
                  </TableCell>

                  <TableCell>
                    {item.created_at && moment(item.created_at).format('DD/MM/YYYY')}
                  </TableCell>
                  {status !== 'pending' && (
                    <TableCell>
                      {item.updated_at && moment(item.updated_at).format('DD/MM/YYYY')}
                    </TableCell>
                  )}
                  {status !== 'pending' && item.admin && (
                    <TableCell>
                      <TextLink component={Link} to={`/dashboard/admins/${item.admin.id}`}>
                        {item.admin.name}
                      </TextLink>
                    </TableCell>
                  )}
                  {item.status.toLowerCase() === 'pending' ? (
                    <TableCell>
                      <IconButton
                        className={classes.reject}
                        onClick={() => setActivityStatus(item.id, 'rejected')}
                      >
                        <X size='20' />
                      </IconButton>
                      <IconButton
                        className={classes.approve}
                        onClick={() => setActivityStatus(item.id, 'approved')}
                      >
                        <Check size='20' />
                      </IconButton>
                    </TableCell>
                  ) : (
                    <TableCell>
                      <StatusBadge severity={severityStatus(item.status)} text={item.status} />
                    </TableCell>
                  )}
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <Pagination />
    </div>
  )
}

export default ActivityTable
