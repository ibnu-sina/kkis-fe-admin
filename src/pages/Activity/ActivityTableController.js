import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getActivityList } from 'redux/actions/activityListActions'

import ActivityTable from './ActivityTable'
import CustomSnackbar from 'components/CustomSnackbar'
import Loader from 'components/Loader'
import { Card, Tab, Tabs } from '@material-ui/core'
import useReduxPagination from 'hooks/useReduxPagination'

function ActivityTableController({ action }) {
  const dispatch = useDispatch()
  const [status, setStatus] = useState('pending')

  const { Pagination } = useReduxPagination('activityList')
  const { page, perPage } = useSelector((state) => state.activityListPagination)
  const { successMsg, status: fetchStatus } = useSelector((state) => state.activityList)

  const handleChange = (event, newStatus) => {
    setStatus(newStatus)
  }

  useEffect(() => {
    dispatch(getActivityList({ action, status }))
  }, [dispatch, action, page, perPage, status])

  return (
    <Card>
      <Tabs value={status} onChange={handleChange} textColor='secondary'>
        <Tab label='Pending' value='pending' />
        <Tab label='Approved' value='approved' />
        <Tab label='Rejected' value='rejected' />
        {fetchStatus === 'updated' && (
          <CustomSnackbar variant='filled' severity='success' vertical='top' horizontal='center'>
            {successMsg}
          </CustomSnackbar>
        )}
      </Tabs>
      {fetchStatus === 'loading' && <Loader />}
      {fetchStatus !== 'loading' && <ActivityTable status={status} Pagination={Pagination} />}
    </Card>
  )
}

export default ActivityTableController
