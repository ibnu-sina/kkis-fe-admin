import React from 'react'
import { Box, Container, makeStyles, Typography } from '@material-ui/core'

import ActivityTableController from './ActivityTableController'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100vh',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}))

const CheckinView = ({ match }) => {
  const classes = useStyles()

  const action = match.params.action

  return (
    <div className={classes.root}>
      <Container maxWidth='xl'>
        <Typography variant='h2' component='h2'>
          Student check {action === 'checkin' ? 'in' : 'out'}
        </Typography>
        <Box mt={3}>
          <ActivityTableController action={action} />
        </Box>
      </Container>
    </div>
  )
}

export default CheckinView
