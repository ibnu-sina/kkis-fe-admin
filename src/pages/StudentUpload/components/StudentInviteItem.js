import React, { useState } from 'react'
import { Check, Trash } from 'react-feather'
import { IconButton, TableCell, TableRow, TextField } from '@material-ui/core'
import { removeInviteListItem, updateInviteListItem } from 'redux/actions/studentInviteListActions'
import { useDispatch } from 'react-redux'

function StudentInviteItem({ student }) {
  const getError = (field) => student.errors?.find((i) => i.field === field)

  const dispatch = useDispatch()

  const [roomError, setRoomError] = useState(getError('room'))
  const [emailError, setEmailError] = useState(getError('email'))
  const [matricNoError, setMatricNoError] = useState(getError('matric_no'))
  const [courseCodeError, setCourseCodeError] = useState(getError('course_code'))

  const [room, setRoom] = useState(student.room)
  const [email, setEmail] = useState(student.email)
  const [matricNo, setMatricNo] = useState(student.matric_no)
  const [courseCode, setCourseCode] = useState(student.course_code)

  const handleUpdate = () => {
    // Construct new student info
    const newStudentInfo = {
      ...student,
      room,
      email,
      matric_no: matricNo,
      course_code: courseCode,
      errors: null,
    }

    // Update student invite list state
    dispatch(updateInviteListItem(student.id, newStudentInfo))

    // Reset errors
    setRoomError(false)
    setEmailError(false)
    setMatricNoError(false)
    setCourseCodeError(false)
  }

  return (
    <TableRow hover>
      <TableCell>
        {roomError ? (
          <TextField
            error
            helperText={roomError.message}
            value={room}
            onChange={(e) => setRoom(e.target.value)}
          />
        ) : (
          room
        )}
      </TableCell>
      <TableCell>
        {matricNoError ? (
          <TextField
            error
            helperText={matricNoError.message}
            value={matricNo}
            onChange={(e) => setMatricNo(e.target.value)}
          />
        ) : (
          matricNo
        )}
      </TableCell>
      <TableCell>{student.name}</TableCell>
      <TableCell>
        {emailError ? (
          <TextField
            error
            helperText={emailError.message}
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        ) : (
          email
        )}
      </TableCell>
      <TableCell>
        {courseCodeError ? (
          <TextField
            error
            helperText={courseCodeError.message}
            value={courseCode}
            onChange={(e) => setCourseCode(e.target.value)}
          />
        ) : (
          courseCode
        )}
      </TableCell>
      <TableCell align='center'>
        {student.errors && (
          <IconButton onClick={handleUpdate}>
            <Check size='20' />
          </IconButton>
        )}
        <IconButton onClick={() => dispatch(removeInviteListItem(student.id))}>
          <Trash size='20' />
        </IconButton>
      </TableCell>
    </TableRow>
  )
}

export default StudentInviteItem
