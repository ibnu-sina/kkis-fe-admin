import React from 'react'
import PerfectScrollbar from 'react-perfect-scrollbar'
import { Box, Card, Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core'
import { useSelector } from 'react-redux'
import StudentInviteItem from './components/StudentInviteItem'

const ReviewPage = () => {
  const { studentInviteList: students } = useSelector((state) => state.uploadStudentInviteList)
  return (
    <Card>
      <PerfectScrollbar>
        <Box minWidth={1050}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell width='15%'>Room</TableCell>
                <TableCell width='15%'>Matric Number</TableCell>
                <TableCell width='15%'>Name</TableCell>
                <TableCell>Email</TableCell>
                <TableCell>Course Code</TableCell>
                <TableCell width='10%' align='center'>
                  Update/Remove
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {students.map((student) => (
                <StudentInviteItem key={student.id} student={student} />
              ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
    </Card>
  )
}

export default ReviewPage
