import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { inviteMultipleStudents } from 'redux/actions/studentInviteListActions'
import { Box, Button, Container, makeStyles, Typography } from '@material-ui/core'
import CustomSnackbar from 'components/CustomSnackbar'

import ReviewPage from './ReviewPage'
import Loader from 'components/Loader'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100vh',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}))

function StudentUpload({ history }) {
  const classes = useStyles()
  const dispatch = useDispatch()

  const { errorMsg, status, studentInviteList } = useSelector(
    (state) => state.uploadStudentInviteList
  )

  useEffect(() => {
    if (status === 'idle' || status === 'created' || studentInviteList.length === 0)
      history.push('/dashboard/students')
  }, [dispatch, status, history, studentInviteList])

  const onClick = () => {
    dispatch(inviteMultipleStudents(studentInviteList))
  }

  return (
    <div className={classes.root}>
      <Container>
        {status === 'loading' && <Loader />}
        {status === 'error' && (
          <CustomSnackbar vertical='top' horizontal='center' severity='error' variant='filled'>
            {errorMsg}
          </CustomSnackbar>
        )}
        {status !== 'loading' && (
          <>
            <Typography variant='h2' component='h2'>
              Review Student Info
            </Typography>
            <Box my={3}>
              <ReviewPage />
            </Box>
            <Button variant='contained' color='primary' onClick={onClick}>
              Review and Submit
            </Button>
          </>
        )}
      </Container>
    </div>
  )
}

export default StudentUpload
