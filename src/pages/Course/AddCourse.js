import React, { useState, useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { useDispatch, useSelector } from 'react-redux'
import { addCourse } from 'redux/actions/courseListActions'

import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import { Typography } from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'

function AddCourse() {
  const [open, setOpen] = useState(false)
  const { register, handleSubmit, errors } = useForm()

  const dispatch = useDispatch()
  const { errorMsg, status } = useSelector((state) => state.courseList)

  useEffect(() => {
    if (status === 'created') setOpen(false)
  }, [status, setOpen])

  const onSubmit = ({ name, description, code }) => {
    dispatch(addCourse({ name, description, code }))
  }
  return (
    <>
      <Box display='flex' justifyContent='flex-end'>
        <Button color='primary' variant='contained' onClick={() => setOpen(true)}>
          Add course
        </Button>
      </Box>
      <Dialog open={open} onClose={() => setOpen(false)} maxWidth='xs'>
        <form onSubmit={handleSubmit(onSubmit)}>
          <DialogTitle disableTypography>
            <Typography variant='h3' component='h3'>
              Add course
            </Typography>
          </DialogTitle>
          <DialogContent>
            <Grid container spacing={1}>
              <Grid item xs={12}>
                {status === 'invite_error' && <Alert severity='error'>{errorMsg}</Alert>}
              </Grid>
              <Grid item xs={12}>
                <TextField
                  type='text'
                  fullWidth
                  label='Course name'
                  name='name'
                  variant='outlined'
                  inputRef={register({ required: true })}
                  error={!!errors.name}
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  type='text'
                  fullWidth
                  label='Course code'
                  name='code'
                  variant='outlined'
                  inputRef={register({ required: true })}
                  error={!!errors.code}
                  helperText='To be used as reference in multiple student invite'
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  type='text'
                  fullWidth
                  label='Course description'
                  name='description'
                  variant='outlined'
                  inputRef={register}
                  error={!!errors.description}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button type='submit' color='primary'>
              Add course
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </>
  )
}

export default AddCourse
