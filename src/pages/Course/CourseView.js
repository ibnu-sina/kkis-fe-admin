import React, { useEffect, useState } from 'react'
import { getCourse } from 'redux/actions/courseListActions'
import { useDispatch, useSelector } from 'react-redux'

import { makeStyles } from '@material-ui/core'
import Box from '@material-ui/core/Box'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Grid from '@material-ui/core/Grid'

import Loader from 'components/Loader'
import CustomSnackbar from 'components/CustomSnackbar'
import CourseDetail from './components/CourseDetail'
import CourseEdit from './components/CourseEdit'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100vh',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}))

function CourseView({ match }) {
  const classes = useStyles()
  const dispatch = useDispatch()
  const [mode, setMode] = useState('view')

  const { errorMsg, status, course } = useSelector((state) => state.course)
  const courseId = match.params.id
  useEffect(() => {
    dispatch(getCourse(courseId))
  }, [dispatch, courseId])

  return (
    <div className={classes.root}>
      <Container>
        {status === 'error' && (
          <CustomSnackbar vertical='top' horizontal='center' severity='error' variant='filled'>
            {errorMsg}
          </CustomSnackbar>
        )}
        {status === 'loading' && <Loader />}
        {(status === 'loaded' || status === 'updated') && (
          <Box mt={3}>
            <Grid container spacing={3}>
              <Grid item lg={4} md={6} xs={12}>
                <Card>
                  {mode === 'view' && (
                    <CourseDetail course={course} setMode={setMode} status={status} />
                  )}
                  {mode === 'edit' && (
                    <CourseEdit course={course} setMode={setMode} id={courseId} />
                  )}
                </Card>
              </Grid>
              <Grid item lg={8} md={6} xs={12}>
                <Card>
                  <CardContent>
                    <Typography variant='h5'>Registered students</Typography>
                    <Table>
                      <TableHead>
                        <TableRow>
                          <TableCell>Name</TableCell>
                          <TableCell>Email</TableCell>
                          <TableCell>Matric No.</TableCell>
                          <TableCell>Room</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {(status === 'loaded' || status === 'updated') &&
                          course.students.map((s) => (
                            <TableRow key={s.id}>
                              <TableCell>{s.name}</TableCell>
                              <TableCell>{s.email}</TableCell>
                              <TableCell>{s.matric_no}</TableCell>
                              <TableCell>{s.room}</TableCell>
                            </TableRow>
                          ))}
                      </TableBody>
                    </Table>
                  </CardContent>
                </Card>
              </Grid>
            </Grid>
          </Box>
        )}
      </Container>
    </div>
  )
}

export default CourseView
