import React from 'react'
import { Box, Container, makeStyles, Typography } from '@material-ui/core'

import ActionTabs from './ActionTabs'
import AddCourse from './AddCourse'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100vh',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}))

const CourseListView = () => {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <Container>
        <Typography variant='h2' component='h2'>
          Course management
        </Typography>
        <AddCourse />
        <Box mt={3}>
          <ActionTabs />
        </Box>
      </Container>
    </div>
  )
}

export default CourseListView
