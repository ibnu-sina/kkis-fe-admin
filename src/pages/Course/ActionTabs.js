import React, { useState } from 'react'

import Card from '@material-ui/core/Card'
import Tab from '@material-ui/core/Tab'
import Tabs from '@material-ui/core/Tabs'
import Toolbar from './components/Toolbar'
import ManageCourse from './ManageCourse'

function ActionTabs({ className, ...rest }) {
  const [value, setValue] = useState(0)

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  return (
    <Card className={className} {...rest}>
      <Tabs value={value} onChange={handleChange} textColor='secondary'>
        <Tab label='Manage Course' />
      </Tabs>
      <Toolbar />
      <ManageCourse role='tabpanel' hidden={value !== 0} />
    </Card>
  )
}

export default ActionTabs
