import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getCourseList, toggleCourse } from 'redux/actions/courseListActions'

import { NavLink as RouterLink } from 'react-router-dom'
import PerfectScrollbar from 'react-perfect-scrollbar'
import Loader from 'components/Loader'
import CustomSnackbar from 'components/CustomSnackbar'
import {
  Box,
  Link,
  Switch,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
} from '@material-ui/core'
import useReduxPagination from 'hooks/useReduxPagination'

function ManageCourse({ role, hidden }) {
  const dispatch = useDispatch()
  const { courseList, status, errorMsg } = useSelector((state) => state.courseList)
  const { page, perPage } = useSelector((state) => state.courseListPagination)
  const { Pagination } = useReduxPagination('courseList')

  useEffect(() => {
    dispatch(getCourseList())
  }, [dispatch, page, perPage])

  const handleChange = (action, id) => {
    dispatch(toggleCourse(action, id))
  }

  const showTable = ['loaded', 'created', 'updated', 'deleted'].some((i) => i === status)

  return (
    <div role={role} hidden={hidden}>
      <PerfectScrollbar>
        <Box minWidth={1050}>
          {status === 'loading' && <Loader />}
          {status === 'error' && <CustomSnackbar severity='error'>{errorMsg}</CustomSnackbar>}
          {showTable && (
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell>
                    <Tooltip title='To be used as reference in multiple student invite'>
                      <span>Code</span>
                    </Tooltip>
                  </TableCell>
                  <TableCell>Description</TableCell>
                  <TableCell>Check in</TableCell>
                  <TableCell>Check out</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {courseList.map((course) => (
                  <TableRow hover key={course.id}>
                    <TableCell>
                      <Link component={RouterLink} to={`/dashboard/courses/${course.id}`}>
                        {course.name}
                      </Link>
                    </TableCell>
                    <TableCell>{course.code}</TableCell>
                    <TableCell>{course.description}</TableCell>

                    <TableCell>
                      <Switch
                        checked={!!course.is_checkin}
                        name='checkin'
                        onChange={() => handleChange('checkin', course.id)}
                      />
                    </TableCell>
                    <TableCell>
                      <Switch
                        checked={!!course.is_checkout}
                        name='checkout'
                        onChange={() => handleChange('checkout', course.id)}
                      />
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          )}
        </Box>
      </PerfectScrollbar>
      <Pagination />
    </div>
  )
}

export default ManageCourse
