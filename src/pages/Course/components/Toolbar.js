import React from 'react'

import { Box, CardContent } from '@material-ui/core'
import { getCourseList } from 'redux/actions/courseListActions'
import useReduxSearch from 'hooks/useReduxSearch'

const Toolbar = () => {
  const { SearchField } = useReduxSearch('courseList', getCourseList)
  return (
    <CardContent>
      <Box maxWidth={500}>
        <SearchField />
      </Box>
    </CardContent>
  )
}

export default Toolbar
