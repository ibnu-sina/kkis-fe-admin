import React from 'react'
import { useDispatch } from 'react-redux'
import { deleteCourse } from 'redux/actions/courseListActions'
import { useHistory } from 'react-router-dom'

import Button from '@material-ui/core/Button'
import CardActions from '@material-ui/core/CardActions'
import Chip from '@material-ui/core/Chip'
import Divider from '@material-ui/core/Divider'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import { CardContent } from '@material-ui/core'

function CourseDetail({ course, setMode }) {
  const dispatch = useDispatch()
  const history = useHistory()
  const onClick = (id) => {
    dispatch(deleteCourse(id))
    history.push('/dashboard/courses')
  }
  return (
    <>
      <CardContent>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Typography gutterBottom variant='h2' component='h2'>
              {course.name}
            </Typography>
            <Typography gutterBottom variant='body1' component='p' color='textSecondary'>
              {course.description}
            </Typography>
          </Grid>
        </Grid>
      </CardContent>

      {Boolean(course.is_checkin || course.is_checkout) && (
        <>
          <Divider />
          <CardContent>
            <Grid container spacing={1}>
              {Boolean(course.is_checkin) && (
                <Grid item>
                  <Chip label='Check-in' color='secondary' />
                </Grid>
              )}
              {Boolean(course.is_checkout) && (
                <Grid item>
                  <Chip label='Check-out' color='secondary' />
                </Grid>
              )}
            </Grid>
          </CardContent>
        </>
      )}

      <Divider />
      <CardActions>
        <Button color='primary' fullWidth variant='text' onClick={() => setMode('edit')}>
          Edit course
        </Button>
      </CardActions>

      {course.students.length === 0 && (
        <>
          <Divider />
          <CardActions>
            <Button color='secondary' fullWidth variant='text' onClick={() => onClick(course.id)}>
              Delete course
            </Button>
          </CardActions>
        </>
      )}
    </>
  )
}

export default CourseDetail
