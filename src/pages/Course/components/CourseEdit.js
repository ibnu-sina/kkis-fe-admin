import React, { useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { useDispatch } from 'react-redux'
import { editCourse } from 'redux/actions/courseListActions'

import Button from '@material-ui/core/Button'
import CardActions from '@material-ui/core/CardActions'
import Chip from '@material-ui/core/Chip'
import Divider from '@material-ui/core/Divider'
import Grid from '@material-ui/core/Grid'
import { CardContent, TextField } from '@material-ui/core'

function CourseEdit({ course, setMode, id }) {
  const { register, handleSubmit, errors, setValue } = useForm()
  const dispatch = useDispatch()
  const onSubmit = (data) => {
    dispatch(editCourse(data, id))
    setMode('view')
  }

  useEffect(() => {
    const config = { shouldValidate: true, shouldDirty: true }
    setValue('name', course.name, config)
    setValue('code', course.code, config)
    setValue('description', course.description, config)
  }, [course, setValue])

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <CardContent>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <TextField
                fullWidth
                label='Course name'
                name='name'
                variant='outlined'
                inputRef={register({ required: true })}
                error={!!errors.name}
                helperText={!!errors.name && 'Course name is required'}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                fullWidth
                label='Course code'
                name='code'
                variant='outlined'
                inputRef={register({ required: true })}
                error={!!errors.code}
                helperText={!!errors.code && 'Course code is required'}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                fullWidth
                multiline
                rows={4}
                label='Course description'
                name='description'
                variant='outlined'
                inputRef={register}
              />
            </Grid>
          </Grid>
        </CardContent>

        <Divider />

        <CardContent>
          <Grid container spacing={1}>
            <Grid item>
              <Chip label='Check-in' color={course.is_checkin ? 'secondary' : 'default'} />
            </Grid>
            <Grid item>
              <Chip label='Check-out' color={course.is_checkout ? 'secondary' : 'default'} />
            </Grid>
          </Grid>
        </CardContent>

        <Divider />
        <CardActions>
          <Button color='primary' fullWidth variant='text' type='submit'>
            Update course
          </Button>
        </CardActions>
      </form>
    </>
  )
}

export default CourseEdit
