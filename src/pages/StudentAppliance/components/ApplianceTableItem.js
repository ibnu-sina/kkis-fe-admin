import React from 'react'
import { Button, IconButton, Link as TextLink, TableCell, TableRow } from '@material-ui/core'
import { Trash } from 'react-feather'
import { Link } from 'react-router-dom'
import useSerialNo from 'hooks/useSerialNo'
import { useDispatch } from 'react-redux'
import { deleteStudentAppliance } from 'redux/actions/studentApplianceActions'

function ApplianceTableItem({ item }) {
  const {
    serial_no,
    student_id,
    student_name,
    appliance_id,
    appliance_name,
    appliance_price,
  } = item

  const pageInfo = {
    from: 'student_appliance_page',
    student_id,
    appliance_id,
  }

  const dispatch = useDispatch()
  const { setDialogOpen, AddSerialNoDialog } = useSerialNo(pageInfo)
  const handleDelete = () => {
    dispatch(deleteStudentAppliance(pageInfo))
  }

  return (
    <TableRow hover>
      <AddSerialNoDialog />
      <TableCell>
        {serial_no || (
          <Button color='primary' onClick={() => setDialogOpen(true)}>
            Add Serial No.
          </Button>
        )}
      </TableCell>
      <TableCell>
        <TextLink component={Link} to={`/dashboard/students/${student_id}`}>
          {student_name}
        </TextLink>
      </TableCell>
      <TableCell>{appliance_name}</TableCell>
      <TableCell>RM{appliance_price.toFixed(2)}</TableCell>
      <TableCell>
        <IconButton onClick={handleDelete}>
          <Trash size='15' />
        </IconButton>
      </TableCell>
    </TableRow>
  )
}

export default ApplianceTableItem
