import React, { useEffect } from 'react'
import { getStudentApplianceList } from 'redux/actions/studentApplianceActions'
import { useDispatch, useSelector } from 'react-redux'

import { Box, Container, makeStyles, Typography } from '@material-ui/core'
import ApplianceTable from './ApplianceTable'
import Loader from 'components/Loader'
import CustomSnackbar from 'components/CustomSnackbar'
import { RESET_APPLIANCE } from 'redux/types/applianceListTypes'
import { RESET_STUDENT_APPLIANCE } from 'redux/types/studentApplianceTypes'
import useReduxPagination from 'hooks/useReduxPagination'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100vh',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}))

const StudentAppliance = () => {
  const classes = useStyles()
  const dispatch = useDispatch()
  const { Pagination } = useReduxPagination('studentApplianceList')
  const { page, perPage } = useSelector((state) => state.studentApplianceListPagination)
  const { status } = useSelector((state) => state.studentApplianceList)
  const { successMsg: applianceSuccess } = useSelector((state) => state.appliance)
  const { successMsg } = useSelector((state) => state.studentAppliance)

  useEffect(() => {
    dispatch(getStudentApplianceList())
    return () => {
      dispatch({ type: RESET_STUDENT_APPLIANCE })
      dispatch({ type: RESET_APPLIANCE })
    }
  }, [dispatch, page, perPage])

  return (
    <div className={classes.root}>
      <Container maxWidth='xl'>
        <Typography variant='h2' component='h2'>
          Registered appliances
        </Typography>
        {(successMsg || applianceSuccess) && (
          <CustomSnackbar horizontal='top' vertical='center' variant='filled' severity='success'>
            {successMsg || applianceSuccess}
          </CustomSnackbar>
        )}
        <Box mt={3}>
          {status === 'loading' && <Loader />}
          {status === 'loaded' && <ApplianceTable Pagination={Pagination} />}
        </Box>
      </Container>
    </div>
  )
}

export default StudentAppliance
