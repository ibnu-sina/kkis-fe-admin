import React from 'react'
import {
  Box,
  Card,
  CardContent,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from '@material-ui/core'
import PerfectScrollbar from 'react-perfect-scrollbar'
import { useSelector } from 'react-redux'
import ApplianceTableItem from './components/ApplianceTableItem'
import useReduxSearch from 'hooks/useReduxSearch'
import { getStudentApplianceList } from 'redux/actions/studentApplianceActions'

function ApplianceTable({ Pagination }) {
  const { SearchField } = useReduxSearch('studentApplianceList', getStudentApplianceList)
  const { applianceList } = useSelector((state) => state.studentApplianceList)
  return (
    <Card>
      <CardContent>
        <Grid container>
          <Grid item xs={12} md={6} lg={4}>
            <SearchField />
          </Grid>
        </Grid>
      </CardContent>
      <PerfectScrollbar>
        <Box minWidth={1050}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Serial No.</TableCell>
                <TableCell>Student Name</TableCell>
                <TableCell>Appliance Name</TableCell>
                <TableCell>Price</TableCell>
                <TableCell>Delete</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {applianceList.map((i) => (
                <ApplianceTableItem item={i} key={i.id} />
              ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <Pagination />
    </Card>
  )
}

export default ApplianceTable
