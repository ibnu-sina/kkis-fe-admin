import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getStudentList } from 'redux/actions/studentListActions'
import { CardContent, Grid, TextField } from '@material-ui/core'
import { getStudentInviteList } from 'redux/actions/studentInviteListActions'
import {
  UPDATE_STUDENT_ROOM_STATUS_FILTER,
  UPDATE_STUDENT_ROOM_FILTER,
  UPDATE_STUDENT_COURSE_FILTER,
} from 'redux/types/studentListTypes'
import { getRoomList } from 'redux/actions/roomActions'
import { getCourseList } from 'redux/actions/courseListActions'
import useReduxSearch from 'hooks/useReduxSearch'

const Toolbar = ({ tab }) => {
  const dispatch = useDispatch()
  const { SearchField: StudentListSearchField } = useReduxSearch('studentList', getStudentList)

  const studentInviteListHook = useReduxSearch('studentInviteList', getStudentInviteList)
  const StudentInviteListSearchField = studentInviteListHook.SearchField

  const { courseList } = useSelector((state) => state.courseList)
  const { roomList } = useSelector((state) => state.roomList)
  const { filter } = useSelector((state) => state.studentList)

  useEffect(() => {
    dispatch(getRoomList())
    dispatch(getCourseList())
  }, [dispatch])

  const handleFilterStatus = (e) => {
    const roomStatus = e.target.value
    dispatch({ type: UPDATE_STUDENT_ROOM_STATUS_FILTER, payload: roomStatus })
    dispatch(getStudentList())
  }

  const handleRoomFilter = (e) => {
    const roomId = e.target.value
    dispatch({ type: UPDATE_STUDENT_ROOM_FILTER, payload: roomId })

    if (tab === 0) dispatch(getStudentList())
    if (tab === 1) dispatch(getStudentInviteList())
  }

  const handleCourseFilter = (e) => {
    const courseId = e.target.value
    dispatch({ type: UPDATE_STUDENT_COURSE_FILTER, payload: courseId })
    if (tab === 0) dispatch(getStudentList())
    if (tab === 1) dispatch(getStudentInviteList())
  }

  return (
    <CardContent>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={4} xl={4}>
          {tab === 0 && <StudentListSearchField />}
          {tab === 1 && <StudentInviteListSearchField />}
        </Grid>
        <Grid item xs={12} sm={4} lg={2} xl={1}>
          <TextField
            select
            fullWidth
            label='Filter Room'
            value={filter.roomId}
            onChange={handleRoomFilter}
            SelectProps={{ native: true }}
            variant='outlined'
            InputLabelProps={{ shrink: true }}
          >
            <option value=''>All</option>
            {roomList.map((room) => (
              <option key={room.id} value={room.id}>
                {room.name}
              </option>
            ))}
          </TextField>
        </Grid>
        <Grid item xs={12} sm={4} lg={2} xl={1}>
          <TextField
            select
            fullWidth
            label='Filter Course'
            value={filter.courseId}
            onChange={handleCourseFilter}
            SelectProps={{ native: true }}
            variant='outlined'
            InputLabelProps={{ shrink: true }}
          >
            <option value=''>All</option>
            {courseList.map((course) => (
              <option key={course.id} value={course.id}>
                {course.name}
              </option>
            ))}
          </TextField>
        </Grid>
        {tab === 0 && (
          <Grid item xs={12} sm={4} lg={2} xl={1}>
            <TextField
              select
              fullWidth
              label='Filter Status'
              value={filter.roomStatus}
              onChange={handleFilterStatus}
              SelectProps={{ native: true }}
              variant='outlined'
              InputLabelProps={{ shrink: true }}
            >
              <option value=''>All</option>
              <option value='checked_in'>Checked In</option>
              <option value='checked_out'>Checked Out</option>
            </TextField>
          </Grid>
        )}
      </Grid>
    </CardContent>
  )
}

export default Toolbar
