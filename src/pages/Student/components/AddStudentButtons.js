import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { getCourseList } from 'redux/actions/courseListActions'

import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'

import InviteStudentModal from './InviteStudentModal'
import UploadFileModal from './UploadFileModal'

function AddStudentButtons() {
  const dispatch = useDispatch()
  const [inviteModal, setInviteModal] = useState(false)
  const [uploadModal, setUploadModal] = useState(false)

  const onClick = () => {
    setInviteModal(true)
    dispatch(getCourseList())
  }

  return (
    <>
      <Grid container spacing={2}>
        <Grid item>
          <Button color='primary' variant='contained' onClick={onClick}>
            Invite student
          </Button>
        </Grid>
        <Grid item>
          <Button color='primary' onClick={() => setUploadModal(true)}>
            Upload CSV
          </Button>
        </Grid>
      </Grid>

      {inviteModal && (
        <InviteStudentModal open={inviteModal} onClose={() => setInviteModal(false)} />
      )}
      {uploadModal && <UploadFileModal open={uploadModal} onClose={() => setUploadModal(false)} />}
    </>
  )
}

export default AddStudentButtons
