import React from 'react'
import { useForm } from 'react-hook-form'
import { useDispatch, useSelector } from 'react-redux'
import { addStudentInviteList } from 'redux/actions/studentInviteListActions'

import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import Box from '@material-ui/core/Box'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import { Typography } from '@material-ui/core'
import RoomDropdown from 'components/RoomDropdown'

function InviteStudentModal({ open, onClose }) {
  const { register, handleSubmit, errors, control } = useForm()
  const dispatch = useDispatch()
  const { courseList } = useSelector((state) => state.courseList)

  const onSubmit = (data) => {
    const { room, ...rest } = data
    const body = {
      ...rest,
      room_id: room.id,
    }
    dispatch(addStudentInviteList(body))
  }

  return (
    <Dialog open={open} onClose={onClose} maxWidth='xs'>
      <form onSubmit={handleSubmit(onSubmit)}>
        <DialogTitle disableTypography>
          <Typography variant='h3' component='h3'>
            Invite student
          </Typography>
        </DialogTitle>
        <DialogContent>
          <Box mb={5}>
            <Grid container spacing={3}>
              {/* Room */}
              <Grid item xs={12}>
                <RoomDropdown required errors={errors} control={control} />
              </Grid>

              {/* Name */}
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  label='Full Name'
                  name='name'
                  variant='outlined'
                  inputRef={register({ required: true })}
                  error={!!errors.name}
                  helperText={!!errors.name && 'Name is required'}
                />
              </Grid>

              {/* Email */}
              <Grid item xs={12}>
                <TextField
                  type='email'
                  fullWidth
                  label='Email Address'
                  name='email'
                  variant='outlined'
                  inputRef={register({
                    required: { value: true, message: 'Email is required' },
                    pattern: {
                      value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                      message: 'Invalid email address',
                    },
                  })}
                  error={!!errors.email}
                  helperText={!!errors.email && errors.email.message}
                />
              </Grid>

              {/* Matric number */}
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  label='Matric number'
                  name='matric_no'
                  variant='outlined'
                  inputRef={register({ required: true })}
                  error={!!errors.matric_no}
                  helperText={!!errors.matric_no && 'Matric number is required'}
                />
              </Grid>

              {/* Course */}
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  label='Select course'
                  name='course_id'
                  select
                  SelectProps={{ native: true }}
                  InputLabelProps={{ shrink: true }}
                  variant='outlined'
                  defaultValue={courseList.length > 0 && courseList[0].id}
                  inputRef={register({ required: true })}
                  error={!!errors.course_id}
                  helperText={!!errors.course_id && 'Course is required'}
                >
                  {courseList.map((i, id) => (
                    <option key={i.id} value={i.id}>
                      {i.name}
                    </option>
                  ))}
                </TextField>
              </Grid>
            </Grid>
          </Box>
        </DialogContent>
        <DialogActions>
          <Button type='submit' color='primary'>
            Send invitation
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  )
}

export default InviteStudentModal
