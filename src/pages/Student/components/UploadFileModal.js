import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { parseFileUpload } from 'redux/actions/studentInviteListActions'

import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import IconButton from '@material-ui/core/IconButton'
import Snackbar from '@material-ui/core/Snackbar'
import SnackbarContent from '@material-ui/core/SnackbarContent'
import TextLink from '@material-ui/core/Link'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core'
import { DropzoneArea } from 'material-ui-dropzone'
import { X, AlertCircle } from 'react-feather'

const template_url = 'https://avicennasixcollege.s3-ap-southeast-1.amazonaws.com/template.csv'

const useStyles = makeStyles((theme) => ({
  infoAlert: {
    backgroundColor: theme.palette.info.main,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
    '& > svg': {
      marginRight: theme.spacing(1),
    },
  },
  icon: {
    fontSize: 20,
    opacity: 0.9,
  },
}))

function UploadFileModal({ open, onClose }) {
  const classes = useStyles()

  const [file, setFile] = useState(undefined)
  const [snackbar, setSnackbar] = useState(false)

  const history = useHistory()
  const dispatch = useDispatch()
  const { status } = useSelector((state) => state.uploadStudentInviteList)

  useEffect(() => {
    if (status === 'uploaded') history.push('/dashboard/students/upload')
  }, [status, history])

  const onSubmitFile = (e) => {
    e.preventDefault()

    if (!file.length) return setSnackbar(true)
    dispatch(parseFileUpload(file[0]))
  }

  return (
    <Dialog open={open} onClose={onClose} maxWidth='md' fullWidth>
      <form onSubmit={onSubmitFile}>
        <DialogTitle disableTypography>
          <Typography variant='h3' component='h3'>
            Upload CSV
          </Typography>
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            Upload CSV file to send invitation to multiple students at once. We recommend to{' '}
            <TextLink href={template_url}>use this template.</TextLink>
          </DialogContentText>
        </DialogContent>
        <DialogContent>
          <Box mb={2}>
            <DropzoneArea
              clearOnUnmount
              showFileNames
              filesLimit={1}
              acceptedFiles={[
                '.csv',
                'text/csv',
                'application/vnd.ms-excel',
                'application/csv',
                'text/x-csv',
                'application/x-csv',
                'text/comma-separated-values',
                'text/x-comma-separated-values',
              ]}
              onChange={(upload) => setFile(upload)}
            />
          </Box>

          <Snackbar
            anchorOrigin={{
              horizontal: 'left',
              vertical: 'bottom',
            }}
            autoHideDuration={3000}
            open={snackbar}
            onClose={() => setSnackbar(false)}
            variant='standard'
          >
            <SnackbarContent
              className={classes.infoAlert}
              message={
                <span className={classes.message}>
                  <AlertCircle className={classes.icon} />
                  Please upload a file
                </span>
              }
              action={[
                <IconButton key='close' color='inherit' onClick={() => setSnackbar(false)}>
                  <X className={classes.icon} />
                </IconButton>,
              ]}
            />
          </Snackbar>
        </DialogContent>
        <DialogActions>
          <Button type='submit' color='primary'>
            Upload data
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  )
}

export default UploadFileModal
