import React, { useEffect } from 'react'
import {
  Box,
  Button,
  Card,
  CardContent,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from '@material-ui/core'
import moment from 'moment'
import { useDispatch, useSelector } from 'react-redux'
import {
  getStudentInviteList,
  deleteStudentInviteList,
  renewStudentInviteList,
} from 'redux/actions/studentInviteListActions'

import PerfectScrollbar from 'react-perfect-scrollbar'
import { X as XIcon } from 'react-feather'
import AddStudentButtons from './components/AddStudentButtons'
import CustomSnackbar from 'components/CustomSnackbar'
import useReduxPagination from 'hooks/useReduxPagination'

function InviteStudent({ ...rest }) {
  const studentInviteListState = useSelector((state) => state.studentInviteList)
  const { status, successMsg, errorMsg, studentInviteList } = studentInviteListState

  const { perPage, page } = useSelector((state) => state.studentInviteListPagination)
  const { Pagination } = useReduxPagination('studentInviteList')

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getStudentInviteList())
  }, [dispatch, perPage, page])

  const cancelInvitation = (id) => {
    dispatch(deleteStudentInviteList(id))
  }

  const resendInvitation = (id) => {
    dispatch(renewStudentInviteList(id))
  }

  const isExpired = (date) => Date.now() > new Date(date)
  const showTable = ['loaded', 'added', 'updated', 'deleted', 'invite_error'].some(
    (i) => i === status
  )

  return (
    <Card {...rest}>
      {successMsg && (
        <CustomSnackbar severity='success' vertical='top' horizontal='center' variant='filled'>
          {successMsg}
        </CustomSnackbar>
      )}
      {errorMsg && (
        <CustomSnackbar severity='error' vertical='top' horizontal='center' variant='filled'>
          {errorMsg}
        </CustomSnackbar>
      )}
      {showTable && (
        <>
          <CardContent>
            <AddStudentButtons />
          </CardContent>
          <PerfectScrollbar>
            <Box minWidth={1050}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Room</TableCell>
                    <TableCell>Name</TableCell>
                    <TableCell>Email</TableCell>
                    <TableCell>Matric number</TableCell>
                    <TableCell>Course</TableCell>
                    <TableCell>Valid until</TableCell>
                    <TableCell>Cancel invitation</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {studentInviteList.map((student) => (
                    <TableRow hover key={student.id}>
                      <TableCell>{student.room?.name}</TableCell>
                      <TableCell>{student.name}</TableCell>
                      <TableCell>{student.email}</TableCell>
                      <TableCell>{student.matric_no}</TableCell>
                      <TableCell>{student.course?.name}</TableCell>
                      <TableCell>
                        {isExpired(student.expired_at) ? (
                          <Button color='secondary' onClick={() => resendInvitation(student.id)}>
                            Resend invite
                          </Button>
                        ) : (
                          moment(student.expired_at).format('DD/MM/YYYY (hh:mmA)')
                        )}
                      </TableCell>
                      <TableCell>
                        <IconButton onClick={() => cancelInvitation(student.id)}>
                          <XIcon size='20' />
                        </IconButton>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </PerfectScrollbar>
          <Pagination />
        </>
      )}
    </Card>
  )
}

export default InviteStudent
