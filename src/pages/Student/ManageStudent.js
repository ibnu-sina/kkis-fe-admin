import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'

import PerfectScrollbar from 'react-perfect-scrollbar'
import Box from '@material-ui/core/Box'
import Card from '@material-ui/core/Card'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TextLink from '@material-ui/core/Link'
import Loader from 'components/Loader'
import CustomSnackbar from 'components/CustomSnackbar'
import { getStudentList } from 'redux/actions/studentListActions'
import StatusBadge from 'components/StatusBadge'
import useReduxPagination from 'hooks/useReduxPagination'

function ManageStudent({ ...rest }) {
  const { Pagination } = useReduxPagination('studentList')
  const { studentList, status, errorMsg } = useSelector((state) => state.studentList)
  const { page, perPage } = useSelector((state) => state.studentListPagination)

  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(getStudentList())
  }, [dispatch, page, perPage])

  const roomStatus = {
    checked_out: <StatusBadge text='Checked Out' severity='error' />,
    checked_in: <StatusBadge text='Checked In' severity='success' />,
  }

  return (
    <Card {...rest}>
      {status === 'error' && (
        <CustomSnackbar vertical='top' horizontal='center' severity='error' variant='filled'>
          {errorMsg}
        </CustomSnackbar>
      )}
      <PerfectScrollbar>
        <Box minWidth={1050}>
          {status === 'loading' && <Loader />}
          {status === 'loaded' && (
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Room</TableCell>
                  <TableCell>Status</TableCell>
                  <TableCell>Name</TableCell>
                  <TableCell>Email</TableCell>
                  <TableCell>Matric number</TableCell>
                  <TableCell>Phone number</TableCell>
                  <TableCell>Course</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {studentList.map((student) => (
                  <TableRow hover key={student.id}>
                    <TableCell>{student.room?.name}</TableCell>
                    <TableCell>{roomStatus[student.room_status]}</TableCell>
                    <TableCell>
                      <TextLink component={Link} to={`/dashboard/students/${student.id}`}>
                        {student.name}
                      </TextLink>
                    </TableCell>
                    <TableCell>{student.email}</TableCell>
                    <TableCell>{student.matric_no}</TableCell>
                    <TableCell>{student.phone_no}</TableCell>
                    <TableCell>{student.course?.name}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          )}
        </Box>
      </PerfectScrollbar>
      <Pagination />
    </Card>
  )
}

export default ManageStudent
