import React, { useState } from 'react'
import { useSelector } from 'react-redux'

import { makeStyles } from '@material-ui/core'
import Box from '@material-ui/core/Box'
import Card from '@material-ui/core/Card'
import Container from '@material-ui/core/Container'
import Tab from '@material-ui/core/Tab'
import Tabs from '@material-ui/core/Tabs'
import Toolbar from './components/Toolbar'
import Typography from '@material-ui/core/Typography'

import ManageStudent from './ManageStudent'
import InviteStudent from './InviteStudent'
import CustomSnackbar from 'components/CustomSnackbar'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100vh',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}))

const StudentListView = () => {
  const classes = useStyles()
  const [value, setValue] = useState(0)
  const { status, successMsg } = useSelector((state) => state.uploadStudentInviteList)

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  return (
    <div className={classes.root}>
      <Container maxWidth='xl'>
        <Typography variant='h2' component='h2'>
          Student management
        </Typography>
        <Box mt={3}>
          {status === 'created' && (
            <CustomSnackbar variant='filled' severity='success' horizontal='center' vertical='top'>
              {successMsg}
            </CustomSnackbar>
          )}
          <Card>
            <Tabs value={value} onChange={handleChange} textColor='secondary'>
              <Tab label='Active students' />
              <Tab label='Invited students' />
            </Tabs>
            <Toolbar tab={value} />
            {value === 0 && <ManageStudent role='tabpanel' />}
            {value === 1 && <InviteStudent role='tabpanel' />}
          </Card>
        </Box>
      </Container>
    </div>
  )
}

export default StudentListView
