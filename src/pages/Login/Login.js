import React, { useEffect } from 'react'
import { Link as RouterLink } from 'react-router-dom'
import { useForm } from 'react-hook-form'
import { useDispatch, useSelector } from 'react-redux'
import { login } from 'redux/actions/userActions'
import queryString from 'query-string'

import Alert from '@material-ui/lab/Alert'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Link from '@material-ui/core/Link'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'

const Login = ({ history, location }) => {
  const dispatch = useDispatch()
  const { register, handleSubmit, errors } = useForm({ mode: 'onBlur' })

  const state = useSelector((state) => state.userLogin)

  const onSubmit = ({ email, password }) => {
    dispatch(login(email, password))
  }

  useEffect(() => {
    const { from } = queryString.parse(location.search)
    if (state.isLoggedIn) history.push(from || '/dashboard/home')
  }, [history, state, location])

  return (
    <>
      <Grid
        container
        spacing={0}
        direction='column'
        alignItems='center'
        justify='center'
        style={{ minHeight: '80vh' }}
      >
        <Grid>
          <form onSubmit={handleSubmit(onSubmit)}>
            <Box mb={3}>
              <Typography color='textPrimary' variant='h2'>
                Log in
              </Typography>
              <Typography color='textSecondary' gutterBottom variant='body2'>
                Log in on the internal platform
              </Typography>
            </Box>
            {state.error && <Alert severity='error'>{state.error}</Alert>}
            <TextField
              fullWidth
              label='Email Address'
              margin='normal'
              name='email'
              type='email'
              variant='outlined'
              inputRef={register({
                required: { value: true, message: 'Email is required' },
                pattern: {
                  value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                  message: 'Invalid email address',
                },
              })}
              error={!!errors.email}
              helperText={!!errors.email && errors.email.message}
            />
            <TextField
              fullWidth
              label='Password'
              margin='normal'
              name='password'
              type='password'
              variant='outlined'
              inputRef={register({ required: true })}
              error={!!errors.password}
              helperText={!!errors.password && 'Password is required'}
            />

            <Typography color='textSecondary' variant='body1'>
              <Link component={RouterLink} to='/forgot-password' variant='h6'>
                Forgot password?
              </Link>
            </Typography>

            <Box my={2}>
              <Button color='primary' fullWidth size='large' type='submit' variant='contained'>
                Login
              </Button>
            </Box>
          </form>
        </Grid>
      </Grid>
    </>
  )
}

export default Login
