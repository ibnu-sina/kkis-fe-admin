import React, { useState } from 'react'
import axios from 'axios'
import { useForm } from 'react-hook-form'

import Alert from '@material-ui/lab/Alert'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'

import Loader from 'components/Loader'

function ForgotPassword() {
  const [status, setStatus] = useState('idle')
  const [message, setMessage] = useState('')
  const { register, handleSubmit, errors } = useForm({ mode: 'onBlur' })

  const onSubmit = async ({ email }) => {
    try {
      setStatus('loading')
      const { data } = await axios.post('/auth/forgot-password', { email })
      setStatus('success')
      setMessage(data.message)
    } catch (error) {
      setStatus('error')
      setMessage(
        error.response && error.response.data.message ? error.response.data.message : error.message
      )
    }
  }
  return (
    <>
      <Grid
        container
        spacing={0}
        direction='column'
        alignItems='center'
        justify='center'
        style={{ minHeight: '80vh' }}
      >
        <Grid>
          <form onSubmit={handleSubmit(onSubmit)}>
            {status === 'loading' && <Loader />}
            {status === 'success' && <Alert>{message}</Alert>}
            {status === 'error' && <Alert severity='error'>{message}</Alert>}
            {status !== 'loading' && (
              <>
                <Box my={3}>
                  <Typography color='textPrimary' variant='h2'>
                    Reset Password
                  </Typography>
                  <Typography color='textSecondary' gutterBottom variant='body2'>
                    To reset your password, please provide your registered email address.
                  </Typography>
                </Box>

                <TextField
                  fullWidth
                  label='Email address'
                  margin='normal'
                  name='email'
                  type='email'
                  variant='outlined'
                  inputRef={register({
                    required: { value: true, message: 'Email is required' },
                    pattern: {
                      value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                      message: 'Invalid email address',
                    },
                  })}
                  error={!!errors.email}
                  helperText={!!errors.email && errors.email.message}
                />
                <Box my={2}>
                  <Button color='primary' fullWidth size='large' type='submit' variant='contained'>
                    Continue
                  </Button>
                </Box>
              </>
            )}
          </form>
        </Grid>
      </Grid>
    </>
  )
}

export default ForgotPassword
