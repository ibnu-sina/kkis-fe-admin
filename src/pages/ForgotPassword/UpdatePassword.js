import React, { useRef } from 'react'
import { useParams } from 'react-router-dom'
import { useForm } from 'react-hook-form'
import { useDispatch, useSelector } from 'react-redux'
import { updatePasswordByToken } from 'redux/actions/userActions'

import Alert from '@material-ui/lab/Alert'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import Loader from 'components/Loader'

function UpdatePassword() {
  const { token } = useParams()
  const dispatch = useDispatch()

  const { error, success, loading } = useSelector((state) => state.userLogin)

  const { register, handleSubmit, errors, watch } = useForm({ mode: 'onChange' })
  const password = useRef({})
  password.current = watch('password', '')

  const onSubmit = ({ password, confirm_password }) => {
    dispatch(updatePasswordByToken(password, confirm_password, token))
  }

  return (
    <>
      <Grid
        container
        spacing={0}
        direction='column'
        alignItems='center'
        justify='center'
        style={{ minHeight: '80vh' }}
      >
        <Grid>
          {loading && <Loader />}
          {!loading && (
            <form onSubmit={handleSubmit(onSubmit)}>
              <Box mb={3}>
                <Typography color='textPrimary' variant='h2'>
                  Update password
                </Typography>
                <Typography color='textSecondary' gutterBottom variant='body2'>
                  Enter your new password
                </Typography>
              </Box>
              {!!success && <Alert severity='success'>{success}</Alert>}
              {!!error && <Alert severity='error'>{error}</Alert>}
              <TextField
                fullWidth
                label='Password'
                margin='normal'
                name='password'
                type='password'
                variant='outlined'
                inputRef={register({ required: true })}
                error={!!errors.password}
                helperText={!!errors.password && 'Password is required'}
                disabled={!!error}
              />

              <TextField
                fullWidth
                label='Confirm Password'
                margin='normal'
                name='confirm_password'
                type='password'
                variant='outlined'
                inputRef={register({
                  validate: (value) => value === password.current || 'The passwords do not match',
                })}
                error={!!errors.confirm_password}
                helperText={!!errors.confirm_password && errors.confirm_password.message}
                disabled={!!error}
              />

              <Box my={2}>
                <Button
                  color='primary'
                  fullWidth
                  size='large'
                  type='submit'
                  variant='contained'
                  disabled={!!error}
                >
                  Update password
                </Button>
              </Box>
            </form>
          )}
        </Grid>
      </Grid>
    </>
  )
}

export default UpdatePassword
