import React, { useEffect } from 'react'
import { Box, Button, Container, Grid, makeStyles, Typography } from '@material-ui/core'

import RoomList from './RoomList'
import { useRoomForm } from './hooks/useRoomForm'
import { useDispatch, useSelector } from 'react-redux'
import { getRoomList } from 'redux/actions/roomActions'
import Loader from 'components/Loader'
import CustomSnackbar from 'components/CustomSnackbar'
import { RESET_ROOM } from 'redux/types/roomTypes'
import useReduxPagination from 'hooks/useReduxPagination'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100vh',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}))

function RoomListView() {
  const classes = useStyles()
  const dispatch = useDispatch()
  const { openForm, RoomForm } = useRoomForm('add')

  const { Pagination } = useReduxPagination('roomList', getRoomList)
  const { page, perPage } = useSelector((state) => state.roomListPagination)
  const { status, successMsg, errorMsg } = useSelector((state) => state.roomList)
  const { successMsg: successMsgRoom, errorMsg: errorMsgRoom } = useSelector((state) => state.room)

  useEffect(() => {
    dispatch(getRoomList())

    return () => dispatch({ type: RESET_ROOM })
  }, [dispatch, page, perPage])

  return (
    <div className={classes.root}>
      <Container>
        <Box>
          <Typography variant='h2' component='h2'>
            Room management
          </Typography>
          {successMsg && (
            <CustomSnackbar vertical='top' horizontal='center' variant='filled' severity='success'>
              {successMsg}
            </CustomSnackbar>
          )}
          {errorMsg && (
            <CustomSnackbar vertical='top' horizontal='center' variant='filled' severity='error'>
              {errorMsg}
            </CustomSnackbar>
          )}
          {successMsgRoom && (
            <CustomSnackbar vertical='top' horizontal='center' variant='filled' severity='success'>
              {successMsgRoom}
            </CustomSnackbar>
          )}
          {errorMsgRoom && (
            <CustomSnackbar vertical='top' horizontal='center' variant='filled' severity='error'>
              {errorMsgRoom}
            </CustomSnackbar>
          )}
          <Box mt={3}>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <Box mb={2}>
                  <RoomForm />
                  <Button color='primary' variant='contained' onClick={() => openForm(true)}>
                    Add room
                  </Button>
                </Box>
                {status === 'loading' && <Loader />}
                {(status === 'loaded' || status === 'error') && (
                  <RoomList Pagination={Pagination} />
                )}
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
    </div>
  )
}

export default RoomListView
