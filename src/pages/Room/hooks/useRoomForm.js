import React, { useState } from 'react'
import { useForm } from 'react-hook-form'

import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import { Typography } from '@material-ui/core'
import { Capitalize } from 'helpers'
import { useDispatch } from 'react-redux'
import { addRoom, editRoom } from 'redux/actions/roomActions'

function useRoomForm(action) {
  const dispatch = useDispatch()
  const ACTION = Capitalize(action)
  const isEdit = action === 'edit'

  const [open, setOpen] = useState(false)
  const onClose = () => setOpen(false)

  // handle submit
  const onSubmit = (data) => {
    const { id, ...body } = data
    if (isEdit) {
      dispatch(editRoom(body, id))
    } else {
      dispatch(addRoom(body))
    }
    onClose()
  }

  function RoomForm({ room }) {
    const { register, handleSubmit, errors } = useForm({
      defaultValues: {
        id: room?.id,
        block: room?.block,
        number: room?.number,
        capacity: room?.capacity,
        description: room?.description,
      },
    })

    return (
      <Dialog open={open} onClose={onClose} maxWidth='xs'>
        <form onSubmit={handleSubmit(onSubmit)}>
          <DialogTitle disableTypography>
            <Typography variant='h3' component='h3'>
              {ACTION} room
            </Typography>
          </DialogTitle>
          <DialogContent>
            <Grid container spacing={1}>
              <TextField name='id' inputRef={register} style={{ display: 'none' }} />
              <Grid item xs={12}>
                <TextField
                  type='text'
                  fullWidth
                  label='Block'
                  name='block'
                  variant='outlined'
                  inputRef={register({ required: true })}
                  error={!!errors.block}
                  helperText={!!errors.block && 'Room block is required'}
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  type='text'
                  fullWidth
                  label='Number'
                  name='number'
                  variant='outlined'
                  inputRef={register({ required: true })}
                  error={!!errors.number}
                  helperText={!!errors.block && 'Room number is required'}
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  type='number'
                  fullWidth
                  label='Capacity'
                  name='capacity'
                  variant='outlined'
                  inputRef={register({ required: true })}
                  error={!!errors.capacity}
                  helperText={!!errors.block && 'Room capacity is required'}
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  type='text'
                  fullWidth
                  label='Description'
                  name='description'
                  variant='outlined'
                  inputRef={register}
                  error={!!errors.description}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button type='submit' color='primary' variant='contained'>
              {isEdit ? 'Update' : 'Confirm'}
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    )
  }

  return { RoomForm, openForm: setOpen }
}

export { useRoomForm }
