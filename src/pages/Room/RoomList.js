import React from 'react'
import {
  Card,
  CardContent,
  CardHeader,
  Divider,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from '@material-ui/core'
import RoomTableItem from './components/RoomTableItem'
import StatusFilter from './components/StatusFilter'
import { useSelector } from 'react-redux'

function RoomList({ Pagination }) {
  const { roomList } = useSelector((state) => state.roomList)
  return (
    <Card>
      <CardContent>
        <StatusFilter />
      </CardContent>
      <CardHeader title='List of room' />
      <Divider />
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell>Occupied</TableCell>
            <TableCell>Capacity</TableCell>
            <TableCell>Status</TableCell>
            <TableCell>Description</TableCell>
            <TableCell align='center'>Actions</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {roomList.map((room) => (
            <RoomTableItem room={room} key={room.id} />
          ))}
        </TableBody>
      </Table>
      <Pagination />
    </Card>
  )
}

export default RoomList
