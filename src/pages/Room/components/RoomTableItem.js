import React, { useState } from 'react'
import { Button, IconButton, TableCell, TableRow } from '@material-ui/core'
import { ArrowRight, Edit2, Trash } from 'react-feather'
import StatusBadge from 'components/StatusBadge'
import { OccupantInfo } from './OccupantInfo'
import { useRoomForm } from '../hooks/useRoomForm'
import { useDispatch } from 'react-redux'
import { useConfirm } from 'material-ui-confirm'
import { deleteRoom } from 'redux/actions/roomActions'

function RoomTableItem({ room }) {
  const [openInfo, setOpenInfo] = useState(false)
  const { openForm, RoomForm } = useRoomForm('edit')
  const dispatch = useDispatch()
  const confirm = useConfirm()

  const handleDelete = async (id) => {
    await confirm({
      confirmationText: 'Delete',
      confirmationButtonProps: { color: 'secondary', variant: 'contained' },
      description: `This will permanently delete room ${room.name}.`,
    })
    dispatch(deleteRoom(id))
  }

  return (
    <TableRow hover>
      <TableCell>{room.name}</TableCell>
      <TableCell>
        <Button
          color='primary'
          endIcon={<ArrowRight size='15' />}
          onClick={() => setOpenInfo(true)}
        >
          {room.occupied}
        </Button>
      </TableCell>
      <TableCell>{room.capacity}</TableCell>
      <TableCell>
        {room.is_available ? (
          <StatusBadge text='Available' severity='success' />
        ) : (
          <StatusBadge text='Full' severity='error' />
        )}
      </TableCell>
      <TableCell>{room.description || '-'}</TableCell>
      <TableCell align='center'>
        <IconButton onClick={() => openForm(true)}>
          <Edit2 size='20' />
        </IconButton>
        <IconButton onClick={() => handleDelete(room.id)}>
          <Trash size='20' />
        </IconButton>
      </TableCell>
      <RoomForm room={room} />
      <OccupantInfo
        students={room.students}
        open={openInfo}
        handleClose={() => setOpenInfo(false)}
      />
    </TableRow>
  )
}

export default RoomTableItem
