import React from 'react'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Link as TextLink,
  makeStyles,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Typography,
} from '@material-ui/core'
import { Link } from 'react-router-dom'

function OccupantInfo({ students, open, handleClose }) {
  const useStyles = makeStyles((theme) => ({ text: { color: theme.palette.text.secondary } }))
  const classes = useStyles()

  return (
    <Dialog open={open} onClose={handleClose} maxWidth='xs' fullWidth>
      <DialogTitle disableTypography>
        <Typography variant='h5' component='h5'>
          Occupant Info
        </Typography>
      </DialogTitle>
      <DialogContent>
        <Table>
          {students?.length === 0 ? (
            <TableBody>
              <Typography>No occupants</Typography>
            </TableBody>
          ) : (
            <TableBody>
              {students?.map((student, index) => (
                <TableRow key={index}>
                  <TableCell>
                    <Typography variant='h6'>Student {index + 1}</Typography>
                  </TableCell>
                  <TableCell className={classes.text}>
                    <TextLink component={Link} to={`/dashboard/students/${student.id}`}>
                      {student.name}
                    </TextLink>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          )}
        </Table>
      </DialogContent>
      <DialogActions>
        <Button fullWidth color='primary' onClick={handleClose}>
          OK
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export { OccupantInfo }
