import React from 'react'
import { TextField } from '@material-ui/core'
import { UPDATE_ROOM_STATUS_FILTER } from 'redux/types/roomTypes'
import { useDispatch, useSelector } from 'react-redux'
import { getRoomList } from 'redux/actions/roomActions'

function StatusFilter() {
  const dispatch = useDispatch()
  const { filter } = useSelector((state) => state.roomList)

  const handleChange = (e) => {
    dispatch({ type: UPDATE_ROOM_STATUS_FILTER, payload: e.target.value })
    dispatch(getRoomList())
  }

  return (
    <TextField
      select
      label='Status'
      onChange={handleChange}
      value={filter.is_available}
      SelectProps={{ native: true }}
      variant='outlined'
      InputLabelProps={{ shrink: true }}
    >
      <option value=''>All</option>
      <option value='0'>Full</option>
      <option value='1'>Available</option>
    </TextField>
  )
}

export default StatusFilter
