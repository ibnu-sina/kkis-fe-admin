import React, { useRef, useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { useDispatch } from 'react-redux'
import { updatePassword } from 'redux/actions/profileActions'

import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardHeader from '@material-ui/core/CardHeader'
import Divider from '@material-ui/core/Divider'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'

function ChangePassword({ status, errorMsg }) {
  const dispatch = useDispatch()

  const { register, handleSubmit, errors, watch, setValue } = useForm({ mode: 'onChange' })
  const password = useRef({})
  password.current = watch('password', '')

  const onSubmit = ({ old_password, password }) => {
    dispatch(updatePassword(old_password, password))
  }

  useEffect(() => {
    if (status === 'password_success') {
      setValue('old_password', '')
      setValue('password', '')
      setValue('confirm_password', '')
    }
  }, [status, setValue])

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Card>
        <CardHeader title='Change password' />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>
            <Grid item xs={12}></Grid>
            <Grid item xs={12}>
              <Typography component='p' color='textSecondary' variant='h6'>
                CURRENT PASSWORD
              </Typography>
            </Grid>
            <Grid item md={6} xs={12}>
              <TextField
                autoFocus={!!errorMsg}
                fullWidth
                label='Current Password'
                name='old_password'
                type='password'
                variant='outlined'
                inputRef={register({ required: true })}
                error={!!errors.old_password}
                helperText={!!errors.old_password && 'Old password is required'}
              />
            </Grid>
            <Grid item xs={12}>
              <Typography component='p' color='textSecondary' variant='h6'>
                NEW PASSWORD
              </Typography>
            </Grid>
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label='Password'
                name='password'
                type='password'
                variant='outlined'
                inputRef={register({ required: true })}
                error={!!errors.password}
                helperText={!!errors.password && 'Password is required'}
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label='Confirm Password'
                name='confirm_password'
                type='password'
                variant='outlined'
                inputRef={register({
                  required: { value: true, message: 'Please confirm password' },
                  validate: (value) => value === password.current || 'The passwords do not match',
                })}
                error={!!errors.confirm_password}
                helperText={!!errors.confirm_password && errors.confirm_password.message}
              />
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        <Box display='flex' justifyContent='flex-end' p={2}>
          <Box>
            <Button color='primary' variant='contained' type='submit'>
              Update password
            </Button>
          </Box>
        </Box>
      </Card>
    </form>
  )
}

export default ChangePassword
