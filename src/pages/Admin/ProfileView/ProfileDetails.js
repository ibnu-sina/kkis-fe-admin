import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
  Typography,
} from '@material-ui/core'
import React, { useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { useDispatch } from 'react-redux'
import { editMyProfile } from 'redux/actions/profileActions'

const gender = ['Male', 'Female']

function ProfileDetails({ user, isMe }) {
  const { register, handleSubmit, errors, setValue } = useForm()
  const dispatch = useDispatch()

  useEffect(() => {
    const config = { shouldDirty: true }
    setValue('name', user.name, config)
    setValue('gender', user.gender, config)
    setValue('phone_no', user.phone_no, config)
  }, [user, setValue])

  const onSubmit = (data) => {
    dispatch(editMyProfile(data))
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Card>
        <CardHeader subheader={isMe && 'The information can be edited'} title='Profile' />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Typography component='p' color='textSecondary' variant='h6'>
                PERSONAL INFO
              </Typography>
            </Grid>
            <Grid item md={6} xs={12}>
              <TextField
                InputProps={{ readOnly: !isMe }}
                fullWidth
                label='Name'
                name='name'
                variant='outlined'
                inputRef={register({ required: true })}
                error={!!errors.name}
                helperText={!!errors.name && 'Name is required'}
                InputLabelProps={{ shrink: true }}
              />
            </Grid>
            {isMe ? (
              <Grid item md={6} xs={12}>
                <TextField
                  fullWidth
                  label='Gender'
                  name='gender'
                  select
                  SelectProps={{ native: true }}
                  inputRef={register}
                  variant='outlined'
                  InputLabelProps={{ shrink: true }}
                >
                  {gender.map((option) => (
                    <option key={option} value={option}>
                      {option}
                    </option>
                  ))}
                </TextField>
              </Grid>
            ) : (
              <Grid item md={6} xs={12}>
                <TextField
                  InputProps={{ readOnly: true }}
                  fullWidth
                  label='Gender'
                  name='gender'
                  inputRef={register}
                  variant='outlined'
                  InputLabelProps={{ shrink: true }}
                />
              </Grid>
            )}
            <Grid item xs={12}>
              <Typography component='p' color='textSecondary' variant='h6'>
                COLLEGE INFO
              </Typography>
            </Grid>
            <Grid item md={6} xs={12}>
              <TextField
                InputProps={{ readOnly: true }}
                fullWidth
                label='Role (Read Only)'
                name='role'
                value={user.role}
                variant='outlined'
                InputLabelProps={{ shrink: true }}
              />
            </Grid>
            <Grid item xs={12}>
              <Typography component='p' color='textSecondary' variant='h6'>
                CONTACT INFO
              </Typography>
            </Grid>
            <Grid item md={6} xs={12}>
              <TextField
                InputProps={{ readOnly: true }}
                fullWidth
                label='Email Address (Read Only)'
                name='email'
                variant='outlined'
                value={user.email}
                InputLabelProps={{ shrink: true }}
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <TextField
                InputProps={{ readOnly: !isMe }}
                fullWidth
                label='Phone Number'
                name='phone_no'
                type='number'
                inputRef={register}
                variant='outlined'
                InputLabelProps={{ shrink: true }}
              />
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        {isMe && (
          <Box display='flex' justifyContent='flex-end' p={2}>
            <Box>
              <Button color='primary' variant='contained' type='submit'>
                Update profile
              </Button>
            </Box>
          </Box>
        )}
      </Card>
    </form>
  )
}

export default ProfileDetails
