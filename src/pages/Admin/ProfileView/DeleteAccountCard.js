import React, { useState } from 'react'
import { Box, Button, Card, CardContent, Typography } from '@material-ui/core'
import { deleteAccount } from 'redux/actions/profileActions'
import { useDispatch } from 'react-redux'
import ConfirmDeleteDialog from './ConfirmDeleteDialog'

function DeleteAccountCard({ user }) {
  const [open, setOpen] = useState(false)
  const dispatch = useDispatch()
  const openConfirm = () => {
    setOpen(true)
  }

  const handleOk = (id) => {
    dispatch(deleteAccount(id))
  }
  return (
    <Card>
      <ConfirmDeleteDialog
        open={open}
        handleCancel={() => setOpen(false)}
        handleOk={() => handleOk(user.id)}
      />
      <CardContent>
        <Box display='flex' flexDirection='column' mb={5}>
          <Typography color='secondary' variant='h4'>
            Delete account
          </Typography>

          <Box my={4}>
            <Typography variant='body1' component='p'>
              This action cannot be undone. You will need a new registration link if you want to
              login again.
            </Typography>
            <Typography variant='body1' component='p'>
              Account will be permanently deleted immediately.
            </Typography>
          </Box>

          <Typography>
            Are you <b>ABSOLUTELY SURE</b> you wish to delete your account?
          </Typography>
        </Box>
        <Button color='secondary' variant='contained' onClick={openConfirm}>
          DELETE ACCOUNT
        </Button>
      </CardContent>
    </Card>
  )
}

export default DeleteAccountCard
