import React from 'react'
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Typography,
} from '@material-ui/core'
import { Alert, AlertTitle } from '@material-ui/lab'

function ConfirmDeleteDialog({ open, handleCancel, handleOk }) {
  return (
    <Dialog maxWidth='sm' open={open}>
      <DialogTitle disableTypography>
        <Typography color='secondary' variant='h4'>
          Delete account. Are you ABSOLUTELY SURE?
        </Typography>
      </DialogTitle>
      <DialogContent>
        <Box mb={3}>
          <Alert severity='error'>
            <AlertTitle>You are about to permanently delete your account</AlertTitle>
            Once your account is permanently deleted it cannot be recovered.
          </Alert>
        </Box>
        <DialogContentText>
          This action cannot be undone. You will need a new registration link if you want to login
          again.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCancel}>CANCEL</Button>
        <Button onClick={handleOk} color='secondary' variant='contained'>
          CONFIRM
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default ConfirmDeleteDialog
