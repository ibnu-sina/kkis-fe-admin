import React, { useEffect, useState } from 'react'
import { getProfile } from 'redux/actions/profileActions'
import { useDispatch, useSelector } from 'react-redux'

import { Box, Button, Container, Grid, makeStyles } from '@material-ui/core'

import Loader from 'components/Loader'
import CustomSnackbacr from 'components/CustomSnackbar'
import ProfileDetails from './ProfileDetails'
import ChangePassword from './ChangePassword'
import SectionList from 'components/SectionList'
import DeleteAccountCard from './DeleteAccountCard'
import { ChevronLeft as ChevronLeftIcon } from 'react-feather'
import { Link } from 'react-router-dom'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100vh',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}))

const myListItem = [
  { section: 'profile', label: 'Profile' },
  { section: 'change-password', label: 'Change Password' },
  { section: 'delete-account', label: 'Delete Account' },
]

const listItem = [{ section: 'profile', label: 'Profile' }]

function Profile({ match }) {
  const classes = useStyles()

  const id = Number(match.params.id)
  const dispatch = useDispatch()
  const { successMsg, errorMsg, status, adminProfile } = useSelector((state) => state.profile)
  const { uid } = useSelector((state) => state.userLogin)

  const [isMe, setIsMe] = useState(false)
  const [section, setSection] = useState('profile')

  useEffect(() => {
    dispatch(getProfile(id))
    setIsMe(id === uid)
  }, [dispatch, id, match, uid])

  return (
    <div className={classes.root}>
      <Container maxWidth='lg'>
        {status === 'loading' && <Loader />}
        {(errorMsg || successMsg) && (
          <CustomSnackbacr severity={errorMsg ? 'error' : 'success'}>
            {errorMsg || successMsg}
          </CustomSnackbacr>
        )}
        <Button
          color='primary'
          component={Link}
          startIcon={<ChevronLeftIcon />}
          to='/dashboard/admins'
        >
          Back to admin list
        </Button>
        {status !== 'loading' && (
          <Box mt={3}>
            <Grid container spacing={3}>
              <Grid item lg={4} md={6} xs={12}>
                <SectionList
                  section={section}
                  setSection={setSection}
                  listItem={isMe ? myListItem : listItem}
                />
              </Grid>
              <Grid item lg={8} md={6} xs={12}>
                {section === 'profile' && (
                  <ProfileDetails user={adminProfile} isMe={isMe} successMsg={successMsg} />
                )}
                {section === 'change-password' && (
                  <ChangePassword status={status} errorMsg={errorMsg} />
                )}
                {section === 'delete-account' && (
                  <DeleteAccountCard user={adminProfile} status={status} errorMsg={errorMsg} />
                )}
              </Grid>
            </Grid>
          </Box>
        )}
      </Container>
    </div>
  )
}

export default Profile
