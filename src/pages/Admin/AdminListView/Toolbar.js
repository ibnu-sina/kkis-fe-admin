import React from 'react'

import { getAdminList } from 'redux/actions/adminListActions'
import { Box, Card, CardContent } from '@material-ui/core'
import useReduxSearch from 'hooks/useReduxSearch'

function Toolbar({ className, ...rest }) {
  const { SearchField } = useReduxSearch('adminList', getAdminList)
  return (
    <div className={className} {...rest}>
      <Box mt={3}>
        <Card>
          <CardContent>
            <Box maxWidth={500}>
              <SearchField />
            </Box>
          </CardContent>
        </Card>
      </Box>
    </div>
  )
}

export default Toolbar
