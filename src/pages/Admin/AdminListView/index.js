import React, { useEffect } from 'react'
import { getAdminList } from 'redux/actions/adminListActions'
import { useDispatch, useSelector } from 'react-redux'

import { Box, Container, makeStyles, Typography } from '@material-ui/core'

import Results from './Results'
import Toolbar from './Toolbar'
import Loader from 'components/Loader'
import CustomSnackbar from 'components/CustomSnackbar'
import useReduxPagination from 'hooks/useReduxPagination'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100vh',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}))

function AdminListView() {
  const classes = useStyles()
  const dispatch = useDispatch()

  const { page, perPage } = useSelector((state) => state.adminListPagination)
  const { errorMsg, status } = useSelector((state) => state.adminList)
  const { Pagination } = useReduxPagination('adminList')

  useEffect(() => {
    dispatch(getAdminList())
  }, [dispatch, page, perPage])

  return (
    <div className={classes.root}>
      <Container>
        <Typography variant='h2' component='h2'>
          All admin
        </Typography>
        <Toolbar />
        <Box mt={3}>
          {errorMsg && (
            <CustomSnackbar horizontal='center' vertical='top' variant='filled' severity='error'>
              {errorMsg}
            </CustomSnackbar>
          )}
          {status === 'loading' && <Loader />}
          {status === 'loaded' && <Results Pagination={Pagination} />}
        </Box>
      </Container>
    </div>
  )
}

export default AdminListView
