import React from 'react'
import moment from 'moment'
import PerfectScrollbar from 'react-perfect-scrollbar'
import {
  Box,
  Card,
  Link,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from '@material-ui/core'
import { NavLink as RouterLink } from 'react-router-dom'
import { useSelector } from 'react-redux'

const Results = ({ Pagination }) => {
  const { adminList } = useSelector((state) => state.adminList)

  return (
    <Card>
      <PerfectScrollbar>
        <Box minWidth={1050}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Name</TableCell>
                <TableCell>Role</TableCell>
                <TableCell>Email</TableCell>
                <TableCell>Phone No.</TableCell>
                <TableCell>Register date</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {adminList.map((admin) => (
                <TableRow hover key={admin.id}>
                  <TableCell>
                    <Link component={RouterLink} to={`/dashboard/admins/${admin.id}`}>
                      {admin.name}
                    </Link>
                  </TableCell>
                  <TableCell>{admin.role}</TableCell>
                  <TableCell>{admin.email}</TableCell>
                  <TableCell>{admin.phone_no}</TableCell>
                  <TableCell>{moment(admin.created_at).format('DD/MM/YYYY')}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <Pagination />
    </Card>
  )
}

export default Results
