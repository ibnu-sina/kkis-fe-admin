import React, { useEffect } from 'react'
import { getInviteList } from 'redux/actions/inviteListActions'
import { useDispatch, useSelector } from 'react-redux'

import { Box, Container, makeStyles, Typography } from '@material-ui/core'

import Results from './Results'
import AdminInvite from './AdminInvite'
import Loader from 'components/Loader'
import CustomSnackbar from 'components/CustomSnackbar'
import useReduxPagination from 'hooks/useReduxPagination'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100vh',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}))

function InvitedAdminView() {
  const classes = useStyles()
  const dispatch = useDispatch()

  const { Pagination } = useReduxPagination('inviteList')
  const { successMsg, errorMsg, status } = useSelector((state) => state.inviteList)
  const { page, perPage } = useSelector((state) => state.inviteListPagination)

  useEffect(() => {
    dispatch(getInviteList())
  }, [dispatch, page, perPage])

  return (
    <div className={classes.root}>
      <Container>
        <Typography variant='h2' component='h2'>
          Admin invite list
        </Typography>
        <AdminInvite />
        <Box mt={3}>
          {status === 'loading' && <Loader />}
          {errorMsg && (
            <CustomSnackbar horizontal='center' vertical='top' variant='filled' severity='error'>
              {errorMsg}
            </CustomSnackbar>
          )}
          {successMsg && (
            <CustomSnackbar horizontal='center' vertical='top' variant='filled' severity='success'>
              {successMsg}
            </CustomSnackbar>
          )}
          {(status === 'loaded' || status === 'invite_error' || status === 'updated') && (
            <Results Pagination={Pagination} />
          )}
        </Box>
      </Container>
    </div>
  )
}

export default InvitedAdminView
