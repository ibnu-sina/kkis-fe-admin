import React, { useState, useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { useDispatch, useSelector } from 'react-redux'
import { addInviteList } from 'redux/actions/inviteListActions'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import { Typography } from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'

const roles = ['Master', 'Fellow', 'Staff', 'JTK']

function AdminInvite() {
  const [open, setOpen] = useState(false)
  const { register, handleSubmit, errors } = useForm()

  const dispatch = useDispatch()
  const { errorMsg, status } = useSelector((state) => state.inviteList)

  useEffect(() => {
    if (status === 'loaded') setOpen(false)
  }, [status, setOpen])

  const onSubmit = ({ email, role }) => {
    dispatch(addInviteList(email, role))
  }
  return (
    <>
      <Box display='flex' justifyContent='flex-end'>
        <Button color='primary' variant='contained' onClick={() => setOpen(true)}>
          Invite admin
        </Button>
      </Box>
      <Dialog open={open} onClose={() => setOpen(false)} maxWidth='xs'>
        <form onSubmit={handleSubmit(onSubmit)}>
          <DialogTitle disableTypography>
            <Typography variant='h3' component='h3'>
              Invite admin
            </Typography>
          </DialogTitle>
          <DialogContent>
            <Grid container spacing={1}>
              <Grid item xs={12}>
                {status === 'invite_error' && <Alert severity='error'>{errorMsg}</Alert>}
              </Grid>
              <Grid item xs={12}>
                <TextField
                  type='email'
                  fullWidth
                  label='Email Address'
                  name='email'
                  variant='outlined'
                  inputRef={register({
                    required: true,
                    pattern: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                  })}
                  error={!!errors.email}
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  name='role'
                  fullWidth
                  select
                  SelectProps={{ native: true }}
                  variant='outlined'
                  defaultValue=''
                  inputRef={register({ required: true })}
                >
                  {roles.map((option) => (
                    <option key={option} value={option}>
                      {option}
                    </option>
                  ))}
                </TextField>
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button type='submit' color='primary'>
              Send invite
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </>
  )
}

export default AdminInvite
