import React from 'react'
import moment from 'moment'
import { useDispatch, useSelector } from 'react-redux'
import { renewInviteList, deleteInviteList, getInviteList } from 'redux/actions/inviteListActions'

import PerfectScrollbar from 'react-perfect-scrollbar'
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from '@material-ui/core'
import { X } from 'react-feather'
import useReduxSearch from 'hooks/useReduxSearch'

const Results = ({ Pagination }) => {
  const dispatch = useDispatch()
  const { SearchField } = useReduxSearch('inviteList', getInviteList)

  const { inviteList } = useSelector((state) => state.inviteList)

  const isExpired = (date) => {
    return Date.now() > new Date(date)
  }

  const resendInvitation = (id) => {
    dispatch(renewInviteList(id))
  }

  const deleteInvitation = (id) => {
    dispatch(deleteInviteList(id))
  }

  return (
    <Box mt={2}>
      <Card>
        <CardContent>
          <Grid container>
            <Grid item xs={12} md={6} lg={4}>
              <SearchField />
            </Grid>
          </Grid>
        </CardContent>
        <PerfectScrollbar>
          <Box minWidth={1000}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell width='20%'>Role</TableCell>
                  <TableCell>Email</TableCell>
                  <TableCell width='20%'>Valid until</TableCell>
                  <TableCell width='15%' align='center'>
                    Cancel invitation
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {inviteList.map((admin) => (
                  <TableRow hover key={admin.id}>
                    <TableCell>{admin.role}</TableCell>
                    <TableCell>{admin.email}</TableCell>
                    <TableCell>
                      {isExpired(admin.expired_at) ? (
                        <Button color='secondary' onClick={() => resendInvitation(admin.id)}>
                          Resend Invitation
                        </Button>
                      ) : (
                        moment(admin.expired_at).format('DD/MM/YYYY (hh:mmA)')
                      )}
                    </TableCell>
                    <TableCell align='center'>
                      <IconButton onClick={() => deleteInvitation(admin.id)}>
                        <X size='20' />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </Box>
        </PerfectScrollbar>
        <Pagination />
      </Card>
    </Box>
  )
}

export default Results
