import React from 'react'
import {
  Card,
  CardHeader,
  Divider,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from '@material-ui/core'
import { Edit2, Trash } from 'react-feather'
import { useDispatch, useSelector } from 'react-redux'
import { deleteAppliance } from 'redux/actions/applianceListActions'

function ApplianceList({ setIsEdit, setEditData, Pagination }) {
  const dispatch = useDispatch()

  const { applianceList } = useSelector((state) => state.applianceList)

  const toggleEdit = (item) => {
    setEditData(item)
    setIsEdit(true)
  }

  const handleDelete = (id) => {
    dispatch(deleteAppliance(id))
  }

  return (
    <Card>
      <CardHeader title='List of appliances' />
      <Divider />
      <Table>
        <TableHead>
          <TableRow>
            <TableCell width='40%'>Name</TableCell>
            <TableCell width='40%'>Price</TableCell>
            <TableCell align='center'>Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {applianceList.map((item) => (
            <TableRow hover key={item.id}>
              <TableCell>{item.name}</TableCell>
              <TableCell>{'RM' + Number(item.price).toFixed(2)}</TableCell>
              <TableCell>
                <IconButton onClick={() => toggleEdit(item)}>
                  <Edit2 size='20' />
                </IconButton>
                <IconButton onClick={() => handleDelete(item.id)}>
                  <Trash size='20' />
                </IconButton>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <Pagination />
    </Card>
  )
}

export default ApplianceList
