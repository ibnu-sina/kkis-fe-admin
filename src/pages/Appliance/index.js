import React, { useEffect, useState } from 'react'
import { Box, Container, Grid, makeStyles, Typography } from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux'
import { getApplianceList } from 'redux/actions/applianceListActions'
import { RESET_APPLIANCE } from 'redux/types/applianceListTypes'

import ApplianceList from './ApplianceList'
import AddAppliance from './AddAppliance'
import Loader from 'components/Loader'
import CustomSnackbar from 'components/CustomSnackbar'
import useReduxPagination from 'hooks/useReduxPagination'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100vh',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}))

const ApplianceListView = () => {
  const classes = useStyles()
  const dispatch = useDispatch()

  const { Pagination } = useReduxPagination('applianceList', getApplianceList)
  const { page, perPage } = useSelector((state) => state.applianceListPagination)
  const { status, successMsg, errorMsg } = useSelector((state) => state.applianceList)
  const { successMsg: editSuccessMsg } = useSelector((state) => state.appliance)

  const [isEdit, setIsEdit] = useState(false)
  const [editData, setEditData] = useState()

  useEffect(() => {
    dispatch(getApplianceList())
    return () => dispatch({ type: RESET_APPLIANCE })
  }, [dispatch, page, perPage])

  return (
    <div className={classes.root}>
      <Container>
        <Box>
          <Typography variant='h2' component='h2'>
            Appliance management
          </Typography>
          <Box mt={3}>
            <Grid container spacing={3}>
              <Grid item lg={6} xs={12}>
                <AddAppliance isEdit={isEdit} editData={editData} setIsEdit={setIsEdit} />
              </Grid>
              <Grid item lg={6} xs={12}>
                {errorMsg && (
                  <CustomSnackbar
                    horizontal='center'
                    vertical='top'
                    severity='error'
                    variant='filled'
                  >
                    {errorMsg}
                  </CustomSnackbar>
                )}
                {(successMsg || editSuccessMsg) && (
                  <CustomSnackbar
                    horizontal='center'
                    vertical='top'
                    severity='success'
                    variant='filled'
                  >
                    {successMsg || editSuccessMsg}
                  </CustomSnackbar>
                )}
                {status === 'loading' && <Loader />}
                {status === 'loaded' && (
                  <ApplianceList
                    setIsEdit={setIsEdit}
                    setEditData={setEditData}
                    Pagination={Pagination}
                  />
                )}
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
    </div>
  )
}

export default ApplianceListView
