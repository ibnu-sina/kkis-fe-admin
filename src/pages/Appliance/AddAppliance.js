import React, { useEffect, useState } from 'react'
import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
} from '@material-ui/core'
import { useForm } from 'react-hook-form'
import { useDispatch } from 'react-redux'
import { addAppliance, editAppliance } from 'redux/actions/applianceListActions'

function AddAppliance({ isEdit, editData, setIsEdit }) {
  const dispatch = useDispatch()
  const { register, handleSubmit, errors, setValue } = useForm()

  const onSubmit = (data) => {
    if (isEdit) {
      dispatch(editAppliance(data, editData.id))
      setIsEdit(false)
    } else {
      dispatch(addAppliance(data))
    }
    setValue('name', '')
    setValue('price', '')
  }

  const [shrink, setShrink] = useState()
  useEffect(() => {
    if (editData) {
      setShrink(true)
      setValue('name', editData.name)
      setValue('price', editData.price)
    }
  }, [editData, setValue])

  return (
    <Card>
      <CardHeader title={`${isEdit ? 'Edit' : 'Add'} an appliance`} />
      <Divider />
      <form onSubmit={handleSubmit(onSubmit)}>
        <CardContent>
          <Grid container spacing={1}>
            <Grid item md={6} xs={12}>
              <TextField
                type='text'
                fullWidth
                label='Name'
                name='name'
                variant='outlined'
                inputRef={register({ required: true })}
                error={!!errors.name}
                InputLabelProps={{ shrink }}
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <TextField
                type='number'
                fullWidth
                inputProps={{ min: 0, step: '.01' }}
                label='Price'
                name='price'
                variant='outlined'
                inputRef={register({ required: true })}
                error={!!errors.price}
                InputLabelProps={{ shrink }}
              />
            </Grid>
          </Grid>
        </CardContent>
        <CardActions>
          <Button type='submit' color='primary' variant='text' fullWidth>
            {isEdit ? 'Update' : 'Add'} appliance
          </Button>
        </CardActions>
      </form>
    </Card>
  )
}

export default AddAppliance
