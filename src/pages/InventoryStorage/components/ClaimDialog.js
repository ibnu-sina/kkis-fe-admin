import React from 'react'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  TextField,
  Typography,
} from '@material-ui/core'
import { useForm } from 'react-hook-form'
import { claimStorageRequest } from 'redux/actions/inventoryStorageActions'
import { useDispatch } from 'react-redux'

function ClaimDialog({ item, open, handleClose }) {
  const { handleSubmit, register, errors } = useForm()

  const dispatch = useDispatch()

  const onSubmit = (body) => {
    const { claim_box_no, claim_description, claim_claimer_name } = body
    const data = {
      id: item.id,
      claim_box_no: Number(claim_box_no),
      claim_description,
      claim_claimer_name,
    }
    dispatch(claimStorageRequest(data))
    handleClose()
  }

  return (
    <Dialog open={open} onClose={handleClose} maxWidth='xs' fullWidth>
      <form onSubmit={handleSubmit(onSubmit)}>
        <DialogTitle disableTypography>
          <Typography variant='h5' component='h5'>
            Please fill up the form
          </Typography>
        </DialogTitle>
        <DialogContent>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <TextField
                fullWidth
                type='number'
                label='Total Box'
                name='claim_box_no'
                variant='outlined'
                inputRef={register({ required: true })}
                error={!!errors.claim_box_no}
                helperText={!!errors.claim_box_no && 'Required field'}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                fullWidth
                type='text'
                label='Others'
                name='claim_description'
                variant='outlined'
                autoComplete='off'
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                fullWidth
                type='text'
                label='Claimer Name'
                name='claim_claimer_name'
                variant='outlined'
                autoComplete='off'
                inputRef={register({ required: true })}
                error={!!errors.claim_claimer_name}
                helperText={!!errors.claim_claimer_name && 'Required field'}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button color='primary' onClick={handleClose}>
            Cancel
          </Button>
          <Button type='submit' color='primary' variant='contained'>
            Confirm
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  )
}

export default ClaimDialog
