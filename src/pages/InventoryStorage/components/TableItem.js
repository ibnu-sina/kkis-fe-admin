import React, { useState } from 'react'
import moment from 'moment'
import { Button, Link as TextLink, TableCell, TableRow } from '@material-ui/core'
import StatusBadge from 'components/StatusBadge'
import ClaimDialog from './ClaimDialog'
import { Link } from 'react-router-dom'
import ViewDialog from './ViewDialog'
import ResolveDialog from './ResolveDialog'

function TableItem({ item }) {
  const [openClaim, setOpenClaim] = useState(false)
  const [openView, setOpenView] = useState(false)
  const [openResolve, setOpenResolve] = useState(false)

  const actionButton = {
    claimed: (
      <Button color='primary' onClick={() => setOpenView(true)}>
        View
      </Button>
    ),
    pending: (
      <Button color='primary' onClick={() => setOpenResolve(true)}>
        Resolve
      </Button>
    ),
    in_store: (
      <Button color='primary' onClick={() => setOpenClaim(true)}>
        Claim
      </Button>
    ),
  }

  const statusBadge = {
    claimed: <StatusBadge text='Claimed' severity='success' />,
    pending: <StatusBadge text='Pending' severity='warning' />,
    in_store: <StatusBadge text='In Store' severity='info' />,
  }

  return (
    <TableRow key={item.id}>
      {openClaim && (
        <ClaimDialog open={openClaim} handleClose={() => setOpenClaim(false)} item={item} />
      )}
      {openView && (
        <ViewDialog open={openView} handleClose={() => setOpenView(false)} item={item} />
      )}
      {openResolve && (
        <ResolveDialog open={openResolve} handleClose={() => setOpenResolve(false)} item={item} />
      )}
      <TableCell>{moment(item.request_date).format('DD/MM/YYYY')}</TableCell>
      <TableCell>
        <TextLink component={Link} to={`/dashboard/students/${item.student.id}`}>
          {item.student.name}
        </TextLink>
      </TableCell>
      <TableCell>{item.request_box_no || 'N/A'}</TableCell>
      <TableCell>{item.request_description || '-'}</TableCell>
      <TableCell>
        {item.request_admin ? (
          <TextLink component={Link} to={`/dashboard/admins/${item.request_admin.id}`}>
            {item.request_admin.name}
          </TextLink>
        ) : (
          'N/A'
        )}
      </TableCell>
      <TableCell>
        {item.request_storekeeper ? (
          <TextLink component={Link} to={`/dashboard/admins/${item.request_storekeeper.id}`}>
            {item.request_storekeeper.name}
          </TextLink>
        ) : (
          'N/A'
        )}
      </TableCell>
      <TableCell>{statusBadge[item.status]}</TableCell>
      <TableCell>{actionButton[item.status]}</TableCell>
    </TableRow>
  )
}

export default TableItem
