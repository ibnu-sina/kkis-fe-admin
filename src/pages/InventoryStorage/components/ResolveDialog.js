import React, { useState, useEffect } from 'react'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  TextField,
  Typography,
} from '@material-ui/core'
import { Controller, useForm } from 'react-hook-form'
import { Autocomplete } from '@material-ui/lab'
import { useDispatch, useSelector } from 'react-redux'
import { getAdminList } from 'redux/actions/adminListActions'
import { resolveStorageRequest } from 'redux/actions/inventoryStorageActions'

function ResolveDialog({ item, open, handleClose }) {
  const { handleSubmit, register, errors, control } = useForm()

  const dispatch = useDispatch()
  const { adminList, status } = useSelector((state) => state.adminList)

  const onSubmit = (body) => {
    const { request_box_no, request_description, request_storekeeper } = body
    const { id } = request_storekeeper
    const data = {
      id: item.id,
      request_box_no: Number(request_box_no),
      request_description,
      request_storekeeper_id: id,
    }
    dispatch(resolveStorageRequest(data))
    handleClose()
  }

  useEffect(() => {
    if (status === 'idle') {
      dispatch(getAdminList())
    }
    setAdmin(adminList)
  }, [dispatch, adminList, status])

  const [admin, setAdmin] = useState([])

  return (
    <Dialog open={open} onClose={handleClose} maxWidth='xs' fullWidth>
      <form onSubmit={handleSubmit(onSubmit)}>
        <DialogTitle disableTypography>
          <Typography variant='h5' component='h5'>
            Please fill up the form
          </Typography>
        </DialogTitle>
        <DialogContent>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <TextField
                fullWidth
                type='number'
                label='Total Box'
                name='request_box_no'
                variant='outlined'
                autoComplete='off'
                inputRef={register({ required: true })}
                error={!!errors.request_box_no}
                helperText={!!errors.request_box_no && 'Required field'}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                fullWidth
                type='text'
                label='Others'
                name='request_description'
                variant='outlined'
                inputRef={register}
                autoComplete='off'
              />
            </Grid>

            <Grid item xs={12}>
              <Controller
                as={
                  <Autocomplete
                    options={admin}
                    getOptionSelected={(option) => option.name}
                    getOptionLabel={(option) =>
                      option.id ? `${option.name} (${option.email})` : ''
                    }
                    renderOption={(option) => `${option.name} (${option.email})`}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label='Storekeeper'
                        variant='outlined'
                        error={!!errors.request_storekeeper}
                      />
                    )}
                  />
                }
                onChange={([, data]) => data}
                name='request_storekeeper'
                control={control}
                defaultValue={{ id: null, name: '', email: '' }}
                rules={{
                  validate: (value) => {
                    if (!value) return false
                    return Boolean(value.id && value.name)
                  },
                }}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button color='primary' onClick={handleClose}>
            Cancel
          </Button>
          <Button type='submit' color='primary' variant='contained'>
            Confirm
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  )
}

export default ResolveDialog
