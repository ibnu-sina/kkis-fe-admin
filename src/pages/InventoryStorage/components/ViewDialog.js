import React from 'react'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Link as TextLink,
  makeStyles,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Typography,
} from '@material-ui/core'
import { Link } from 'react-router-dom'
import moment from 'moment'

function ViewDialog({ item, open, handleClose }) {
  const useStyles = makeStyles((theme) => ({ text: { color: theme.palette.text.secondary } }))
  const classes = useStyles()

  return (
    <Dialog open={open} onClose={handleClose} maxWidth='xs' fullWidth>
      <DialogTitle disableTypography>
        <Typography variant='h5' component='h5'>
          Claim Info
        </Typography>
      </DialogTitle>
      <DialogContent>
        <Table>
          <TableBody>
            <TableRow>
              <TableCell>
                <Typography variant='h6'>Claim Date</Typography>
              </TableCell>
              <TableCell className={classes.text}>
                {moment(item.claim_date).format('D MMM YY')}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <Typography variant='h6'>Total Box</Typography>
              </TableCell>
              <TableCell className={classes.text}>{item.claim_box_no}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <Typography variant='h6'>Others</Typography>
              </TableCell>
              <TableCell className={classes.text}>{item.claim_description || '-'}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <Typography variant='h6'>Claimer</Typography>
              </TableCell>
              <TableCell className={classes.text}>{item.claim_claimer_name}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <Typography variant='h6'>Admin</Typography>
              </TableCell>
              <TableCell>
                <TextLink component={Link} to={`/dashboard/admins/${item.claim_admin.id}`}>
                  {item.claim_admin.name}
                </TextLink>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </DialogContent>
      <DialogActions>
        <Button fullWidth color='primary' onClick={handleClose}>
          OK
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default ViewDialog
