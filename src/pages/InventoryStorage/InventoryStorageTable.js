import React from 'react'
import PerfectScrollbar from 'react-perfect-scrollbar'
import {
  Box,
  Card,
  CardContent,
  makeStyles,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TextField,
} from '@material-ui/core'
import TableItem from './components/TableItem'
import { useDispatch, useSelector } from 'react-redux'
import { getInventoryStorageList } from 'redux/actions/inventoryStorageActions'
import { UPDATE_INVENTORY_STORAGE_STATUS_FILTER } from 'redux/types/inventoryStorageTypes'

const useStyles = makeStyles({
  root: {
    width: 200,
  },
})

function InventoryStorageTable({ Pagination }) {
  const classes = useStyles()
  const dispatch = useDispatch()

  const { inventoryStorageList, filter } = useSelector((state) => state.inventoryStorageList)

  const handleChange = (e) => {
    dispatch({ type: UPDATE_INVENTORY_STORAGE_STATUS_FILTER, payload: e.target.value })
    dispatch(getInventoryStorageList())
  }

  return (
    <Card>
      <CardContent>
        <Box display='flex'>
          <TextField
            select
            label='Status'
            onChange={handleChange}
            value={filter.status}
            SelectProps={{ native: true }}
            variant='outlined'
            className={classes.root}
            InputLabelProps={{ shrink: true }}
          >
            <option value=''>All</option>
            <option value='pending'>Pending</option>
            <option value='in_store'>In Store</option>
            <option value='claimed'>Claimed</option>
          </TextField>
        </Box>
      </CardContent>
      <PerfectScrollbar>
        <Box minWidth={1050}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Store Date</TableCell>
                <TableCell>Student Name</TableCell>
                <TableCell>Box No.</TableCell>
                <TableCell>Others</TableCell>
                <TableCell>PIC</TableCell>
                <TableCell>Storekeeper</TableCell>
                <TableCell>Status</TableCell>
                <TableCell>Action</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {inventoryStorageList.map((item) => (
                <TableItem key={item.id} item={item} />
              ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <Pagination />
    </Card>
  )
}

export default InventoryStorageTable
