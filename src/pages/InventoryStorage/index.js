import React, { useEffect } from 'react'

import { Box, Container, makeStyles, Typography } from '@material-ui/core'
import InventoryStorageTable from './InventoryStorageTable'
import { useDispatch, useSelector } from 'react-redux'
import Loader from 'components/Loader'
import { getInventoryStorageList } from 'redux/actions/inventoryStorageActions'
import CustomSnackbar from 'components/CustomSnackbar'
import { RESET_INVENTORY_STORAGE } from 'redux/types/inventoryStorageTypes'
import useReduxPagination from 'hooks/useReduxPagination'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100vh',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}))

function InventoryStorage() {
  const classes = useStyles()

  const dispatch = useDispatch()
  const { Pagination } = useReduxPagination('inventoryStorageList')
  const { page, perPage } = useSelector((state) => state.inventoryStorageListPagination)
  const { status } = useSelector((state) => state.inventoryStorageList)
  const { successMsg, errorMsg } = useSelector((state) => state.inventoryStorage)

  useEffect(() => {
    dispatch(getInventoryStorageList())

    return () => dispatch({ type: RESET_INVENTORY_STORAGE })
  }, [dispatch, page, perPage])

  return (
    <div className={classes.root}>
      <Container maxWidth='xl'>
        <Typography variant='h2' component='h2'>
          Inventory storage
        </Typography>
        {(successMsg || errorMsg) && (
          <CustomSnackbar
            horizontal='center'
            vertical='top'
            variant='filled'
            severity={successMsg ? 'success' : 'error'}
          >
            {successMsg || errorMsg}
          </CustomSnackbar>
        )}
        <Box mt={3}>
          {status === 'loading' && <Loader />}
          {status === 'loaded' && <InventoryStorageTable Pagination={Pagination} />}
        </Box>
      </Container>
    </div>
  )
}

export default InventoryStorage
