import React, { useEffect, useRef } from 'react'
import { useParams } from 'react-router-dom'
import { useForm } from 'react-hook-form'
import { useDispatch, useSelector } from 'react-redux'
import { verifyToken } from 'redux/actions/registerActions'
import { registerAdmin } from 'redux/actions/userActions'

import Alert from '@material-ui/lab/Alert'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import Loader from 'components/Loader'

function Register() {
  const { token } = useParams()
  const dispatch = useDispatch()

  const { status, errorMsg } = useSelector((state) => state.register)

  const { register, handleSubmit, errors, watch } = useForm({ mode: 'onChange' })
  const password = useRef({})
  password.current = watch('password', '')

  useEffect(() => {
    if (status === 'idle') dispatch(verifyToken(token))
  }, [dispatch, token, status])

  const onSubmit = ({ name, password, confirm_password }) => {
    dispatch(registerAdmin(name, password, confirm_password, token))
  }

  return (
    <>
      <Grid
        container
        spacing={0}
        direction='column'
        alignItems='center'
        justify='center'
        style={{ minHeight: '80vh' }}
      >
        <Grid>
          {status === 'loading' && <Loader />}
          {status !== 'loading' && (
            <form onSubmit={handleSubmit(onSubmit)}>
              <Box mb={3}>
                <Typography color='textPrimary' variant='h2'>
                  Welcome to KKIS{' '}
                  <span role='img' aria-label='wave'>
                    👋
                  </span>
                </Typography>
                <Typography color='textSecondary' gutterBottom variant='body2'>
                  Please fill in the information below to complete your registration
                </Typography>
              </Box>
              {status === 'error' && <Alert severity='error'>{errorMsg}</Alert>}
              <TextField
                fullWidth
                label='Name'
                margin='normal'
                name='name'
                type='text'
                variant='outlined'
                inputRef={register({ required: true })}
                error={!!errors.name}
                helperText={!!errors.name && 'Name is required'}
                disabled={status === 'error'}
              />
              <TextField
                fullWidth
                label='Password'
                margin='normal'
                name='password'
                type='password'
                variant='outlined'
                inputRef={register({ required: true })}
                error={!!errors.password}
                helperText={!!errors.password && 'Password is required'}
                disabled={status === 'error'}
              />

              <TextField
                fullWidth
                label='Confirm Password'
                margin='normal'
                name='confirm_password'
                type='password'
                variant='outlined'
                inputRef={register({
                  validate: (value) => value === password.current || 'The passwords do not match',
                })}
                error={!!errors.confirm_password}
                helperText={!!errors.confirm_password && errors.confirm_password.message}
                disabled={status === 'error'}
              />

              <Box my={2}>
                <Button
                  color='primary'
                  fullWidth
                  size='large'
                  type='submit'
                  variant='contained'
                  disabled={status === 'error'}
                >
                  Register
                </Button>
              </Box>
            </form>
          )}
        </Grid>
      </Grid>
    </>
  )
}

export default Register
