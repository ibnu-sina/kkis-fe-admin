import React from 'react'

import { Box, CardContent } from '@material-ui/core'
import { getMeritList } from 'redux/actions/meritListActions'
import useReduxSearch from 'hooks/useReduxSearch'

const Toolbar = () => {
  const { SearchField } = useReduxSearch('meritList', getMeritList)
  return (
    <CardContent>
      <Box maxWidth={500}>
        <SearchField />
      </Box>
    </CardContent>
  )
}

export default Toolbar
