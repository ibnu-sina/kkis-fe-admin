import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getMeritList } from 'redux/actions/meritListActions'

import Toolbar from './components/Toolbar'
import MeritTableData from './MeritTableData'
import { Card, Tab, Tabs } from '@material-ui/core'
import useReduxPagination from 'hooks/useReduxPagination'

function MeritTable({ className, ...rest }) {
  const dispatch = useDispatch()
  const [value, setValue] = useState('')

  const { Pagination } = useReduxPagination('meritList')
  const { page, perPage } = useSelector((state) => state.meritListPagination)

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  useEffect(() => {
    dispatch(getMeritList({ status: value }))
  }, [dispatch, page, perPage, value])

  return (
    <Card className={className} {...rest}>
      <Tabs value={value} onChange={handleChange} textColor='secondary'>
        <Tab label='All' value='' />
        <Tab label='Approved' value={1} />
        <Tab label='Pending' value={0} />
      </Tabs>
      <Toolbar />
      <MeritTableData Pagination={Pagination} />
    </Card>
  )
}

export default MeritTable
