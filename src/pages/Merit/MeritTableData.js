import React from 'react'
import moment from 'moment'
import { NavLink as RouterLink } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { toggleMeritApproval } from 'redux/actions/meritListActions'

import PerfectScrollbar from 'react-perfect-scrollbar'
import { Link as LinkIcon } from 'react-feather'
import {
  Box,
  FormControlLabel,
  IconButton,
  Link,
  Switch,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from '@material-ui/core'

function MeritTableData({ Pagination }) {
  const dispatch = useDispatch()

  const { meritList } = useSelector((state) => state.meritList)

  const handleChange = (id) => {
    dispatch(toggleMeritApproval(id))
  }

  return (
    <div role='tabpanel'>
      <PerfectScrollbar>
        <Box minWidth={1050}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Student Name</TableCell>
                <TableCell>Name</TableCell>
                <TableCell>Type</TableCell>
                <TableCell>Position</TableCell>
                <TableCell>Level</TableCell>
                <TableCell>Achievement</TableCell>
                <TableCell>Term</TableCell>
                <TableCell>Merit</TableCell>
                <TableCell>Date</TableCell>
                <TableCell>Session</TableCell>
                <TableCell>Status</TableCell>
                <TableCell>Attachment</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {meritList.map((merit) => (
                <TableRow hover key={merit.id}>
                  <TableCell>
                    <Link component={RouterLink} to={`/dashboard/students/${merit.student.id}`}>
                      {merit.student.name}
                    </Link>
                  </TableCell>
                  <TableCell>{merit.name}</TableCell>
                  <TableCell>{merit.type}</TableCell>
                  <TableCell>{merit.position}</TableCell>
                  <TableCell>{merit.level}</TableCell>
                  <TableCell>{merit.achievement}</TableCell>
                  <TableCell>{merit.term}</TableCell>
                  <TableCell>{merit.merit}</TableCell>
                  <TableCell>{merit.date && moment(merit.date).format('D MMM YYYY')}</TableCell>
                  <TableCell>{merit.session}</TableCell>
                  <TableCell>
                    <FormControlLabel
                      control={
                        <Switch
                          name='approval'
                          color='primary'
                          size='small'
                          checked={Boolean(merit.is_approved)}
                          onChange={() => handleChange(merit.id)}
                        />
                      }
                      label={merit.is_approved ? 'Approved' : 'Pending'}
                    />
                  </TableCell>
                  <TableCell>
                    {merit.attachment ? (
                      <IconButton component={Link} href={merit.attachment.url} target='_blank'>
                        <LinkIcon size='20' />
                      </IconButton>
                    ) : (
                      'No attachment'
                    )}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <Pagination />
    </div>
  )
}

export default MeritTableData
