import React, { useEffect, useState } from 'react'
import { getStudentProfile } from 'redux/actions/studentProfileActions'
import { useDispatch, useSelector } from 'react-redux'
import { Box, Button, Container, Grid, makeStyles, Typography } from '@material-ui/core'

import Loader from 'components/Loader'
import SectionList from 'components/SectionList'
import CustomSnackbacr from 'components/CustomSnackbar'
import PersonalCard from './components/PersonalCard'
import ContactCard from './components/ContactCard'
import UniversityCard from './components/UniversityCard'
import RoomCard from './components/RoomCard'
import SignatureCard from './components/SignatureCard'

import { RESET_STUDENT_APPLIANCE } from 'redux/types/studentApplianceTypes'
import { RESET_REASSIGN_STUDENT_ROOM } from 'redux/types/studentProfileTypes'
import { ChevronLeft as ChevronLeftIcon } from 'react-feather'
import { Link } from 'react-router-dom'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100vh',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}))

const listItem = [
  { section: 'personal', label: 'Personal Info' },
  { section: 'contact', label: 'Contact Info' },
  { section: 'university', label: 'University Info' },
  { section: 'room', label: 'Room Info' },
  { section: 'signature', label: 'Student Signature' },
]

function StudentProfile({ match }) {
  const [section, setSection] = useState('personal')
  const classes = useStyles()

  const id = Number(match.params.id)
  const dispatch = useDispatch()

  const { errorMsg, status, studentProfile } = useSelector((state) => state.studentProfile)
  const { successMsg: roomSuccess, errorMsg: roomError } = useSelector((state) => state.studentRoom)
  const { errorMsg: applianceError, successMsg } = useSelector((state) => state.studentAppliance)

  useEffect(() => {
    dispatch(getStudentProfile(id))

    return () => {
      dispatch({ type: RESET_REASSIGN_STUDENT_ROOM })
      dispatch({ type: RESET_STUDENT_APPLIANCE })
    }
  }, [dispatch, id])

  if (['loading', 'idle'].includes(status)) return <Loader />
  return (
    <div className={classes.root}>
      <Container maxWidth='lg'>
        {(errorMsg || applianceError || roomError) && (
          <CustomSnackbacr vertical='top' horizontal='center' severity='error' variant='filled'>
            {errorMsg || applianceError || roomError}
          </CustomSnackbacr>
        )}
        {(successMsg || roomSuccess) && (
          <CustomSnackbacr vertical='top' horizontal='center' severity='success' variant='filled'>
            {successMsg || roomSuccess}
          </CustomSnackbacr>
        )}
        <Button
          color='primary'
          startIcon={<ChevronLeftIcon />}
          component={Link}
          to='/dashboard/students'
        >
          Back to student list
        </Button>
        <Box mt={3}>
          <Typography component='h1' variant='h4'>
            {studentProfile.name}
          </Typography>
          <Typography component='h2' variant='body1'>
            {studentProfile.matric_no}
          </Typography>
        </Box>
        <Box mt={3}>
          <Grid container spacing={3}>
            <Grid item xs={12} md={4} lg={3}>
              <SectionList section={section} setSection={setSection} listItem={listItem} />
            </Grid>
            <Grid item xs={12} md={8} lg={6}>
              {section === 'personal' && <PersonalCard user={studentProfile} />}
              {section === 'contact' && <ContactCard user={studentProfile} />}
              {section === 'university' && <UniversityCard user={studentProfile} />}
              {section === 'room' && <RoomCard user={studentProfile} />}
              {section === 'signature' && <SignatureCard user={studentProfile} />}
            </Grid>
          </Grid>
        </Box>
      </Container>
    </div>
  )
}

export default StudentProfile
