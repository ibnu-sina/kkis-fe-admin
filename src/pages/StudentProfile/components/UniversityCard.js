import React from 'react'

import {
  Card,
  CardHeader,
  Divider,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Typography,
} from '@material-ui/core'

function UniversityCard({ user }) {
  return (
    <Card>
      <CardHeader title='University Info' />
      <Divider />
      <Table>
        <TableBody>
          <TableRow>
            <TableCell width='30%'>
              <Typography variant='h6'>Matric No.</Typography>
            </TableCell>
            <TableCell width='70%'>{user.matric_no}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography variant='h6'>Course</Typography>
            </TableCell>
            <TableCell>{user.course?.name}</TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </Card>
  )
}

export default UniversityCard
