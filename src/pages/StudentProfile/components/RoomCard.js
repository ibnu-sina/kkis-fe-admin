import React, { useState } from 'react'

import {
  Box,
  Button,
  Card,
  CardHeader,
  Divider,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Typography,
} from '@material-ui/core'
import StatusBadge from 'components/StatusBadge'
import EditRoomModal from './EditRoomModal'

function RoomCard({ user }) {
  const [open, setOpen] = useState(false)
  const onClose = () => setOpen(false)

  const getStatus = (room_status) => {
    const status = room_status.split('_').join(' ')
    const severity = room_status.includes('in') ? 'success' : 'error'
    return <StatusBadge text={status} severity={severity} />
  }
  return (
    <Card>
      {open && <EditRoomModal open={open} onClose={onClose} user={user} />}
      <CardHeader title='Room Info' />
      <Divider />
      <Table>
        <TableBody>
          <TableRow>
            <TableCell width='30%'>
              <Typography variant='h6'>Status</Typography>
            </TableCell>
            <TableCell width='70%'>{getStatus(user.room_status)}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography variant='h6'>Name</Typography>
            </TableCell>
            <TableCell>{user.room?.name || 'N/A'}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell colSpan={2} padding='none'>
              <Box>
                <Button size='large' color='primary' fullWidth onClick={() => setOpen(true)}>
                  Edit Room
                </Button>
              </Box>
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </Card>
  )
}

export default RoomCard
