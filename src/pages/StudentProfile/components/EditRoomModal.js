import React from 'react'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Typography,
} from '@material-ui/core'
import { useForm } from 'react-hook-form'
import RoomDropdown from 'components/RoomDropdown'
import { useDispatch } from 'react-redux'
import { reassignRoom, removeStudentRoom } from 'redux/actions/studentProfileActions'

function EditRoomModal({ open, onClose, user }) {
  const { handleSubmit, errors, control } = useForm()
  const dispatch = useDispatch()

  // handle submit
  const onSubmit = ({ room }) => {
    if (room) {
      // Reassign student room
      const data = { student_id: user.id, room_id: room.id }
      dispatch(reassignRoom(data))
    } else {
      // remove student from room
      dispatch(removeStudentRoom(user.id))
    }
    onClose()
  }

  return (
    <Dialog open={open} onClose={onClose} maxWidth='xs' fullWidth>
      <form onSubmit={handleSubmit(onSubmit)}>
        <DialogTitle disableTypography>
          <Typography variant='h3' component='h3'>
            Edit Room
          </Typography>
        </DialogTitle>
        <DialogContent>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <RoomDropdown control={control} errors={errors} currentRoom={user.room} />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button type='submit' color='primary' variant='contained'>
            Update
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  )
}

export default EditRoomModal
