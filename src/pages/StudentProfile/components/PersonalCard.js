import React from 'react'
import moment from 'moment'
import {
  Card,
  CardHeader,
  Divider,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Typography,
} from '@material-ui/core'

function PersonalCard({ user }) {
  return (
    <Card>
      <CardHeader title='Personal Info' />
      <Divider />
      <Table>
        <TableBody>
          <TableRow>
            <TableCell width='30%'>
              <Typography variant='h6'>Name</Typography>
            </TableCell>
            <TableCell width='70%'>{user.name}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography variant='h6'>Gender</Typography>
            </TableCell>
            <TableCell>{user.gender}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography variant='h6'>IC No.</Typography>
            </TableCell>
            <TableCell>{user.ic_no}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography variant='h6'>Religion</Typography>
            </TableCell>
            <TableCell>{user.religion}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography variant='h6'>Race</Typography>
            </TableCell>
            <TableCell>{user.race}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography variant='h6'>Birth Date</Typography>
            </TableCell>
            <TableCell>
              {user.birth_date ? moment(user.birth_date).format('D MMM YYYY') : ''}
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography variant='h6'>Birth Place</Typography>
            </TableCell>
            <TableCell>{user.birth_place}</TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </Card>
  )
}

export default PersonalCard
