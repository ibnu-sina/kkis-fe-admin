import React, { useEffect, useState } from 'react'
import axios from 'axios'

import { Box, Card, makeStyles, Typography } from '@material-ui/core'
import Loader from 'components/Loader'

const useStyles = makeStyles(() => ({
  container: {
    height: '400px',
  },
  image: {
    objectFit: 'cover',
  },
}))

function SignatureCard({ user }) {
  const classes = useStyles()

  const [loading, setLoading] = useState(false)
  const [photoUrl, setPhotoUrl] = useState('')

  useEffect(() => {
    axios
      .get(`/student-uploads/signature/${user.id}`)
      .then(({ data: { data } }) => {
        setPhotoUrl(data.url)
        setLoading(false)
      })
      .catch((_) => setLoading(false))
  }, [user])

  return (
    <Card className={classes.container}>
      <Box display='flex' alignItems='center' justifyContent='center' css={{ height: '100%' }}>
        {loading ? (
          <Loader />
        ) : photoUrl ? (
          <img className={classes.image} src={photoUrl} alt='student-signature' />
        ) : (
          <Box>
            <Typography>This student have not uploaded thier signature yet.</Typography>
          </Box>
        )}
      </Box>
    </Card>
  )
}

export default SignatureCard
