import React from 'react'
import moment from 'moment'

import Box from '@material-ui/core/Box'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'

function MainCard({ user }) {
  return (
    <Card>
      <CardContent>
        <Box alignItems='center' display='flex' flexDirection='column'>
          <Typography color='textPrimary' gutterBottom variant='h4'>
            {user.name}
          </Typography>
          <Typography color='textSecondary' variant='body1'>
            {`Registered in ${moment(user.created_at).format('MMM YYYY')}`}
          </Typography>
        </Box>
      </CardContent>
    </Card>
  )
}

export default MainCard
