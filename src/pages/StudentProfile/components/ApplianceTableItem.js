import React from 'react'
import useSerialNo from 'hooks/useSerialNo'
import { useDispatch } from 'react-redux'
import { deleteStudentAppliance } from 'redux/actions/studentApplianceActions'
import { Button, IconButton, TableCell, TableRow } from '@material-ui/core'
import { Trash } from 'react-feather'

function ApplianceTableItem({ item, student_id }) {
  const { id: appliance_id, name, pivot } = item
  const pageInfo = {
    from: 'student_profile_page',
    student_id,
    appliance_id,
  }
  const { setDialogOpen, AddSerialNoDialog } = useSerialNo(pageInfo)

  const dispatch = useDispatch()
  const handleDelete = () => {
    dispatch(deleteStudentAppliance(pageInfo))
  }
  return (
    <TableRow>
      <AddSerialNoDialog />
      <TableCell>{name}</TableCell>
      <TableCell>
        {pivot.serial_no ? (
          pivot.serial_no
        ) : (
          <Button color='primary' onClick={() => setDialogOpen(true)}>
            Add Serial No.
          </Button>
        )}
      </TableCell>
      <TableCell>
        <IconButton onClick={handleDelete}>
          <Trash size='15' />
        </IconButton>
      </TableCell>
    </TableRow>
  )
}

export default ApplianceTableItem
