import React from 'react'
import {
  Box,
  Card,
  CardHeader,
  Divider,
  makeStyles,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from '@material-ui/core'
import ApplianceTableItem from './ApplianceTableItem'

function ApplianceCard({ user }) {
  const useStyles = makeStyles(() => ({
    tableBody: {
      overflowX: 'scroll',
    },
  }))
  const classes = useStyles()

  return (
    <Card>
      <Box display='flex' flexDirection='column' maxHeight='300px'>
        <CardHeader title='Registered Appliances' />
        <Divider />
        <Box className={classes.tableBody}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Name</TableCell>
                <TableCell>Serial No.</TableCell>
                <TableCell width='20%'>Delete</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {user.appliances.map((item) => (
                <ApplianceTableItem student_id={user.id} item={item} key={item.id} />
              ))}
            </TableBody>
          </Table>
        </Box>
      </Box>
    </Card>
  )
}

export default ApplianceCard
