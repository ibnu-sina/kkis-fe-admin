import React from 'react'
import {
  Card,
  CardHeader,
  Divider,
  Link,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Typography,
} from '@material-ui/core'

function ContactCard({ user }) {
  return (
    <Card>
      <CardHeader title='Contact Info' />
      <Divider />
      <Table>
        <TableBody>
          <TableRow>
            <TableCell width='30%'>
              <Typography variant='h6'>Email</Typography>
            </TableCell>
            <TableCell width='70%'>
              <Link href={`mailto:${user.email}`}>{user.email}</Link>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography variant='h6'>Phone No.</Typography>
            </TableCell>
            <TableCell>{user.phone_no}</TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </Card>
  )
}

export default ContactCard
