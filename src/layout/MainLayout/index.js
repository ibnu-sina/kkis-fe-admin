import React from 'react'

import { makeStyles } from '@material-ui/core'
import TopBar from './TopBar'
import { Route } from 'react-router-dom'
import Login from 'pages/Login/Login'
import ForgotPassword from 'pages/ForgotPassword/ForgotPassword'
import Register from 'pages/Register'
import UpdatePassword from 'pages/ForgotPassword/UpdatePassword'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.default,
    display: 'flex',
    height: '100%',
    overflow: 'hidden',
    width: '100%',
  },
  wrapper: {
    display: 'flex',
    flex: '1 1 auto',
    overflow: 'hidden',
    paddingTop: 64,
  },
  contentContainer: {
    display: 'flex',
    flex: '1 1 auto',
    overflow: 'hidden',
  },
  content: {
    flex: '1 1 auto',
    height: '100%',
    overflow: 'auto',
  },
}))

const MainLayout = ({ match }) => {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <TopBar />
      <div className={classes.wrapper}>
        <div className={classes.contentContainer}>
          <div className={classes.content}>
            <Route exact path={match.url} component={Login} />
            <Route exact path={match.url + 'update-password/:token'} component={UpdatePassword} />
            <Route exact path={match.url + 'forgot-password'} component={ForgotPassword} />
            <Route exact path={match.url + 'register/:token'} component={Register} />
          </div>
        </div>
      </div>
    </div>
  )
}

export default MainLayout
