import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { logout } from 'redux/actions/userActions'

import { LogOut as LogOutIcon, Menu as MenuIcon, User as UserIcon } from 'react-feather'
import {
  AppBar,
  Box,
  Button,
  Hidden,
  IconButton,
  ListItem,
  ListItemText,
  Popover,
  Toolbar,
} from '@material-ui/core'
import { useHistory } from 'react-router-dom'
import { AccountCircle } from '@material-ui/icons'

const TopBar = ({ className, onMobileNavOpen, ...rest }) => {
  const dispatch = useDispatch()
  const history = useHistory()

  const [menu, setMenu] = useState(false)

  const { profile, status } = useSelector((state) => state.userProfile)

  const logOut = () => {
    dispatch(logout())
  }

  const handleNavigate = (link) => {
    history.push(link)
    setMenu(false)
  }

  return (
    <AppBar elevation={0} {...rest}>
      <Toolbar>
        <Hidden lgUp>
          <IconButton color='inherit' onClick={onMobileNavOpen}>
            <MenuIcon />
          </IconButton>
        </Hidden>
        <Box flexGrow={1} />
        {status === 'loaded' && (
          <Box>
            <Button
              id='button-base'
              color='inherit'
              size='large'
              startIcon={<AccountCircle />}
              onClick={() => setMenu(true)}
            >
              {profile.name}
            </Button>
          </Box>
        )}
        <Popover
          open={menu}
          anchorEl={document.getElementById('button-base')}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          onClose={() => setMenu(false)}
        >
          <ListItem button onClick={() => handleNavigate(`/dashboard/admins/${profile.id}`)}>
            <Box mr={1}>
              <UserIcon size={18} />
            </Box>
            <ListItemText primary='My Profile' primaryTypographyProps={{ variant: 'body2' }} />
          </ListItem>
          <ListItem button onClick={logOut}>
            <Box mr={1}>
              <LogOutIcon size={18} />
            </Box>
            <ListItemText primary='Log Out' primaryTypographyProps={{ variant: 'body2' }} />
          </ListItem>
        </Popover>
      </Toolbar>
    </AppBar>
  )
}

export default TopBar
