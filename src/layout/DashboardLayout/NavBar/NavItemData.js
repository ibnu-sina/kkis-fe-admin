import { Box, LogIn, LogOut } from 'react-feather'
import {
  BorderColorOutlined,
  ComputerOutlined,
  GroupOutlined,
  LocalHotelOutlined,
  LocalOfferOutlined,
  MeetingRoomOutlined,
  PersonAddOutlined,
  SchoolOutlined,
} from '@material-ui/icons'

export default [
  { title: 'Student' },
  {
    href: '/students',
    icon: GroupOutlined,
    title: 'Students',
  },
  {
    href: '/activity/checkin',
    icon: LogIn,
    title: 'Check in',
  },
  {
    href: '/activity/checkout',
    icon: LogOut,
    title: 'Check out',
  },
  {
    href: '/merit',
    icon: BorderColorOutlined,
    title: 'Merit',
  },
  { title: 'Manage' },
  {
    href: '/appliances',
    icon: ComputerOutlined,
    title: 'Appliance',
  },
  {
    href: '/rooms',
    icon: MeetingRoomOutlined,
    title: 'Room',
  },
  {
    href: '/courses',
    icon: SchoolOutlined,
    title: 'Course',
  },
  { title: 'Forms' },
  {
    href: '/forms/semester-break',
    icon: LocalHotelOutlined,
    title: 'Semester break stay',
  },
  {
    href: '/forms/student-appliances',
    icon: LocalOfferOutlined,
    title: 'Registered appliances',
  },
  {
    href: '/forms/inventory-storage',
    icon: Box,
    title: 'Inventory storage',
  },
  { title: 'Admin' },
  {
    href: '/admins',
    icon: GroupOutlined,
    title: 'Admin',
  },
  {
    href: '/invite-admin',
    icon: PersonAddOutlined,
    title: 'Invite admin',
  },
]
