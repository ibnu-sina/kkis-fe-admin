import React, { useState, useEffect } from 'react'
import { Switch, Route } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { getUserProfile } from 'redux/actions/userActions'

import { makeStyles } from '@material-ui/core'
import NavBar from './NavBar'
import TopBar from './TopBar'

import AdminListView from 'pages/Admin/AdminListView'
import ApplianceListView from 'pages/Appliance'
import ProfileView from 'pages/Admin/ProfileView'
import CourseListView from 'pages/Course'
import StudentListView from 'pages/Student'
import InvitedAdminView from 'pages/Admin/InvitedAdminView'
import CourseView from 'pages/Course/CourseView'
import StudentProfile from 'pages/StudentProfile'
import StudentUpload from 'pages/StudentUpload'
import MeritListView from 'pages/Merit'
import ActivityListView from 'pages/Activity'
import SemesterBreakStay from 'pages/SemesterBreakStay'
import StudentAppliance from 'pages/StudentAppliance'
import StayRequestView from 'pages/SemesterBreakStay/components/StayRequestView'
import InventoryStorage from 'pages/InventoryStorage'
import RoomListView from 'pages/Room'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    display: 'flex',
    height: '100%',
    overflow: 'hidden',
    width: '100%',
  },
  wrapper: {
    display: 'flex',
    flex: '1 1 auto',
    overflow: 'hidden',
    paddingTop: 64,
    [theme.breakpoints.up('lg')]: {
      paddingLeft: 256,
    },
  },
  contentContainer: {
    display: 'flex',
    flex: '1 1 auto',
    overflow: 'hidden',
  },
  content: {
    flex: '1 1 auto',
    height: '100%',
    overflow: 'auto',
  },
}))

const DashboardLayout = () => {
  const classes = useStyles()
  const [isMobileNavOpen, setMobileNavOpen] = useState(false)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getUserProfile())
  }, [dispatch])

  return (
    <div className={classes.root}>
      <TopBar onMobileNavOpen={() => setMobileNavOpen(true)} />
      <NavBar onMobileClose={() => setMobileNavOpen(false)} openMobile={isMobileNavOpen} />
      <div className={classes.wrapper}>
        <div className={classes.contentContainer}>
          <div className={classes.content}>
            <Switch>
              <Route exact path='/dashboard/admins' component={AdminListView} />
              <Route exact path='/dashboard/admins/:id' component={ProfileView} />
              <Route exact path='/dashboard/appliances' component={ApplianceListView} />
              <Route exact path='/dashboard/rooms' component={RoomListView} />
              <Route exact path='/dashboard/invite-admin' component={InvitedAdminView} />
              <Route exact path='/dashboard/courses' component={CourseListView} />
              <Route exact path='/dashboard/courses/:id' component={CourseView} />
              <Route exact path='/dashboard/students' component={StudentListView} />
              <Route exact path='/dashboard/students/upload' component={StudentUpload} />
              <Route exact path='/dashboard/students/:id' component={StudentProfile} />
              <Route exact path='/dashboard/merit' component={MeritListView} />
              <Route exact path='/dashboard/activity/:action' component={ActivityListView} />
              <Route exact path='/dashboard/forms/semester-break' component={SemesterBreakStay} />
              <Route exact path='/dashboard/forms/semester-break/:id' component={StayRequestView} />
              <Route exact path='/dashboard/forms/student-appliances' component={StudentAppliance} />
              <Route exact path='/dashboard/forms/inventory-storage' component={InventoryStorage} />
            </Switch>
          </div>
        </div>
      </div>
    </div>
  )
}

export default DashboardLayout
